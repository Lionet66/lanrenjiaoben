// ==UserScript==
// @name              视频解析，集合了优酷、爱奇艺、腾讯、芒果、乐视、等全网VIP视频免费破解去广告
// @namespace
// @version            2.4.20
// @description       自用解析脚本，仅用于学习研究，请勿用于商业用途。
// @author            嘿嘿
// @icon              data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAIABJREFUeF7tfQmcFNW1/ldV3T0rmwgCLihuoCCK4o4iiolCcN+NIKiguK/xqU/jvvw1GtckT+MaTaJxTVDADaOiRFFcMGryEnEHZR1mprur6v/OOffculXTPTNozC/vhVZ+Pd1dy617vnv2c6434qj7YgCIAx9xHPM/95X9bH/zyvA8jz/SMUEcmZ/k/Eh+4mP0H30Ogpz8bo72Y8/cV08IzXkyljCW7z0/kuM8H77vIxfJdQEPMf3H16FPPCBEfmo4/AMdH/MfAP3sydGIeOwRf4pjPVHOj32+OnJmwJ4n85TMQ5j6HAWB/cyHxX5qnrxYnsOPA0RRBHj6vGa+opCPp/mT4/R+Hj+3zmnZjB2xzCcdq/Ohn+VdZ1qeP3nRZJXgEQD4hn7yY1WiOzeKUXIuhhQAmODuHBkQyOBlQgxdgbiMMKRJiHhCopCmG4ghE+vFRf5EF5TrhgiCAMjnmDD5vM+fA8Q8QfkgQD6fp5/h+x7o0FyOJon+ybVBExFFdkJ4PIYwfN8oRrlMh0QohQZ4BKooQrnkyTiJeIhRKoYMqCiU7/wyjdVDWWAJRAW+T+gTYIEwyvHnnC8EJeAapMk7f0fPbb62E2kWgiFi5AfmAAGAg8jMIk4v6AS4RSDOEwDuEgAEZjUbImeuqnhDHAti9RVkrk/D8nwhXhSHiFGPKC6hGLZgje4+Nt+4D4YMGoDBG6yHNXs3oGtjHg20IgncMnfmwXghI/Bj/t5nIqVvlkZ0dsT/vM/tLhjzPBERmv+X5wnNgzbHQNOKGAsWL8F77y/Aq2/9Be+893c0rfDR1e+GEnEBgrdPnEE4KwPKvCMWIMTmel6U5sAJr207H7ygdhl/N5/hAoAvmJ1se76siCwAlBg+odcP0dSyFDsMWw9Tjx2LtbvVohYhfGb8eR6TZdFCdwswZVi0ounl6Qpxudc/j7bf/k6ZlcwiyBWB5g5+RESkeSgzt6Mp/srL43czXsXt905HfdAbUblAEsUCQFeKcFQjws317HRZVpsVAXKgBUBoOAC8RGa4hBY2SawpswpJrkcQmYYWdO0a4dKLxmPjnrWoIbZsSfvt5/Lf8Qo020VEWBEBF9z8a7zx6pfwg24ISIvxiiCdgxcsazXuK7NiMrqNHtkuAPSgRMEgcKYBQMpRzmtBfeNK3PGT09Doh6ghdsTIE7a3+vXNZ4CWI0nBsgdWRJt94OePzsKDj74GP+6G0HCEVQWAcmxv1/F3xsT+VWu3RM+yEsuS9QcPfhQiKn6Fu286FWt395GD3waH1R69imrS6ZlyVIVOn/O/8cBq62cpgEnn3IovFhWAKA8ExhrxjHJINg4vxLTVktWbLABiP61kKYEc24DnT1l6DcrYbmgPXHDifqjxIkREfFeYdzDbqwHQOThWAwBZGGRwzPnLFzj3ygcQxl2NmauiIAEAi4gM5xYLJIa309F3shUAYwa21WhVGZMBs8FV+grXXzwRW/SvR2ARV/mBvi2hOzdN/z5HtQVEjK/h4eBjr0Yx6g0vJg9Bnq0EWbGGAxgdICXOyQpQALAGWsHUEh09WflR8xd44o4z0K3GY9eJMpzvitX/+5C2c0+aBYAw3RjFGDj+4jvw4d/JzMxZM1HNQzYfUg4juV+nOECOFTofpZa/Y8a956HWI5OufdKvXvmdI+g3PaqSDtQah7j8F49j5stLkI9iNu0tHYwXUYGg920DAHdAwi7EzIuaF+DpX52HAOQIImWvffV+NQC+KWk7d14WADTfpINFKOO6+57FYzM/QOTVWc+aywlcRdDbeeI9xjsqJEv8Bir7faD5M8y47yy26zuy61YTvnME/KZHVbN+3OVIrvULbn0EL766hIMerA8YXY2AoNYBiwACgCDCOIBsTMDI/tJSTLv9RNRxUETDJ9WHvxoA35S0nTuvMwCgK5FddvDp12Ph190YAOySY49hAgB2Be88UWIBViaoT9knjT/EzT8+EAP71LL+X+m1muCVCfdd+yk69q/FaIWH3Y64GF5+HSsKNBinVl8KANY75JF2H2O/3Xpj6qF7SiSuisxfDYB/TQBwaNyLsTQCfjDxFoRevQ0Xsw/AWH3eLhPED0DhYIlDk0cvQq70Oabdfjpyqj12jkPZo1YVGNkV801XUMcrQ4aYHV+1+1d77Grnr+rxnZ2nzj6X3j+5bhl3PPYa7nrwbUQB+Y1FFHheIO8jxv8yJkQoAEgFCLwV+P3PpnRK6evsA3eEn9UAaH+GvikAyBAk7W63Iy5EubARc4U0AI66O/YDSY4Qj2CAEw8biv1327wjmv1Dfm/rcv6HXPbf/iJZzrJwZRn7Tr0TvpdPZWl5IzIACIsL8fRdpyLnZcOL382crgbAdzuvydVD7HPST7FoeXfORKJ/vOhHjr8z9gOj5PkxrrzgB9hm3Z6VzX12A9hsvg5G3tnjOjkB1YTlqvLGTt6ujTLzXd2ns8pIdqVUXTnpebeHxTE+bYpwyEl3iRtPdQECAOVpUjJkXPwY0+8+mwM+qfxBozW1eDHKkYdymaxMymmTWckEmsy5yQjp90ppU3R+9je1RLLHa7g65kRKzpjkHLzAPIhL1zYhTyd5tT36u/ek+Du9KMOJ7seRTpPk2plr2AQaJx8yUT913pLcPz3eJkCZ+9mUQcphJK+/jstYZWGs+Yly9Xw+QFef8iSTuxnysXdg18OugFe7LluBzAFGMQeglB4Pkw4aikP33CrzfBHC2EdTOUSZgLEKK4g8UprJqiOPzEyy25KISZqpySIm4lIWrLwEyZStLITQb5OwNWfNaqZsLHqMAiXJ4k2PuL38PUn0NBNnRKDm4FGuHV3fji6KEWgWFY2Wxm7GUjF3scKKTR+XPBfn/JlVZYFnx5UmgM3FNsfTbfIUpPNjNBTaRmymz/kQF980CznVBUZN+C8GAM3z4z87FvkMhekGzcUYxahEQUb8/g+/R2trK48iyOX4oQv5PL/TJNBDlcOQ/w7LmvFLUcm8rCDIbzTQKJI8Fs/3EFLG7f+4MEOTJex7cm1KBqUhsQeLYBt6nEWcIhanj5f590JOxqKrVblBzqRrS7KqpJwHlF6ekyxdzfSl38rlMoqlEP3790e/Pn3ZZiy2ruDvyxFlAVO2sSwxn1yshiP5QcDjZY5B81Io8NzQKwxLKJVKCMMYtbW1jmNGjvXiAH7gI/ADmUuzQpkDcXa5OY7uEVAkRgiV8wO0tDRj5cpmLF++HLU1Baw3YBBHbApehC6FtC5XBjDi8J+ikO9uOMCE/4pzXohy6XPMvPuCTO44pTHHWNq8EldfeTn+NPtFxKGsNN8XAtGEMStRhCqbzMoFu64l791zwpP8gJmkhcoio3p2C0+8ubfGvPUa7rt7H2v5OOe5HIAIPnbcOJRbWhHFRQuSLMAIDDInolxxmnqQ/M3f5YiF57HOuhuY+ZK1q2NI3tvWUui15d3cKwh4IS5evNSCUa9BYB6y4wjU5+qQD4C6IKEPLZS9J9+E1lYDgNETbotzKGOfvYfiuIN2aBPhbyqX8dTjv8ctN98ILywyyxYNUgaiygLHG7jwQl68aisUmUSUHcyIN75pcwxxADv5BlR6flam68Tp8S4As8fSNbIAcM/LXssFAP1dLJfx/T1GUwWA5ToqetKEkXmpBgCqO9h4o0HMQRigpEU5RTOVAFAJIAQAAubiJSuY8GTXu8+gukSxHGGX3fdEVC6jsZBzRHcJj/zxr7j+Fy/L/UdPuDUuBC144BenosHUTFhKAJyX/v09RjG7JGTlyUmcAzFyQ2lT+UNuI0NMkZXEbKjqJi1TOO3ZgMXVAfxICBUbIa6MSzNblGMogWkC3ZedQIcTpQBoKnBE6UwmP47UMybjLHNKtkwqPTP9ve46G2CDAevz38QtWUZTaIxEgS8iR/+RqOG/c0ZMEKf0PGyyySacqFGiPEqUkTOAJ9bO88WSRKudqNCGRBNQ1t9jnwm/sqlFVjwF57hmQF40HvqfvLik4VNeEM3vzruOSnEAerSVHjD2yLsQ50vwvj/+xhjxMjx257nIt1H9gSUrmnD4wQegXJJKoOYIGHv02chTlj/J7Thi+U2hB3cF5+KCENqnyp7kFYU+V8zwQ5OiRUSnkRsZXajJsexUP4TlKqZkio4nXcELpTSNy8RoJeh7TmQzqat25UtmlF2dTDjDtnM5o1vQGiFdAqTcBSyLc/kc8rk8Ghq6scLHZW1xK8KQrA8hFhXKqP5An/N+wOMPCjXC6YI8yqUyFi1aAgJb926NGLB2IxUmGWtKqqGiqIzQcAdRnuvw+bJmtBQFoFz5RIT3RNnMB0bPoPEbK4X0mffmvYJ8uYnjN/TaaeRoqsRIOABlGCPCLofeiHxdI7wxP7w+Xhl/iafvvrSN/KcLvPHmWzj3nDPsZG75vQOwxdjJtnTLrjKbTpbW4lUUqP3gW1ekgIKKRQQMRjegsqnYAxV+MYDYCmMDKDU+ZX3KTViZNGFt4UCJTsDnG3VaOQiNR1acjMMVHZbLkIQzjyPj0GMlSYbHTzTxhShcxETp24bLlGkUYYiWkJRhH19/1cSh2TUbPazfh3L7Q1shxCKUr+fjwy+a8VUzmYh1iH0FugGwmb98zkfeC1Egq548uT5pGB5q/BjvzpmFwMzn0K23QY8uXVOLkGj2vUnXoVzqDm+vI6+Ot9yqNy4/fXzqIP3w4kuzcdklF/JHOnH0UWeg93Z78oOllCzLPTT3zBDAsHyaHCG0ElLNqsoA0GJTS0gHAC6hdZy6IrnI09QmKiF5Jao9ZYitACC+mRA8EVeW2AYAWb8oAYBVIOIsQYS8H4Mkbc6PWOZ+8sXXqGnsxrV/BACqNVz89UrEXgGxV0ZjbYxN+nWV830Py8oxPlywEi0RFTWSu1au76vO5InJm1NO5qMNAAqej9ocMO/lZ5H3ZR4GDdkCa/Vcsw1tL7zxccx69TN4I4+8Mr7szEOw49D1KwLg+Vl/xFVXXGLhv/MRZ2CT4SNB6KaKFa1Fa1uYkFxOVrAsGWJl8llWvBJOOQCVSBFxAiOLbeqZLQ5Nr/QsB8gqRQngklUuLFWjnwaIBqBJVbOMn0x9BoPxghJv4TIurmWUyqf6QowuNTGef+ZZzHr+JfTo0QOlljImn3oKSswBxIH21eIy4lh0IKrE6paPUVfrY+GKIlrjupRSSIJSOJeMW8ZBABNOQOa6H4TsCMt7ZMaTHhKjPu/jzT8+gxry7noeNt98CHr16tWGts/P+RvOu3EGvF0Ovih+7O4L0a2mIv3x7HOzcM1Vl7GMpRW//aGnYrPtd2cAlIgoqry1UxKiAFBZlgUAs1LDsqi4hApOFTAqAlSmJaw9LRLacAA1Cw1HUKBkRYD6nVRE0Wfj/EsDABFq45BXe9d6SY77cnnMetOSlSux/ItP8OK038ErNIiy6Pv4+5cL8aNzz2MAtIQBliyLEMUEAipfJwD6iH0qJ5fSdJcTVQMAyX7WNaoBIOdj7gszmRMQUAYN2hxrrbVWG+J+viTEISffCm+Hg86On//NVawoVHo9++zzuPrqy61GvOOBJ2DALmPgUek0USfKFJRkgkhKQCIwsUhlrbTCeYXRcjIvRrhJQlWRoYZlIqOdyCWv0MQMSrNyua7rH3B/t+PQChq1gMz16GxXF2gIYpw56QgEhRzW6NEDH/7lE/zi3gfg53L4YulyFIIATzxwB2oi4Yw+xUxCH4OGbYMNBm+LpSvLojQb3UQTMtxnd0WbiAASmULwhANEyNMcBwQC+dujLG3SCWIPdQXg9VkdA4BU9j3H3wxvu31Oil98+Kd8g0qv559/AVdddZkFwPb7T8GGu45lHzk9qFtTxmZcBgC6YisBQB5QRIGuUBIRUmzaVvljzpGxewkACTgSU4q0c53A7CTL8caMc0CpMp04QNrI5E4AePah+/Hhm3NF6/cjrLPh5hg/8Wh8ubQECqjkvBh333gN1uzaHUG+gE+XLEXPPv0xap9D0VpsNX4EoyNpebcDfgWA+FnSACCOIDpAxAupEgDI/V1X42GuA4CBAzdDnz592pCWBNHoI6+Ht+3eU+KXf39rVQb+xz++hMsvv9gCYOd9J6P/yH0RoIwSNVUwnSz4Dk63CiWoKmWq1WvOqRKO0Mt1B0ZJK8TqV9CGENVkfpoTaKKDS2weQ0bJs8CyoJOJZZZLLnHE6NaQY5X8y2Uc8hJwxhFqciEO33sPDBuyJSI/ZJf0VTfcjEXLgJXFIspRgNpcC/7w0G9xxOSJ+PCTleyZVxd5doEQwW05vGMduStfRQHJfuYERgeg4QsHEFYvuk6EhkKQAsAmmwxEv3792q7tOMYeE66FN2zPCfGcp+6omuf/ElkBl/3YevZ23ncKNhi1L/y4zF0wyANgTUEDgPTdhFDcz4P978ZutZ6wBAAs21i7Tsw+V+a7clx6DchLVkwSVxVOIVzFaXxi7WU9h6/HZpaPLrkSHnvgflx96SXos2YPfP7115g2+x3EeUqIJUYXMVB7d/cx9ajx6Eb+fAAff74Iv3zgfny0pIzIL/B8kMukzEpiBD9KWu+w7HdEDl/XPAMtCF35Mm6dJ/lefydC63EdAYD8GRtttAnWXnvtygCYeBm8wXv+MH7rybsSIzdz6OzZs3HJJZfYoe5w0HHYbKf9EHplco4itPOerkHTvAHVtn31vnGYgoIYhtAmokcrk1gbdRkQwshAiK4ugROZLzfWidJh0/0kmJIWDXTBHPU1MqFpcmXLBUIUvAB9u/nYc9tt4dfWyoQjwMHHHIPRYw9Biwm9MIDjEh684w588PabWLJ8KRYuWYpzLrsSfftvytFSlvEm4GQuLwAynj8Jg9FLs6xVBCphBbicvsf+DHG503zx94GYm6SsEgDI7mewGxleX/Aw7/lnUJeX62244cZYZ5112gAgDmOMOe5ieJvvcWT89vS7qwKAIkwHH3yw9XiddcMvsLKmP3vxSrSqTbOopNeNeTBtfmSeVx0+RBgGgMajDSdg048cgmr+GAKRjsBAMBOh11HR4ip5PEHm+IAaVtgYPjloIhRo8vyIPX3NJePJoxVFEbqaGHvvsB26NNTKhEYhFi5ehul/eh1NRerzo7GNCI21HrbbbCAeemo61lpnfbQUgXJIHjYxF934g7rCFQAk7ehvrarUcInrkBIRoN5SIqQczX4BBoCYynnmBqJY0+/k7cwHMd5+YSZypBTm82wF9OzZsw0AiDuNOfYieAO/d3g8fxoBoHLe/9KlS3HIIYcwiyWZd/LVtyLstjGzRH7eKlE/946ukqYESh5QOYE8lPUAaqAkY8bZ1jGWxasrNJkwQwdZMR7572P06BJg0uGH4a/z38fKlSvx3FtvYWWYQ6AszPfQvaaIvXbaHjW1teya9cs+9jzgEBx83Km2a5d0/SLfu5iLmtBCYGMWb4EiHFEBoKCwHKBCxw6eJ884xoyoEJYvRCbCs+yn91zMIokcXKT9E/HJI0kgf+uFGezCJgCQErjmmm0dQRSUO3Di+fCGjD0qnvfYHVUBsGzZMuYA2sXrpKsIABtZALSFVvqbrIbOYYoUwg0nZuJT5k1auaMJYA6gMtH8rt8pB7ArxFgz9JmJ48fo0xhj9y23RF19vc2oOefCSzB4l91tsyKSzrS6mr9YgAmH7M+++kVLmvDw44+gfu2BJOwM4OUGoTZn0kQVAwAOyvC9Tfs7Ui81ps86gPnd6ciRmiOjDJODh15ZABChydVcCQA0fvIAKgAoNkAcoBoADj72QniDxxAAbofnZ9uNCWGyADjxmtsQN26IUO13Tzxb9pVRBPXhdOVbQiqLI7lqPH/8wOqj1xiBMQeV5SvhmV9xWFm094Y82Pv1xtw3sPHmW0sql0AHQS7GB2+8iB+feRrnMdCruaWI38+ajZawIG1WDGGIYzQGRZS8WpRKIVopaMOizvTiM1wviXLmmMDUz5BXuQlCdQQEnS83WMpzo+aviVNI6pb8o5EnAJDIn7B+yfwhABAHmDdreoccgKB48DH/QQD4YfwWcYBOAIAGPfXqWxE1DPhGAFDii5YvK50HTrJfAWEcJbryE7MtzQkCP0TXPLBwwV9w8Lj9sEb3LuyPj4olPPrCS2imQIomq/gRaup87DpwENZaUzpplMIYi5evxKy589BUosCLydPjNyGEsO1Iau1NXoEuXAKArGwBAHVIEZafFgWqG7miwBKf/SYGpirSUkEssWIUAGTyMRCYA1QGAHOAWTOY/ZOuQxygkiuY4p6HHHs+vC3GHBG/yQCQ8G325eoApLycePXPEXVZB6FV05UDkIOcMZq6BLWMo5eGd91oIMkwdvs6gHB993SeKoW68gu5EJedfTLmv/466mIffj7diZRWw9KVKzDj1dewtJWuL7pB7MVYo97Dbltvg002H4Tbfnk3ihHQUmrbf9DVa1TG63dKSPaCOjqQcIAc30c4gkyDigI9lqIH7elNlkMq/2JrwMh+Ij4HoOSZKnOACG8+Px2FfKFdAJAGd8Tk0+AN3uuw+K0n7qwKAFcE0MBPuuYXCBvXXiUAyEo3ypoTDVQAsJmnfgEnU0YBoIii6/RqLOPw3XblHDlaPYnVoC1nY3ZQrT9gQ1z581+RsivuZ66QpTy5GK3swPKQc5oVpiKbDoRVkqgSpwSkVDh6kfhIOEBgRQUBQM4xnMUcn02Q0fuqqLT+CwcAWp1HHEAacUr4WQHAYOCkEtEBSAQQAEgH2GyzwRWtAALAD6ecAW+z7x0av/OHu6oCgMzAgw46iK0A4gAn/7//Qqm+L7RVKblIddDKcgX6kmzJLJxCkyY8qTI+b1aI9Qg64WJ35ScearFrG/JFTNxrJP+tXEHfraYdRVixYgWenfsWlrTITLoNLcSkS/O6agBg7cBR4lxOwCvdMLzIKIVk4LEr2VEK5Zy0MlgpepqAIPFgqoeT5pHlv4aJ6W+jA7gAyOoA1cxA0lbGH38mvEF7HhK/O+0ejkFXejU1NeGAAw6oCAB+MKdptHs+pScIIclUSQOAvicAMGcwrDSJGWh41lgDNigj39fkI4wfvTPqa8RsdTVoFwA07umvzsWKssnedbLi1OYWtqysOo2IhE2nV3CWE1AKGV/H6gJyPAGAsp5dZdCKAVYWM6LSuqZlPOrC7gwAskrgm88/1SEHoBEcPfVseAN3PyCeP/3XktpS4ZXlACwCGtZGqG3GTF/gLDGUA2hmSiL7RRRw1o8J2KR0gAwnyEbtKAZ+/Xmn4sO335CJVyXKccCQv2JFSxF/ePk1lEyFh61o8GO2FkguMJEp5dtpf0xE4/RyQx9qHE1jKIVAiYimSaZewP6FiIJZpmCjWA64WZPa/67+oP6BrEjI+l/0eW3MxIllVOMAzA09EW/5XIzOAIBEwMQTToO36aj94/dm/GaVAFBu6McigFm+UfKy2OEEJZMcqna/uHSFYgSAvLp9jV/AcgxHKUxH7SQa2MVbgqPGjTW83UQSdeJNJ+8vv16CZ+a+g8iYpWJNANMevBc33/ATSd+KY/Rbdx384v5Hbe9iWs89CiF2Gr6DuJTDEM3NLXjmtXkokuPFKH6U0bPH8O04N49epVIRM+f8CV+0mDoA0omdiijiBJV0AhcALmdS7Z97L6uFUEUEuADIBYkO0J4V8A8BgPCqjB+A10POhnPp9yRaZdyWEfmxE0dH2ixUH7/QN8ndKxrFx0MNVuCoH+ydYv8ue2Vr5cyzMWKv/ZkX0/VJNtfkQ+w+bCs0NFD+c8RZtjNfnI2mUPQVfhy/jB2GDEKfNdYQDhMCt993DxrX3tiwelIegb/OfwXnnnq6xX1YKuKpV1/Fp8spBkT5TgIA1QVUpBC/SFkBWh+h5l4qeimlaTw/xhpQbyBzA0cHYK7FHCBtBVBGUGVXMHDMSafBG7THgfG7Tz3wjThAuwAwKV9iwkgYU8SErH5JDkn8AYlfQFPGTMKIWg++KJucd1dajAn7jbPeQaWCymeqwHl+9iv4slmSQxkAfoSucRl77LqD7B8Q+1xY8czLc7AySsRfYx2w89DBaKjvwpdd0bICz730BpYZbY8cPbW5EN8bPgyNXessRwiLEZ585RV8tkL6JKcBkMQHVhUAtvHrNwQAWQGVPIEk+o45+TR4m406IH5nRud1APIDqBnIqybDAdRxozoBs38br5ZEDBcA5KolX7b6wNXXn80byEVmhxI/QL/GGN/fdUfkKX6R2eiCEkGKxSJm/PF1LCsR9xGWTEGg6y7+T7w06xn+TK7eLbfZHudffZ0A0+T8rVHnYeT228MziF2yuAnT58xDaOK2FFTqXhvjeztubzaiEOIWW5rx5Gtv4PNlciC5ipXlpx1ElXdmyZqBGt5W3cVyAGNVUXyAzWgTO1AzsBAkHKA9VzBxp2OJAwwcuX88/+nO6wAKALtjxTcAANUUqAOoEgCEM6SzhvNmowo2BesC3HTNj/HBO/NRbjU7l3iSa0fuW3rd/9ATWFGmNDR5+fkAh479Pro0kou3hHIY4+HHnkBTmZO/TKVShAP3HYOe9XVcwUPK5KXXXY9ea2+CODKFLn6AT/7yLi4671zkC3neXaRULmHf/Q/CHvschrKpGaSC2iRY5JqFabNSF4pysQQILgcUP4b1ArIfQACgWcMJANJKIAWDKnoCiQOceCq8TXfbP35vZnUAqBlIMpMmuLMA0DRsLd9WViwyTHLdOLVaw/I2NqBp4jIl7OihNCitzjXHNS/6BD/76U9QKrWIMkqFpkbJZOUNebtfDiV8kJNEZG/EhNWYAAdnypTDJwUh6sm09j7b78QxjK6j43VctgSo5v8peLnw+lts4WlkACAOIQGAcB7DIarUWTP4WR1xs5UTRqd5AaoDkI7ATiHjCMpygGoAoPFMJg6wqlZARwBIWJauYE1qFCVAq48IAOTT5n0PeFuBtP2fMoMqAGDpgvdwyuRjuEbRKhdmGTGqFqlSAAAgAElEQVTxzIzThMpOQbpHkNYtmOieSUIlJNJ5NLGuQ0tZP2VFM3FoGxzjw7c+gThGS5zDQzNfBO9wxDEBAY66hPU6NjEks/mU6wQSrmA4l7FerFWgeQFGCWwPAO3HAoApJ5MnsJM6QMIByBXcD1F2syBlterbN/5qWvmSc2fMP0P4pFmRIF4B0EYHyHIARkwZyz77ECcdO5FzFXnydHeuCnkKTNCM509ZrvuelcNKaKsjZE5SABBHKXkF/Gb6LGmgwfEAwzkUEEb7J9e06xl0d1cTwiesnz/rAjFA0OTdrBVQjQNUcwWzDkBK4Oa7Hxi/Pb26FUAu1QMPPJBXEA2cHEHsBzAAcFcLy9p2AMDWAPsA5EGF3bUFgKsDMCAocdQ2aKAqnxBLP/kAJ0+eVBUAzEQdF26SCpZOf3Y9iVl5zNfIOJoUA+7qJwCEQS3un/YsN9MggyHidHnJ+WOO0A4A3DF0BgBJeFjCwWwdWRGQ6ADEAapZATS2404+Hd7g0fvFbz3526pmoAJAYwEKAJsPYGbEOisyZc9WB1BzkB1A4tBJmYGG5yWVQ5q2negADDDiJQYAp0w5xuYE2tXrrHQXAPp3S6mMm2+6CcccPQldu9UhCEwRq3oG3SzSSmzCiJYsAOJCA+57YiazfgofE+GjMNn/T6OC6hdQXcPdqSUr+5UDyHOLWEg4gFhTHVkB1XQAWleTTzuzYwBUcgUTB+gMAJT1i9Jn/AEZAKRLtxLHR8oT5ugAFLcnACxe8GecdsJxHQLAJRT7B16ajWaurvXxyrPP4aLz/8MGltwVX0k0iBaXhIFdEYCaRgZASE6gbwkAuo1r/mUBIBygYwC0pwNYP8BWo8bEf5rxhOzbJ0+o0pzf2+YDiA7QEQD4IahW3jpyROtXB5DNCdTwppMJww9szB5COMn8XCiyXhNFlix4D6euIgDI8TPr1dfRRP191MNWbsH3R+7KCRQuK042tjRRR9ukqDIAEg6gGUQBcwCzVWUqY4gLS6q04cvWN6gjKJkPU0VNxajEAdqIgLQfoD0OMOmkU+ENGzU2njPj8U4DYOpVlBBCwaC2DRqY6E4On3IAzWNXAHCSo7XzTXw7kwoltXJ0PQFA3njrFDiLP5rPACAlUGW3aOlp2W/NuShizx8BYAWr5OqaLKNL4GPwRhtyDZ32DagGgCRTSIDAvZCoUtqKgDQAJEs8SRlTc5A8gtmXKwL0N811TObjHwcA9gQO3/MH8cvTHrETmR1UpYwg9gR2AABrkxsrgIlpYgAps89wgKRc26Q/a3Inebr8kDmAcAaZ+KWf/BmnTJls8+ctCCoAgJWwKGIP4fOvzMGKMB35pPHU1gBP3Hc/brzhegQ500fX2eLW1TFcscLEJQDUNODeJ2ZyAorkEEoDDU0Z05xB6w9gnSM92xUBoOagLgieL6MXVeQAaSWwWkoY+T+OOfF0eFvvPiZ+dfpjKTnoDiurA3SGAyTaPSkp2oLFjQEkjg1WbihXP1OkmbiCKwNgyQLiAMcj5xQ1KgfgvHuj1CkHICIJAF7DslJiZythiRbUUCkXtmKPHUagtt7kR5iBZAHA+oIBlgLgnsdnsNnwTQCQRAKzVc9CDasEMtIlg/nbiAACwKSpp8HbZo9x8aszfld1+ydNCVMrgAHQuC7CIIkCurIzqflLHEBq6iUpYG1FQFUAGICQA4b1ByPzFn/0Dk49YSr3w3Ptdy5YzgAgzQFex9JWp/LIJmUaUPhAfVTGsMED0WuNNeEbbqAcRgmv7J8mkgCQq+uGux55knVEBgCZgqFvdz93zUA6VxNFVBRlS9usCMhyAJskWk0JXEUOQDrAn55+pFMAoIk84cqffSsAaIk46QCsC5jgRiUA0KqwKWW0a7jZRJrEy9d/f7sqAJRISnglFnGAZ19+DctLnObr6A58RpLahhB5H7jrxp/gt7/5re0lqCBw9QoFQKG+O3758LQ2ALDZwhoGNtnE/wwAtJ8TaETA0JGj47nPTLeux6wO4HIABsBVt3BaeJQrpVymOjnVHEGVdIDOAoAcQeTdVwDQvRYveBdnHD8VnumE4eoA7QHgudmvYxn5azPFpHJ+IhqoDUhN4GGnrYZgjS7dLFh0fojrcMNKwwHydd1wJ3MAKn0hHcBL+QHacoCsQypRCNIc1YzL8QR21hHUEQA4I6gjALTRAUxdQCUA0OTYBA7HFczmG9W3tWMGtscBKgHg6wXv4MzjT2wXALYez6RxkRXw7OzXsTwDAAseJwgTUwOmwMOwTTfEOmv1bZN7kBUBCQf43wOASVNP7xgAWU/gCVfehrjrupIV7FFgRIMssjYsIU07M+sHMEoe6d8pT6DNhDFmnwY7nBIvuo9yEDUzFy94B6edcGJFHSDrAVStvaWlhQGwwhEB7ooWIEjbusArc8oXhY9dpVaP12umRMDvpoOaN/MmTavIAZQDuaKI51PnZxU4gFYGEQegjKA1THaTy91p3MedfBa8LXfbM3796aeqioAsAI6/4lZ43ftzXUDM9bAZrVW1+W8BANZ6Nf2ZM4/bAmDpx2QFiBKYrOC2fgCXC7gAkH000lnFzHq9GPUoYufh26NrQw0nhigA3Am0XkDTrdsvNOKex56xAOAYQCQ9fzUWwFzDhAc77Qf4BgB449knUVNTw7pLNQCQDnIMcYBt9hjLZmA2KKIPmxUBnQaAmVytc9dJ1HTwdBVwmuB8rImCBaRWkw6gIjOmSL+HJeQKnnp8pwFAQCAR8NzLc7GMk0CSlxDeA7V8aGlehH1H74UgLz1/KxFfz2QnEBWZUH1jTRfc+fAM0QnYFexGA5OgEPkJ6NUeAFxg2i5lmk7n6CqV6wIoKVSqg2n8Q4YM5a5l2ReNmTnAtnuOi2c/+UinATDl8lvg91hfKoPaEwE28SPd+EABoF3J1UTMZsGqpzRnOAB1FmHWHlOnDmDJx3/G6VNP4GZMLouu5Al0zcCZf/wTVoRp05Empy4IscdOO3HhBXUIdaOVWTGhEVCOAhoAEAe465GZjgig/AInH0AjkyYcrABoG41MWydq/6uI0KRQzg7qoDCEOMDgwVtUBABx7klTz4C37V77xi//4XdVW8R0hgO4iNUHSrJ50yVh6aRQp/jRNm0yxDHRQmoYKVFDyQkMTEYNAeDsE05AbCqOqkUDbQIGdTUrlfD0i3OwrCi1YuSAImWva97HyG2Ho66OevWlRUobTuGEmV0O4OUbBAAedf+kglEBrFspJCs/nS5eSQdJRFpSaamOIi0QTQBg6ixM84hseXhVAMTAxBNPgTf8+/vEs6c93GkAuBygkg6gg/82AOBraFMkBwD0PXEEIvbST97H6ZMnWysgC4CsIkjEIgA89/JrWNxCYoXy6iLki00YM2oUcjXSGsZ2Gk1ccxYD1mvodCDXHTvioE50AGq9LzXPqRhAUjbethjVBYEbDGI/iPmxswCglDAVAe1xAGJIkwgA23xvXPzKk490CgA0qVOuuBle1404FsCTnC0NMzWB7BH0idGZ/vZOF6wkK9hxCeuK1x7B5ond1DDRBYQjLPv7ezjz+OOskpZV6DRsq02s1Gf/3AsvY2nRR00eOHz/sfj6y0WS3avJKapUdpBBlLUC4nw97n70aVCNICeEaF2A4RiaCURRQD23Lft3excL5W3UwkZHk7yAajqAVge3pwTSNE+ipNBho8fGcygWUCVJ0RUBqwIA7uVjAFCtMCQVFLKpUBnXrjZuMKlh2mL26w/fwrmnkB8gXSRqV5OWihnlMYkJAI2NjVi6+GvkqITapJRxVjgPyFgH7QDADQYRwFgUBHW49/FnLABsMqjtCGKigtoo0okEua5slfXZpFDNClalkN6r6QCdKQ2jYR099WQJBs2Z+XjVrWGzOsAJV90KdJEOIWkOkPQHcB9IawOTmnYhWBt/gNMsSiYhUyGksplarwURPn33NVx89hnpPnCuZp+t/rVWRFKl665At5k03z+z0VWl1DCR8SbS6NfgvmnP22RQjgqSCWiUPgvATGWQFokqi9f3Nn0SOskBSAcgANQUxAysnhYeY/zkk40ZOOOxbwQAnm9bF5BuEKGaeRYAgWkEWQ0ASWGJKn/p+gBq2Ej3XL97DfYasSMDoJIJm00Czcb329hFdjs8+SV7vvb20e9dEUAcYPuRozH53EvIhST1ABEVk0r/eJdjqPavloRbJey6otO9kZJoIHUq4ZqAKuXhLgDazQhCjAlTSAfYY7/41Rm/4Xq+Sq+sI2jKFT+F320TaYzAmnnmLFOMmQDA6AKa82fCw5rkSdEeqgxKVrxc0HYD0/54poGkVhB1q/Mxbsct0dhQx42snW7wFZ8j+2VmI5NE0ctyjmzQ3hxpARCHaGqhtjSz0VSuMQRXbV96BmmnUDpVS8OS22jBbOJRFasnHZsQrV88g1wM7IgA3p+IAmWmQYRygPYAQI6gowkA2+55QDz7qQc6DYDjr7wRXteNpTEC9+TLzpgASQFgdQHb+dJ0u9bmT5TaxMRVlp8AQD1zYv4lMpq/90OsUQD23HFbrNmQgxck7c6VOJWQQL+RTpIFABGJrQDT/ZzuoauUruNeU5+N/ADLm1vw9CuvY1GRgj90ICWCJOFetziUFVHVAezgVIdJzE/6SRpg8p1t5nQ1ALj9ATrLAYijsQgYPnr/+JXpv+40ACgaiC5iBaREQJXuYGzW+KHZjNLdMEITRLRSyEyA7Q6W6QWsfgJrp9MWKhHq8wUUolbUF2SzS8oapi1s1MGkaTeumGDCmibRtkGEIbIyNCKARAUkq48qfqkCV2U55f63kvIX1aC55KR88YpPmj8lOoCcaxtGVG0QkV4IiUPKcADTDYyepzNKYNXKIAATprASuG88Z+ZvVwEAiRLYWQBwUie1tTIpXbqCxBw03ML45lX2uc2hmKNUAAABizqW8jaoKipC2sqONlDQ/gVpn7/rqNKVTd8pYbRvgP4WqYzjlZ24kElsUT9g+p3aw6rSaFO/Mtq/dQwZhGmvoJQiyvdKd0ZJ/AKyN4G2g3MB4IoA8gO88ZwogR31B2AADB25T/z6s+QJTLcsUQ7VJhjkigA9yBUFDiew1gCnfCVigVm6mSDerp57cqe3elEPoGYpJ/4AU0xiGyqa3UNNi1j5Wrr+UdkUEYhbrVMPH9pbh/enojayRDaqH0zag9A5ZdVVaGMqYsNU/EkdjImLUbFHILt5ISoh5HIyCfrQS1i/ERdWbAjFbfBIgVGVA5g+BbZVrMmR1B5BFThAFgCuH6A9DnB0ZwDQxhX8DQFgGx3oBgh26xhpfmz7AhoZaV3AyvKtP0AmSDvanH7cUfjlnXdihZZvI4d6r4zN1+6Ntxd+jaHr9cXc9//Ou3cN6tVFAjdBgO133B4P/mEalrYIa24IPAzo3R1h1CJtYFCDoJDHnz/9Gi2hh7r/acF6wOhdMP/tNzgR5JobbsDYw4/BijKJE3L9iotXAaC7k6pH0CqNJjqY1v6V6K5Fo0Wy0gbWNokyKXJul7BvCoDxx50Ib8iIsfGbsx61ffGzilObfADHD5ASAYkeTRv+Jp/MikqUwnSrWGZl3C/QdPKgtuyk8OkKV8+gBpc0qGLMv54NMUYOHYInXnnb1DTksF63GI88+hh22mschvXugpcWrEChJsKQNbrg3S+X8dioK/mEA8bh2ptuREPvDVBXG2LSPvvjlgcfETlvdkIheR4UfAzt2x1v/G0hwnwNK2i962MM6NsPL77/GUrMddKNIpNVn5b9bsNIVjRtTmICAnM1W0XNuRDaJbQCAHgOK5SGte8HoD6BJ8IbvPOYeN4L5AfonAhwHUGrCgCeeN0ezikhcwFADRi0CTJPhI0FCKYUWnbDxVpguzUaMf+rZVhWClDIR9i6T3fM/2Ix78uzRa9GvPLJSuTyZQzt2RVvfb5c2LUfoKEOGLjmGnjzs4XI+yGO3e9AXHf/Q7IdrgEehXXfe3kmdhq5BxYW87bLOQoRDhuxAx57ehaWlAtJtzGtATTxApX9qmO06RNoKqZYcLn7HNi9FdPdwFlnaqdPYKfLwyPgyCknwtt8570ZAG33mZYJ75AD2LXuWAWZjqGuZzDbN1B742rLWDdPQKS5YYUZIIjThERHgEXvvYQthg3HpytjdK/PYZMedXjzy1bU5iMM7tWAlz9pQo3vYWivGsz9solHnItzbEVst2E/vPzhAjTkAxy5z1544HHK66OMIJn4Rc0RhvbpgvkLl2BlmXYBNWYu6F4x3n/rTXRZfyvbSlZXvrX3DcdKWsuqjpDpc5jJbtadQtQcdDkA2fvVrYDOVQaRQnvk5FPhbT5iTDxv1qNVAdDWFZwxAxUAnunUwd6vdMvYSgCQLJ+kh4/tGWwyJnQ1uNvNCSA0i8e0gPU9dPFbscXaa+LVT5ah9MUH6L/BJvhsRYDGWmDIWnV48aPlqM0FGNanDq99vpy1eQXArptvwADIRSFOGn8Ybv7V/fxEpOXTdvZftXoY2q8b/vxlE5YXiTOo1RKjId+Kd15/Hb0H7ciKZsL2HXtfewxnOoZmdQB3Y0uZF1MmbvYP6iwAanIx5lJGUEeuYMQ44thTSAfYL35j1oOdtgKoWbTtFp5yBFXuFayszeLEegTNRhA2eVQqh7R1jFUKTe9d2+fPmknqIJGdM3deuzveWPQ1hvTpgTc+Xo6W0OddM7bu14AX/rYUdfkctlqrDnM+XSp1A34ONTkfg3vX492vWpBDiCkH74+r73uQh0rrU3z9OVx3zlRcc+st+HRpAFJOWUfIAQ/dehWmnnwG/t5SsB0nq/UGtlvrcN8A2VfAnRu7s6rTgYStJZvvIM01KrWKzTqC5lGvYNp+t51YALW/P3TiWasOgBOvoW7hpl18JwDg2rkuCHTfAEU+TbgWjjABnPJx0QV05YuoUbOQfADkZxjQNcCfZj+PSy+9Gj99YBq7YAkAw/o1CAcIAmzdtx6vfLyYCVCTzyPf8hXO/9FFOOfaW9GtJsTUww/CFXc9IACxO5j76NIYYLPuXfHBkmYsXlFk0VCPFgxdvz9e+mgRiiVSKaVQplJvYA0a8e+qXGZkf+V0ehJxiVOrmh+gEgCo2FU3jKjUI8gCYOCoA+P5M3/r7C5shXpFHeCkK3+GUrc+tjVLUt9epVt4hcQKYv9J+zOzkrVDKG26yH66xC+ggKB3Wzat5qHRXWsDYLv1u+PtT5dgcTPvtIN8AdhmvV549cOPUairwdDejbxSPdpg2svjiekzsfamw9AahejW4GNQzy7sRBJvH63yAPP+thCtQQ5dcsCgPr3Rt19vLPrqKwReHq98+CmWlZNiVBcAagZSjSCbgA7h3Rl2xSOveNs23+xmpnkSHAOQtjrt6QDkCp77zDTU1ta2CwByWux73I+oT+DB8bvUKrZtsSqPs01dwOW3IVqDtiEzzg97XvsAUBcqsz3SYrUDpiJceweb1Z51DKku4O4foLECmdDAbKxEqeoUHCHXLW3WSCMNEJPXkJo35Mj5QwEo2vVMduPmOINPQqCEyJOaQPXs0c5IHHnnKiXPNLXyUCwS5SPEpmWuyn87L2oFVAGALpx0PYRE+VQHcPMocrptDDmyTNyuUrt4jgU895QVAdWKQxMAjD4yfpe6hWv7qTQDcKwAofTUy25C2GNtuxeO7s4lWxnRxKk5qbWDNqnJBI6kwYMXUacuyZxNs3yj4Rq3a1b22+3UMlvLJMoh6Rb0yfjUreggIutmcxrAShwvuuopnz8BgFNkavdWFQ5RrfevbSNvVhR/NuXhdN0wU5Gkyq46vmizKyW8OHtMOFyzpE0YWKKBpCOY+eJMatrEOsJrzz+NhkKedYCNN94Yffv2zVCVPpbwg2Np17Axx8ZvP/bz7D4P9oQ5c+bgggsuYO8ZPfhuR5yOAdvtzCyNXpFvZJ+zc4acbMMqfB5tzaZcgINDZuUnvm/dLEnatkqvzWT7eFqXHLHTHUUy+wqol14TT/QB3DAxXTfdkkWijLp6+Xe1/zMckYUCo1UATeVfBrp6AWkRp25fbTljPlPvIHMjO7fW0jGBHXXoiHmrpp4RBXbjSGpuQYuHdg2j2ZCFRBty0LyStfP6rOdQm5P5XH/99bHBBhu0BUAMjD3uInjb7Ht6PPuha20r1+yRBIDzzz/fAmDUhNMwYMvd2IMlrdKTOLY7QbIdcqIUWS03lXVbtjLPEkxz88wK12ZkugeQA6t0IoimctnNpdJFH3r9rFLKlUBOzN8t2mRA2N+0vZwx94znzz2XV7i5kQJErQIbLMpkHSf9E4XgpOipCFCdgJU8FQ2+bK1LK104hdmY0/xem/OYA9QWpDvb+uv1rwgAGucPJl4Kb7uDL4xn/foiVN4wBiAAnHfeecxO6GFHH3Mm+g/eJQWANGhkotwNJemzym417/g7kssOgOg77fhh7WBHOczwldRKsrl8amam9gcwewQaYqZBkC4SIQ+jRP0Mwe1dKm/44OYJuADg8jCzY4grMlzG4vpBJN27LQDcLWP5eNMaJqd+FNJhgsAupLq8hz8RAPICgP7rrocBAwa04QBFxBg38Tp4Ox55afzQreeiT2NlV7ACQLpoAt+f8h/ot+kOzJsZ5XYLGJ1IUgbzberfdQQ2XdwS3nQDM44klfEJkeS6HDFkhU7zBpQBJwkpohQmx7uErmaOarTRynQnd1DH7HI23T2Mwr7C4eT+YaYhRaL9y7wlO4bKVdUDqptnazpLzpSiuXsGqy5AeyVQY0wOo9s8i3QvZgbAc09z1jNzgCoigFTbccdcC2+Xo66Jz58yBqN3GNQGJfTF3Llzcc4551gRMO7ki9BrwDYdAsDdQNG9sAsAngh16YLs66QBhMpHGw4m7ZcAoA4Uc1EFhFwrDQD7nfUetn1EzUSyu4GpbDcOm+QMY86Z+2vcX0GgANDPCgDyirp+AL2eLZo1K5kAII6fRAQw4Q1r502iDQC4yjoDAO3GXpcDXpv1DAo5AcCGG26I9dZbr82DL24BDjr+Bni7Trgh3mnTbrjsR+MrAmDevHk4++yzeXD0IPuccjF69h/OKVm8ebSacZohpMoOrcRYTKqsnGRlx+b/60o2GrfjKUxAIDEBBoBVAjVzxnAEwwjcJlWVAOB24nAf2GbqGAKrkpsQMq0DUJ4Bp4Nrgkcm3dtaA3aDyXTqnMQaSNmTzbNVBFgAsLbvIWf8/uIK1nZ7ZLRR80wSmeY6hoHXGxFQMErgxhtvinXXXdc+qo7ihXcX4tIr7oU3csItca74Oabfe1HF7Np33nkHZ555Jv9GD7z/6Zeh+zrDUgCgq5NzQqit3bcSACgIFET0Wc0bm/9uRAL37maHiLECDCCoqzg3iDC30U2nbLDIsQ6qrXwBVFrpU9GQ5QBZAOhnfRYCAAHbBYACnWW/40p2v9exdQYApOWTK1g9fZzyZpJCaX54y1izfTw9FwGkNgfMeZZEgCwo2j7e3TxaAXDdvc/jD0+9A2+XCbfEcfNCPH3/BbaRs7sy5s//M04//WSWPQSAg865CvVrDeGEULICdAJp73p34tUM1G3WEv+AXN32ErJl2vK93STJsm2zT4AZVBIjMNchjdjNKjbHZXvuZHUKfUYivP7GhHNTtgz7dn33SkwujM8ez9+pzE+UQLqX6gDEweg8NttMZhQrf7yNvSiBRHDaGTyr/TOn0NgA1R+bHUNFdAjBaffwV56ZgRojAjYZOAhr9+lnrXIFwJiJN2AZauCNOOq2uFAu45F7p4I2Usl6bt97732cdtpJNuHhwLOvQkOfBAAJ0XUbdCWkkZlGm84CwHr6HGWwIwAIy1Ql0CiPBgB2N3GL3nSatX7t1t4pYV3ORJVG3MiRo4FCTF7pTvazrHyT7+8ARsChMj8NALewhDmgAwCx40uG8AIA8ktyo02zsikrSM0/nQcBkIoGAURdHgwAsgKYA1QBwI6H/hR5KoYdcdQvYkLWkQcOxsRx27jVaPzw77wzH2eeeaqZiAAHnHUVGvtuzq5VmcBs0qWdal2zcq6z5FxlTVKdpNScAeAEfVwdQIGmQSTNKSxYMzETLHIaQKStgbQsThxDidnHz2VWshLebg9njB27CbRt+iSEV8CQ7kCv0PEx0O/E+umlm0NTOTptA58zHj3y7NGGGqT8KQcgczlg7Z9kvvEMqv3PzbbIHyDfU3e7F5+ebv0AmwzcDOv0STyBNMstEbD3pDsk6KUAWL5yAWb/9j/twJReb775Fs46izZdphUd4MCzr7YA4AdUj51lHSoW0o4gZS3ECWgikpQvVetNEmYm6peIGJ04k0dgHEUKAF5hvBuoiRZmOoBkrQpXBMjfyrHEpa3BHAWAbhBp07yNnU+ppUx4Iy70upUAIHdRX79po0fZh0Gy8wfZ+QULANlriaqYCTACAJMk6ngKlSOwDhAAs5+byWYgfb/xpoOwbl+K3ciLZufOh1/CnY98IHTY5Ye3x3RgixfhxTsnJdWo5gSyAs466ywrJ/c7+TLUbbi1rGmzTYuuTnlAU1DhuFSZiE6eoCz1JBuXiaOZP2ZJkl+br+f68p1ECRqopJWbxBByiIhbwow18Qfw7cz1qqW+adVu282hE1Yv5py4tLkLSBShrGFj6xqXidNMYdpLWF4mxpDpg5DzApb3pBORu5wIXKDPhgMQc6TfSdsrmC7rdDXxGDqdVUztYL0fY/asGSgYf8JmWwzFWj3XTAAQhdjh4IvgN24K3vWVAECrm2ru9h03BCcfMNweTH/893//NyZTHb5Z4T0GbIYDfnQdZUUDntMqzjynuDwjxMZxRIEQ5h5UWGFMH+UA9E55/fyuZiGtbDb2Da+1nTolcKTKI71L+5ikrTxbCXYrF2Mm8tSTS1f850lPI5lAYdsEP+PpM8UfqrxSmjhxA2L5EqFQTyFdKebu4EJxjY6qjqKJH4bj2S1x5Dh1ANF4af9Eep7aIKWAXCcAAAreSURBVAeK+uUYEOLhYwDQ3el7LrZXEWDe1WXui3Xz0YfvYekXC+AHeZ734TvsiIYaan4jr1YAu4+/DeW4G9PP2+XIu2IKIlCSZOvKj/HyA2fDp5QuG7uIMWrUKNtNm1jtshKxI9nGjfLqhQVJFIvDJdwhq4jm5mYM3WY49hm3Py74z/9EQyGHsiqFGV7DbgPSqg0BRDv3kTP5+ap0cfNo02lTXLaJc5WBpNYop6bRWDRRQ4ltAMmWAxHOKHpKeNPSTieME04yCqAAmbR4AZdwOGNe8nUYEXwMZRUryKUHsdQl1MYRh2y32HorWRhBgHyhwCFstgZyktHTs1cfHkrel/nVRaSeQjKPrXjzgIacLzoBxw9qsMsuu5jxyeYXN/3qSdw77Sur7FsAFEkOlT08cOt4rFNvepIZDXibbbbhzQeZ4MYZEXpmowWfJlSQSiuMnp06co4bMxYbbrQR/LCZMb6y1IrbbrrB5AsS3dI++JJRmvK2iZJRqMyeQDmTK1gye/rQcVkHk2uuMQnMZlLCgY2SqUWwmc+6kBVAruKYYomZDwR2vrz6K5Th2wzfJPlTCGU2pyp4GD58OHMxAkTOD5DL5TnplD/namS3r8Bszu10KxPzz4g+4nlu6r1u0UMWQW0XbL/99oZDiSk6/JDLkC8MYNnK4xlx1D2xu/dfqfgJXrzvbPgmMULFwJTjj5EJ94S16F5AdCHW4k24duKk8YxsdXXyGuGlEuDaa6+1LJrSv3ni2ptdw1orEYO5RYXKXSa6cVrJDeQ+7rFZxZJ/NyLKDkdFT9poEFA5Lue0hSF7F7mASBGHzLacmHmbbDyQmzdRgQsTlAI6zj8ivtulTFe/ywUSxTYBIHNj3vLWx+67757MbhTj9b8uwgmXPgpEdXaMbQBArPaRn49HL2czcWK1F150HkcG41DsTbKX6Z1a73ft2hXjx4srmbLjRFlKlB8FAOWoXXPV1RLXNzJRgyvVcKAtZvR319TKfudewxLc0xSv9B2qEa4aAPR6IvYSK8YltgBaREr2+kqsXN5HfX09thw6TI5T84/lvS9sv2dPTunS6yhRLft3REEWBEz8KMJuI0fbhUnXIWV1xKEXIypsKP2XDOfwdhx/D+sAdpUQgJs+xbO/OhN5r2BlBV104cKFOGXqFCxatAg1DY0YPXo0hm27Azdf8qMSs37aj9edILsi44D1iPnv/hlPTXuc9+Gj83RCdeKpFs8lLLFmXQk8RhtmlePczuDZlc0OHVspnNEVHN+9SyzexFTZeQUO40443V+tHrdRBXNEs8mVjlejqXT+mL334Z6FNFd6vSAnnLSmtl4yeIxMSla8UWqpxFEVbAM0ugYBh97X7NUXQwYPcdDOfcow9y+fYurFMxCS989srMHvO024NyUCqG1aLmzA1ReMxnYD1qjIoy0bzCzb9HyleWfSoUNs3qpbpmRZbkZGZDt3VOMcHX2f6g/g3LNDkZR9ZnVjVBl3xfFyvmJlsZQ2GtM3U+XS/TYrHjPDYLulFQFGHnYJ4vxGINHLHEWtpZ2OvjtzjonKtazAM/ceh4KphOloQlf//q81AxZIcYTT/9+v8NI7RXhxvWm7S2zO6CqVAMCsNQ9s178rrjt335Q8+9d6zNWjqTYDCoC/fLkcR//oAZQjaXrNQoubXxjrhQAgcjjxzDEA/Bg1ZeCOG47EBt3oco5W2M68t8fC2iNXW9YlR2e/t7pClhVnPneWlXd2vNXGkX2mzt73O4cubWcLD3scdQWioH8iyq15agCw88R7zLNltGXuxuUhal2JGfcej4ZKBR4VnqKzE5o9dTUA/rGQKMcR9pp0NZqi9YwprCtfldwKAOCVb12y6tnyUV7yN7zw4IXso66WP97ZFfKPfcx/n6utCmchk++MK+7EK+9TNbNxTRu/Dju+ncXsuRzABQBVjrBo4B0qffTqUsKvrp+EQhVOsBoA3y0YOwsAIv5P7n0Gj8z8CPAK3AybXp4DAPlsOMCuE+5hHYAaHomnTwPe6mnTYIuP9dZsxR1XHSvRqTZp09/tBKy+usxAdSBQ0kqEB6bPw60PzENk8jE9AwCt3LI6lAJg5NH38uIlAIjSZZRB65AxLIRy8pBDvx4l3HX10TZ9bPXK/+dCsxoAaOXf8dg83PXoG4i5YXe6RM8FQBsRIGZB8lJOkPKfGx84ZafURYvx8O1noI7PSg9pNSC+G0BUXfm0RxF8nHDZnXjvv2tQNgSwK19D3hW6tzFHIR1AfPXpPvacBed2tLZNnCjkSnVhS/Dw7adgzUyH2dUA+OcCoCkCxkz6KSK/F/Nuqaxgni4cXT9mGnlaUTBikogAdfq7UbZUtM22UTdhxIgaF7Zi223WxY+P34stBEqi0AH8XwNCdgV+189XXdZzHJxX/cN/nI/b7noWLXH3lMdElHcj0m0r1GSlcgzAIMAjALgiQAGQDbXaHjaa+EjlyZHELPzyYlx+3gRsu0k3zloRXeL/1utfBQCUZLqw6OGw469E0e8HjxNa0rPtAkCpoBnOstYFABzT2dkAwB7oxNkrxdDJSuATM4WWeYruFT/DtT+eiq026iEtYJkpcRqkRcO/GjA6Mq8669iq5sjqaBl06OmkBc+7kMX4bHmIyWffjKZib5QzOZWWfu08kBvS1iBVRQDwCjZA0HCt1RwdUeBekOhMrVTDOEQcLsWwzdfDWSfsi16NPmo0ieJfkDP8qwKADG1qV7M89nD7r5/Bo4+9Bq9LP8TckjxddZUKqdv99dqHngOAX3E+gCYyVNUBsteznCCdjaJAyVFPHJTQGq5El4YW/GDvHbH3iK3Rp2ueuQN3A6X4Aw048hBRShqJFN6HQHgGPWrlXQw6Wlff/nerO6ms7OCSbVeyyUSq0oCTk5SJOfIKpyxjSqLx8fHSVsx88Q08Oe1VfLHEg1/TAES1NqXNvU82FNzeEFMgcQ70RhxzvySEmFSmShk3FS/sAEDlitUsOd5MwaOQe+jkY8k69klMUC5tuQUt/hLUBj76r9cXGw/oi4EbrYsN1++DtXo2olteOoJSHrytqDFZud+etJ27wjcFAFcFcqdZkbOattpCenkELFrcgo/+thDz/voB5r//ET7825doavGQjxrh19RyEg6lp1HuozSsFxmv/pmsK7czT9MeULwRx/xazEDb5kwwZlO11Hww2bVtEhyqZGhQd24xL8WlrK1YtCRMk0riyGQQadEmH09brFKDpwjliNtwIUYRVPnapWs9uvdoRLeGenTtWo8e3QtYs3sPdOnSiMYuNehaV4ee3bqhS0MN8lQjb3vLylRJYYawGEoqVkIzy42AYgkIyzFaiiU0N7dgRVMTWlsjNBVb0LRyJVasKGFJ03K0rChz1vNXK5qxePFiLGtqRstKD8XWGGEYIQK1j42RNztk+j7tJkL5f1p4YoIzqq2b5NfQpjWnK6uJm9oHUK7kZi5pvwKb0VRZe8luDubtcuxvzJHa1Cld9Gi1R7M9XHYzpWrBIa0F1CJNt9WLy464Uz+7oE2Wq0nO1LJz2oePOUxU5g0ZpaMXnUOEpBq+Gv6ON7H2qfOXD48JQFuEU+NJgwDlWGZHEOV0tAOqWEG6Taz4zXVDSmKMUURp8EIpn/YDJnFlAEuta+hakqcrG1HyK5Ln4ms5OpCyNE09s+aaAQCl58vxSU8lmQDtxZQ08tCVLccnCzeNlHT2tQsAOv//Aw4A3DGXD92kAAAAAElFTkSuQmCC
// @namespace         https://github.com/waahah/MyScript/blob/main/jiexiVideo/videoJiexi.user.js
// @require           https://cdn.bootcss.com/jquery/3.5.1/jquery.min.js
// @match             *://v.qq.com/x/cover/*
// @match             *://m.v.qq.com/x/cover/*
// @match             *://v.qq.com/x/page/*
// @match             *://m.v.qq.com/x/page/*
// @match             *://m.v.qq.com/x/play.html?cid=*
// @match             *://m.v.qq.com/play.html?cid=*
// @match             *://m.v.qq.com/cover/.*html
// @match             *://m.v.qq.com/x/m/play?*cid*
// @match             *://m.v.qq.com/
// @match             *://www.iqiyi.com/v*
// @match             *://m.iqiyi.com/v*
// @match             *://m.iqiyi.com/kszt/*
// @match             *://www.iqiyi.com/kszt/*
// @match             *://www.iq.com/play/*
// @match             *://m.iqiyi.com/
// @match             *://v.youku.com/v_show/*
// @match             *://m.youku.com/alipay_video/id_*
// @match             *://m.youku.com/video/id_*
// @match             *://*.youku.com/*
// @match             *://w.mgtv.com/b/*
// @match             *://m.mgtv.com/b/*
// @match             *://www.mgtv.com/b/*
// @match             *://m.mgtv.com/home
// @match             *://tv.sohu.com/v/*
// @match             *://m.tv.sohu.com/v/*
// @match             *://film.sohu.com/album/*
// @match             *://m.film.sohu.com/album/*
// @match             *://www.le.com/ptv/vplay/*
// @match             *://m.le.com/ptv/vplay/*
// @match             *://m.le.com/vplay/*
// @match             *://m.le.com/vplay_*
// @match             *://m.le.com/
// @match             *://v.pptv.com/show/*
// @match             *://m.pptv.com/show/*
// @match             *://vip.pptv.com/show/*
// @match             *://www.acfun.cn/v/*
// @match             *://www.acfun.cn/bangumi/*
// @match             *://m.acfun.cn/v/*
// @match             *://www.bilibili.com/video/*
// @match             *://*.bilibili.com/*"
// @match             *://m.bilibili.com/video/*
// @match             *://www.bilibili.com/anime/*
// @match             *://m.bilibili.com/anime/*
// @match             *://www.bilibili.com/bangumi/play/*
// @match             *://m.bilibili.com/bangumi/play/*
// @match             *://vip.1905.com/play/*
// @match             *://www.1905.com/vod/play/*
// @match             *://www.wxtv.net/*
// @match             *://www.eggvod.cn/*
// @match             *://music.163.com
// @match             *://y.qq.com
// @match             *://*.kugou.com
// @match             *://*.kuwo.cn
// @match             *://tv.wandhi.com/*
// @license           GPL License
// @grant             unsafeWindow
// @grant             GM_openInTab
// @grant             GM.openInTab
// @grant             GM_getValue
// @grant             GM_download
// @grant             GM.getValue
// @grant             GM_setValue
// @grant             GM.setValue
// @grant             GM_xmlhttpRequest
// @grant             GM.xmlHttpRequest
// @grant             GM_registerMenuCommand
// @run-at            document-idle
// @connect           bilibili.com
// @connect           iqiyi.com
// @connect           mgtv.com
// @connect           pl.hd.sohu.com
// ==/UserScript==

(function () {
    'use strict';
    var $ = $ || window.$;
    var host = location.host;
    var startURL = location.href;
    var parseInterfaceList = [];
    var selectedInterfaceList = [];

    function innerParse(url) {
        $("#iframe-player").attr("src", url);
    }


    function GMopenInTab(url, open_in_background) {
        if (typeof GM_openInTab === "function") {
            GM_openInTab(url, open_in_background);
        } else {
            GM.openInTab(url, open_in_background);
        }
    }


    function GMgetValue(name, value) {
        if (typeof GM_getValue === "function") {
            return GM_getValue(name, value);
        } else if (typeof GM !== "undefined"){
            return GM.getValue(name, value);
        }else {
            return false;
        }
    }


    function GMsetValue(name, value) {
        if (typeof GM_setValue === "function") {
            GM_setValue(name, value);
        } else {
            GM.setValue(name, value);
        }
    }

    function GMxmlhttpRequest(obj) {
        if (typeof GM_xmlhttpRequest === "function") {
            GM_xmlhttpRequest(obj);
        } else {
            GM.xmlhttpRequest(obj);
        }
    }

    function GMaddStyle(css) {
        var myStyle = document.createElement('style');
        myStyle.textContent = css;
        var doc = document.head || document.documentElement;
        doc.appendChild(myStyle);
    }

    //实时监听网址变化
    async function urlChangeReload() {
        let currentURL = window.location.href;
        //console.log(`初始URL和当前URL是否相同：${startURL==currentURL}`);
        if (startURL !== currentURL) {
            console.log('刷新页面');
            window.location.reload();
        }
    }

    async function mo() {
        //观察到变动时执行回调函数
        async function callback(mutationsList, observer) {
            // Use traditional 'for loops' for IE 11
            for (const mutation of mutationsList) {
                if (mutation.type === 'childList') {
                    console.log('A child node has been added or removed.');
                }
                else if (mutation.type === 'attributes') {
                    console.log('The ' + mutation.attributeName + ' attribute was modified.');
                }
            }
            await urlChangeReload();
        }

        const config = {
            'attributes': true,
            'childList': true,
            'subtree': false
        };
        // 以上述配置开始观察目标节点
        const promise = new Promise((resolve, reject) => {
            const observe = new MutationObserver(callback);
            resolve(observe);
        })
        promise.then(observe => observe.observe(document.body, config));
    }

    var originalInterfaceList = [
        //--------------------------------------------------------------------------------------
        // {
        //     name: "纯净",
        //     type: "1",
        //     url: "https://jx.xmflv.com/?url="
        // },
        {
            name: "B站",
            type: "1",
            url: "https://jx.jsonplayer.com/player/?url="
        }, {
            name: "弹幕",
            type: "1",
            url: "https://dmjx.m3u8.tv/?url="
        }, {
            name: "盘古",
            type: "1",
            url: "https://www.pangujiexi.cc/jiexi.php?url="
        }
        // , {
        //     name: "解析2",
        //     type: "1",
        //     url: "https://jx.xmflv.com/?url="
        // }, {
        //     name: "解析3",
        //     type: "1",
        //     url: "https://z1.m1907.top/?jx="
        // }, {
        //     name: "解析4",
        //     type: "1",
        //     url: "https://www.ckplayer.vip/jiexi/?url="
        // }
    ];
    if (/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {

        let mobile_node = "";
        let mobile_player_nodes = [
            { url: "m.v.qq.com", node: "#player" },
            { url: "m.iqiyi.com", node: ".m-video-player-wrap" },
            { url: "www.iq.com", node: ".intl-video-wrap" },
            { url: "m.youku.com", node: "#player" },
            { url: "m.mgtv.com", node: ".video-area" },
            { url: "tv.sohu.com", node: "#player" },
            { url: "film.sohu.com", node: "#playerWrap" },
            { url: "m.tv.sohu.com", node: ".player" },
            { url: "m.le.com", node: "#j-player" },
            { url: "v.pptv.com", node: "#pptv_playpage_box" },
            { url: "vip.pptv.com", node: ".w-video" },
            { url: "www.fun.tv", node: "#html-video-player-layout" },
            { url: "www.acfun.cn", node: "#player" },
            { url: "m.bilibili.com", node: ".player-container" },
            { url: "vip.1905.com", node: "#player" },
            { url: "www.1905.com", node: "#vodPlayer" },
            { url: "www.56.com", node: "#play_player" }
        ];
        for (let i in mobile_player_nodes) {
            if (mobile_player_nodes[i].url == host) {
                mobile_node = mobile_player_nodes[i].node;
            }
        }

        let mobile_videoPlayer = $("<div id='iframe-div' ><iframe id='iframe-player' frameborder='0' allowfullscreen='true' width='100%' height='100%'></iframe></div>");
        let mobile_innerList = [];
        let mobile_outerList = [];
        let mobile_innerli = "";
        let mobile_innerlis = "";
        let mobile_outerli = "";
        originalInterfaceList.forEach((item, index) => {
            if (item.type == "0") {
                mobile_outerList.push(item);
                mobile_innerList.push(item);
                mobile_outerli += "<li>" + item.name + "</li>";
            }
            if (item.type == "1") {
                mobile_innerList.push(item);
                mobile_innerli += "<li>" + item.name + "</li>";
            }
            if (item.type == "2") {
                mobile_innerList.push(item);
                mobile_innerlis += "<li>" + item.name + "</li>";
            }
        });
        parseInterfaceList = mobile_innerList;

        let left = 0;
        let top = 320;
        let Position = GMgetValue("Position_" + host);
        if (!!Position) {
            left = Position.left;
            top = Position.top;
        }
       let mobile_ImgBase64 = `
        data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAIABJREFUeF7tfQmcFNW1/ldV3T0rmwgCLihuoCCK4o4iiolCcN+NIKiguK/xqU/jvvw1GtckT+MaTaJxTVDADaOiRFFcMGryEnEHZR1mprur6v/OOffculXTPTNozC/vhVZ+Pd1dy617vnv2c6434qj7YgCIAx9xHPM/95X9bH/zyvA8jz/SMUEcmZ/k/Eh+4mP0H30Ogpz8bo72Y8/cV08IzXkyljCW7z0/kuM8H77vIxfJdQEPMf3H16FPPCBEfmo4/AMdH/MfAP3sydGIeOwRf4pjPVHOj32+OnJmwJ4n85TMQ5j6HAWB/cyHxX5qnrxYnsOPA0RRBHj6vGa+opCPp/mT4/R+Hj+3zmnZjB2xzCcdq/Ohn+VdZ1qeP3nRZJXgEQD4hn7yY1WiOzeKUXIuhhQAmODuHBkQyOBlQgxdgbiMMKRJiHhCopCmG4ghE+vFRf5EF5TrhgiCAMjnmDD5vM+fA8Q8QfkgQD6fp5/h+x7o0FyOJon+ybVBExFFdkJ4PIYwfN8oRrlMh0QohQZ4BKooQrnkyTiJeIhRKoYMqCiU7/wyjdVDWWAJRAW+T+gTYIEwyvHnnC8EJeAapMk7f0fPbb62E2kWgiFi5AfmAAGAg8jMIk4v6AS4RSDOEwDuEgAEZjUbImeuqnhDHAti9RVkrk/D8nwhXhSHiFGPKC6hGLZgje4+Nt+4D4YMGoDBG6yHNXs3oGtjHg20IgncMnfmwXghI/Bj/t5nIqVvlkZ0dsT/vM/tLhjzPBERmv+X5wnNgzbHQNOKGAsWL8F77y/Aq2/9Be+893c0rfDR1e+GEnEBgrdPnEE4KwPKvCMWIMTmel6U5sAJr207H7ygdhl/N5/hAoAvmJ1se76siCwAlBg+odcP0dSyFDsMWw9Tjx2LtbvVohYhfGb8eR6TZdFCdwswZVi0ounl6Qpxudc/j7bf/k6ZlcwiyBWB5g5+RESkeSgzt6Mp/srL43czXsXt905HfdAbUblAEsUCQFeKcFQjws317HRZVpsVAXKgBUBoOAC8RGa4hBY2SawpswpJrkcQmYYWdO0a4dKLxmPjnrWoIbZsSfvt5/Lf8Qo020VEWBEBF9z8a7zx6pfwg24ISIvxiiCdgxcsazXuK7NiMrqNHtkuAPSgRMEgcKYBQMpRzmtBfeNK3PGT09Doh6ghdsTIE7a3+vXNZ4CWI0nBsgdWRJt94OePzsKDj74GP+6G0HCEVQWAcmxv1/F3xsT+VWu3RM+yEsuS9QcPfhQiKn6Fu286FWt395GD3waH1R69imrS6ZlyVIVOn/O/8cBq62cpgEnn3IovFhWAKA8ExhrxjHJINg4vxLTVktWbLABiP61kKYEc24DnT1l6DcrYbmgPXHDifqjxIkREfFeYdzDbqwHQOThWAwBZGGRwzPnLFzj3ygcQxl2NmauiIAEAi4gM5xYLJIa309F3shUAYwa21WhVGZMBs8FV+grXXzwRW/SvR2ARV/mBvi2hOzdN/z5HtQVEjK/h4eBjr0Yx6g0vJg9Bnq0EWbGGAxgdICXOyQpQALAGWsHUEh09WflR8xd44o4z0K3GY9eJMpzvitX/+5C2c0+aBYAw3RjFGDj+4jvw4d/JzMxZM1HNQzYfUg4juV+nOECOFTofpZa/Y8a956HWI5OufdKvXvmdI+g3PaqSDtQah7j8F49j5stLkI9iNu0tHYwXUYGg920DAHdAwi7EzIuaF+DpX52HAOQIImWvffV+NQC+KWk7d14WADTfpINFKOO6+57FYzM/QOTVWc+aywlcRdDbeeI9xjsqJEv8Bir7faD5M8y47yy26zuy61YTvnME/KZHVbN+3OVIrvULbn0EL766hIMerA8YXY2AoNYBiwACgCDCOIBsTMDI/tJSTLv9RNRxUETDJ9WHvxoA35S0nTuvMwCgK5FddvDp12Ph190YAOySY49hAgB2Be88UWIBViaoT9knjT/EzT8+EAP71LL+X+m1muCVCfdd+yk69q/FaIWH3Y64GF5+HSsKNBinVl8KANY75JF2H2O/3Xpj6qF7SiSuisxfDYB/TQBwaNyLsTQCfjDxFoRevQ0Xsw/AWH3eLhPED0DhYIlDk0cvQq70Oabdfjpyqj12jkPZo1YVGNkV801XUMcrQ4aYHV+1+1d77Grnr+rxnZ2nzj6X3j+5bhl3PPYa7nrwbUQB+Y1FFHheIO8jxv8yJkQoAEgFCLwV+P3PpnRK6evsA3eEn9UAaH+GvikAyBAk7W63Iy5EubARc4U0AI66O/YDSY4Qj2CAEw8biv1327wjmv1Dfm/rcv6HXPbf/iJZzrJwZRn7Tr0TvpdPZWl5IzIACIsL8fRdpyLnZcOL382crgbAdzuvydVD7HPST7FoeXfORKJ/vOhHjr8z9gOj5PkxrrzgB9hm3Z6VzX12A9hsvg5G3tnjOjkB1YTlqvLGTt6ujTLzXd2ns8pIdqVUXTnpebeHxTE+bYpwyEl3iRtPdQECAOVpUjJkXPwY0+8+mwM+qfxBozW1eDHKkYdymaxMymmTWckEmsy5yQjp90ppU3R+9je1RLLHa7g65kRKzpjkHLzAPIhL1zYhTyd5tT36u/ek+Du9KMOJ7seRTpPk2plr2AQaJx8yUT913pLcPz3eJkCZ+9mUQcphJK+/jstYZWGs+Yly9Xw+QFef8iSTuxnysXdg18OugFe7LluBzAFGMQeglB4Pkw4aikP33CrzfBHC2EdTOUSZgLEKK4g8UprJqiOPzEyy25KISZqpySIm4lIWrLwEyZStLITQb5OwNWfNaqZsLHqMAiXJ4k2PuL38PUn0NBNnRKDm4FGuHV3fji6KEWgWFY2Wxm7GUjF3scKKTR+XPBfn/JlVZYFnx5UmgM3FNsfTbfIUpPNjNBTaRmymz/kQF980CznVBUZN+C8GAM3z4z87FvkMhekGzcUYxahEQUb8/g+/R2trK48iyOX4oQv5PL/TJNBDlcOQ/w7LmvFLUcm8rCDIbzTQKJI8Fs/3EFLG7f+4MEOTJex7cm1KBqUhsQeLYBt6nEWcIhanj5f590JOxqKrVblBzqRrS7KqpJwHlF6ekyxdzfSl38rlMoqlEP3790e/Pn3ZZiy2ruDvyxFlAVO2sSwxn1yshiP5QcDjZY5B81Io8NzQKwxLKJVKCMMYtbW1jmNGjvXiAH7gI/ADmUuzQpkDcXa5OY7uEVAkRgiV8wO0tDRj5cpmLF++HLU1Baw3YBBHbApehC6FtC5XBjDi8J+ikO9uOMCE/4pzXohy6XPMvPuCTO44pTHHWNq8EldfeTn+NPtFxKGsNN8XAtGEMStRhCqbzMoFu64l791zwpP8gJmkhcoio3p2C0+8ubfGvPUa7rt7H2v5OOe5HIAIPnbcOJRbWhHFRQuSLMAIDDInolxxmnqQ/M3f5YiF57HOuhuY+ZK1q2NI3tvWUui15d3cKwh4IS5evNSCUa9BYB6y4wjU5+qQD4C6IKEPLZS9J9+E1lYDgNETbotzKGOfvYfiuIN2aBPhbyqX8dTjv8ctN98ILywyyxYNUgaiygLHG7jwQl68aisUmUSUHcyIN75pcwxxADv5BlR6flam68Tp8S4As8fSNbIAcM/LXssFAP1dLJfx/T1GUwWA5ToqetKEkXmpBgCqO9h4o0HMQRigpEU5RTOVAFAJIAQAAubiJSuY8GTXu8+gukSxHGGX3fdEVC6jsZBzRHcJj/zxr7j+Fy/L/UdPuDUuBC144BenosHUTFhKAJyX/v09RjG7JGTlyUmcAzFyQ2lT+UNuI0NMkZXEbKjqJi1TOO3ZgMXVAfxICBUbIa6MSzNblGMogWkC3ZedQIcTpQBoKnBE6UwmP47UMybjLHNKtkwqPTP9ve46G2CDAevz38QtWUZTaIxEgS8iR/+RqOG/c0ZMEKf0PGyyySacqFGiPEqUkTOAJ9bO88WSRKudqNCGRBNQ1t9jnwm/sqlFVjwF57hmQF40HvqfvLik4VNeEM3vzruOSnEAerSVHjD2yLsQ50vwvj/+xhjxMjx257nIt1H9gSUrmnD4wQegXJJKoOYIGHv02chTlj/J7Thi+U2hB3cF5+KCENqnyp7kFYU+V8zwQ5OiRUSnkRsZXajJsexUP4TlKqZkio4nXcELpTSNy8RoJeh7TmQzqat25UtmlF2dTDjDtnM5o1vQGiFdAqTcBSyLc/kc8rk8Ghq6scLHZW1xK8KQrA8hFhXKqP5An/N+wOMPCjXC6YI8yqUyFi1aAgJb926NGLB2IxUmGWtKqqGiqIzQcAdRnuvw+bJmtBQFoFz5RIT3RNnMB0bPoPEbK4X0mffmvYJ8uYnjN/TaaeRoqsRIOABlGCPCLofeiHxdI7wxP7w+Xhl/iafvvrSN/KcLvPHmWzj3nDPsZG75vQOwxdjJtnTLrjKbTpbW4lUUqP3gW1ekgIKKRQQMRjegsqnYAxV+MYDYCmMDKDU+ZX3KTViZNGFt4UCJTsDnG3VaOQiNR1acjMMVHZbLkIQzjyPj0GMlSYbHTzTxhShcxETp24bLlGkUYYiWkJRhH19/1cSh2TUbPazfh3L7Q1shxCKUr+fjwy+a8VUzmYh1iH0FugGwmb98zkfeC1Egq548uT5pGB5q/BjvzpmFwMzn0K23QY8uXVOLkGj2vUnXoVzqDm+vI6+Ot9yqNy4/fXzqIP3w4kuzcdklF/JHOnH0UWeg93Z78oOllCzLPTT3zBDAsHyaHCG0ElLNqsoA0GJTS0gHAC6hdZy6IrnI09QmKiF5Jao9ZYitACC+mRA8EVeW2AYAWb8oAYBVIOIsQYS8H4Mkbc6PWOZ+8sXXqGnsxrV/BACqNVz89UrEXgGxV0ZjbYxN+nWV830Py8oxPlywEi0RFTWSu1au76vO5InJm1NO5qMNAAqej9ocMO/lZ5H3ZR4GDdkCa/Vcsw1tL7zxccx69TN4I4+8Mr7szEOw49D1KwLg+Vl/xFVXXGLhv/MRZ2CT4SNB6KaKFa1Fa1uYkFxOVrAsGWJl8llWvBJOOQCVSBFxAiOLbeqZLQ5Nr/QsB8gqRQngklUuLFWjnwaIBqBJVbOMn0x9BoPxghJv4TIurmWUyqf6QowuNTGef+ZZzHr+JfTo0QOlljImn3oKSswBxIH21eIy4lh0IKrE6paPUVfrY+GKIlrjupRSSIJSOJeMW8ZBABNOQOa6H4TsCMt7ZMaTHhKjPu/jzT8+gxry7noeNt98CHr16tWGts/P+RvOu3EGvF0Ovih+7O4L0a2mIv3x7HOzcM1Vl7GMpRW//aGnYrPtd2cAlIgoqry1UxKiAFBZlgUAs1LDsqi4hApOFTAqAlSmJaw9LRLacAA1Cw1HUKBkRYD6nVRE0Wfj/EsDABFq45BXe9d6SY77cnnMetOSlSux/ItP8OK038ErNIiy6Pv4+5cL8aNzz2MAtIQBliyLEMUEAipfJwD6iH0qJ5fSdJcTVQMAyX7WNaoBIOdj7gszmRMQUAYN2hxrrbVWG+J+viTEISffCm+Hg86On//NVawoVHo9++zzuPrqy61GvOOBJ2DALmPgUek0USfKFJRkgkhKQCIwsUhlrbTCeYXRcjIvRrhJQlWRoYZlIqOdyCWv0MQMSrNyua7rH3B/t+PQChq1gMz16GxXF2gIYpw56QgEhRzW6NEDH/7lE/zi3gfg53L4YulyFIIATzxwB2oi4Yw+xUxCH4OGbYMNBm+LpSvLojQb3UQTMtxnd0WbiAASmULwhANEyNMcBwQC+dujLG3SCWIPdQXg9VkdA4BU9j3H3wxvu31Oil98+Kd8g0qv559/AVdddZkFwPb7T8GGu45lHzk9qFtTxmZcBgC6YisBQB5QRIGuUBIRUmzaVvljzpGxewkACTgSU4q0c53A7CTL8caMc0CpMp04QNrI5E4AePah+/Hhm3NF6/cjrLPh5hg/8Wh8ubQECqjkvBh333gN1uzaHUG+gE+XLEXPPv0xap9D0VpsNX4EoyNpebcDfgWA+FnSACCOIDpAxAupEgDI/V1X42GuA4CBAzdDnz592pCWBNHoI6+Ht+3eU+KXf39rVQb+xz++hMsvv9gCYOd9J6P/yH0RoIwSNVUwnSz4Dk63CiWoKmWq1WvOqRKO0Mt1B0ZJK8TqV9CGENVkfpoTaKKDS2weQ0bJs8CyoJOJZZZLLnHE6NaQY5X8y2Uc8hJwxhFqciEO33sPDBuyJSI/ZJf0VTfcjEXLgJXFIspRgNpcC/7w0G9xxOSJ+PCTleyZVxd5doEQwW05vGMduStfRQHJfuYERgeg4QsHEFYvuk6EhkKQAsAmmwxEv3792q7tOMYeE66FN2zPCfGcp+6omuf/ElkBl/3YevZ23ncKNhi1L/y4zF0wyANgTUEDgPTdhFDcz4P978ZutZ6wBAAs21i7Tsw+V+a7clx6DchLVkwSVxVOIVzFaXxi7WU9h6/HZpaPLrkSHnvgflx96SXos2YPfP7115g2+x3EeUqIJUYXMVB7d/cx9ajx6Eb+fAAff74Iv3zgfny0pIzIL/B8kMukzEpiBD9KWu+w7HdEDl/XPAMtCF35Mm6dJ/lefydC63EdAYD8GRtttAnWXnvtygCYeBm8wXv+MH7rybsSIzdz6OzZs3HJJZfYoe5w0HHYbKf9EHplco4itPOerkHTvAHVtn31vnGYgoIYhtAmokcrk1gbdRkQwshAiK4ugROZLzfWidJh0/0kmJIWDXTBHPU1MqFpcmXLBUIUvAB9u/nYc9tt4dfWyoQjwMHHHIPRYw9Biwm9MIDjEh684w588PabWLJ8KRYuWYpzLrsSfftvytFSlvEm4GQuLwAynj8Jg9FLs6xVBCphBbicvsf+DHG503zx94GYm6SsEgDI7mewGxleX/Aw7/lnUJeX62244cZYZ5112gAgDmOMOe5ieJvvcWT89vS7qwKAIkwHH3yw9XiddcMvsLKmP3vxSrSqTbOopNeNeTBtfmSeVx0+RBgGgMajDSdg048cgmr+GAKRjsBAMBOh11HR4ip5PEHm+IAaVtgYPjloIhRo8vyIPX3NJePJoxVFEbqaGHvvsB26NNTKhEYhFi5ehul/eh1NRerzo7GNCI21HrbbbCAeemo61lpnfbQUgXJIHjYxF934g7rCFQAk7ehvrarUcInrkBIRoN5SIqQczX4BBoCYynnmBqJY0+/k7cwHMd5+YSZypBTm82wF9OzZsw0AiDuNOfYieAO/d3g8fxoBoHLe/9KlS3HIIYcwiyWZd/LVtyLstjGzRH7eKlE/946ukqYESh5QOYE8lPUAaqAkY8bZ1jGWxasrNJkwQwdZMR7572P06BJg0uGH4a/z38fKlSvx3FtvYWWYQ6AszPfQvaaIvXbaHjW1teya9cs+9jzgEBx83Km2a5d0/SLfu5iLmtBCYGMWb4EiHFEBoKCwHKBCxw6eJ884xoyoEJYvRCbCs+yn91zMIokcXKT9E/HJI0kgf+uFGezCJgCQErjmmm0dQRSUO3Di+fCGjD0qnvfYHVUBsGzZMuYA2sXrpKsIABtZALSFVvqbrIbOYYoUwg0nZuJT5k1auaMJYA6gMtH8rt8pB7ArxFgz9JmJ48fo0xhj9y23RF19vc2oOefCSzB4l91tsyKSzrS6mr9YgAmH7M+++kVLmvDw44+gfu2BJOwM4OUGoTZn0kQVAwAOyvC9Tfs7Ui81ps86gPnd6ciRmiOjDJODh15ZABChydVcCQA0fvIAKgAoNkAcoBoADj72QniDxxAAbofnZ9uNCWGyADjxmtsQN26IUO13Tzxb9pVRBPXhdOVbQiqLI7lqPH/8wOqj1xiBMQeV5SvhmV9xWFm094Y82Pv1xtw3sPHmW0sql0AHQS7GB2+8iB+feRrnMdCruaWI38+ajZawIG1WDGGIYzQGRZS8WpRKIVopaMOizvTiM1wviXLmmMDUz5BXuQlCdQQEnS83WMpzo+aviVNI6pb8o5EnAJDIn7B+yfwhABAHmDdreoccgKB48DH/QQD4YfwWcYBOAIAGPfXqWxE1DPhGAFDii5YvK50HTrJfAWEcJbryE7MtzQkCP0TXPLBwwV9w8Lj9sEb3LuyPj4olPPrCS2imQIomq/gRaup87DpwENZaUzpplMIYi5evxKy589BUosCLydPjNyGEsO1Iau1NXoEuXAKArGwBAHVIEZafFgWqG7miwBKf/SYGpirSUkEssWIUAGTyMRCYA1QGAHOAWTOY/ZOuQxygkiuY4p6HHHs+vC3GHBG/yQCQ8G325eoApLycePXPEXVZB6FV05UDkIOcMZq6BLWMo5eGd91oIMkwdvs6gHB993SeKoW68gu5EJedfTLmv/466mIffj7diZRWw9KVKzDj1dewtJWuL7pB7MVYo97Dbltvg002H4Tbfnk3ihHQUmrbf9DVa1TG63dKSPaCOjqQcIAc30c4gkyDigI9lqIH7elNlkMq/2JrwMh+Ij4HoOSZKnOACG8+Px2FfKFdAJAGd8Tk0+AN3uuw+K0n7qwKAFcE0MBPuuYXCBvXXiUAyEo3ypoTDVQAsJmnfgEnU0YBoIii6/RqLOPw3XblHDlaPYnVoC1nY3ZQrT9gQ1z581+RsivuZ66QpTy5GK3swPKQc5oVpiKbDoRVkqgSpwSkVDh6kfhIOEBgRQUBQM4xnMUcn02Q0fuqqLT+CwcAWp1HHEAacUr4WQHAYOCkEtEBSAQQAEgH2GyzwRWtAALAD6ecAW+z7x0av/OHu6oCgMzAgw46iK0A4gAn/7//Qqm+L7RVKblIddDKcgX6kmzJLJxCkyY8qTI+b1aI9Qg64WJ35ScearFrG/JFTNxrJP+tXEHfraYdRVixYgWenfsWlrTITLoNLcSkS/O6agBg7cBR4lxOwCvdMLzIKIVk4LEr2VEK5Zy0MlgpepqAIPFgqoeT5pHlv4aJ6W+jA7gAyOoA1cxA0lbGH38mvEF7HhK/O+0ejkFXejU1NeGAAw6oCAB+MKdptHs+pScIIclUSQOAvicAMGcwrDSJGWh41lgDNigj39fkI4wfvTPqa8RsdTVoFwA07umvzsWKssnedbLi1OYWtqysOo2IhE2nV3CWE1AKGV/H6gJyPAGAsp5dZdCKAVYWM6LSuqZlPOrC7gwAskrgm88/1SEHoBEcPfVseAN3PyCeP/3XktpS4ZXlACwCGtZGqG3GTF/gLDGUA2hmSiL7RRRw1o8J2KR0gAwnyEbtKAZ+/Xmn4sO335CJVyXKccCQv2JFSxF/ePk1lEyFh61o8GO2FkguMJEp5dtpf0xE4/RyQx9qHE1jKIVAiYimSaZewP6FiIJZpmCjWA64WZPa/67+oP6BrEjI+l/0eW3MxIllVOMAzA09EW/5XIzOAIBEwMQTToO36aj94/dm/GaVAFBu6McigFm+UfKy2OEEJZMcqna/uHSFYgSAvLp9jV/AcgxHKUxH7SQa2MVbgqPGjTW83UQSdeJNJ+8vv16CZ+a+g8iYpWJNANMevBc33/ATSd+KY/Rbdx384v5Hbe9iWs89CiF2Gr6DuJTDEM3NLXjmtXkokuPFKH6U0bPH8O04N49epVIRM+f8CV+0mDoA0omdiijiBJV0AhcALmdS7Z97L6uFUEUEuADIBYkO0J4V8A8BgPCqjB+A10POhnPp9yRaZdyWEfmxE0dH2ixUH7/QN8ndKxrFx0MNVuCoH+ydYv8ue2Vr5cyzMWKv/ZkX0/VJNtfkQ+w+bCs0NFD+c8RZtjNfnI2mUPQVfhy/jB2GDEKfNdYQDhMCt993DxrX3tiwelIegb/OfwXnnnq6xX1YKuKpV1/Fp8spBkT5TgIA1QVUpBC/SFkBWh+h5l4qeimlaTw/xhpQbyBzA0cHYK7FHCBtBVBGUGVXMHDMSafBG7THgfG7Tz3wjThAuwAwKV9iwkgYU8SErH5JDkn8AYlfQFPGTMKIWg++KJucd1dajAn7jbPeQaWCymeqwHl+9iv4slmSQxkAfoSucRl77LqD7B8Q+1xY8czLc7AySsRfYx2w89DBaKjvwpdd0bICz730BpYZbY8cPbW5EN8bPgyNXessRwiLEZ585RV8tkL6JKcBkMQHVhUAtvHrNwQAWQGVPIEk+o45+TR4m406IH5nRud1APIDqBnIqybDAdRxozoBs38br5ZEDBcA5KolX7b6wNXXn80byEVmhxI/QL/GGN/fdUfkKX6R2eiCEkGKxSJm/PF1LCsR9xGWTEGg6y7+T7w06xn+TK7eLbfZHudffZ0A0+T8rVHnYeT228MziF2yuAnT58xDaOK2FFTqXhvjeztubzaiEOIWW5rx5Gtv4PNlciC5ipXlpx1ElXdmyZqBGt5W3cVyAGNVUXyAzWgTO1AzsBAkHKA9VzBxp2OJAwwcuX88/+nO6wAKALtjxTcAANUUqAOoEgCEM6SzhvNmowo2BesC3HTNj/HBO/NRbjU7l3iSa0fuW3rd/9ATWFGmNDR5+fkAh479Pro0kou3hHIY4+HHnkBTmZO/TKVShAP3HYOe9XVcwUPK5KXXXY9ea2+CODKFLn6AT/7yLi4671zkC3neXaRULmHf/Q/CHvschrKpGaSC2iRY5JqFabNSF4pysQQILgcUP4b1ArIfQACgWcMJANJKIAWDKnoCiQOceCq8TXfbP35vZnUAqBlIMpMmuLMA0DRsLd9WViwyTHLdOLVaw/I2NqBp4jIl7OihNCitzjXHNS/6BD/76U9QKrWIMkqFpkbJZOUNebtfDiV8kJNEZG/EhNWYAAdnypTDJwUh6sm09j7b78QxjK6j43VctgSo5v8peLnw+lts4WlkACAOIQGAcB7DIarUWTP4WR1xs5UTRqd5AaoDkI7ATiHjCMpygGoAoPFMJg6wqlZARwBIWJauYE1qFCVAq48IAOTT5n0PeFuBtP2fMoMqAGDpgvdwyuRjuEbRKhdmGTGqFqlSAAAgAElEQVTxzIzThMpOQbpHkNYtmOieSUIlJNJ5NLGuQ0tZP2VFM3FoGxzjw7c+gThGS5zDQzNfBO9wxDEBAY66hPU6NjEks/mU6wQSrmA4l7FerFWgeQFGCWwPAO3HAoApJ5MnsJM6QMIByBXcD1F2syBlterbN/5qWvmSc2fMP0P4pFmRIF4B0EYHyHIARkwZyz77ECcdO5FzFXnydHeuCnkKTNCM509ZrvuelcNKaKsjZE5SABBHKXkF/Gb6LGmgwfEAwzkUEEb7J9e06xl0d1cTwiesnz/rAjFA0OTdrBVQjQNUcwWzDkBK4Oa7Hxi/Pb26FUAu1QMPPJBXEA2cHEHsBzAAcFcLy9p2AMDWAPsA5EGF3bUFgKsDMCAocdQ2aKAqnxBLP/kAJ0+eVBUAzEQdF26SCpZOf3Y9iVl5zNfIOJoUA+7qJwCEQS3un/YsN9MggyHidHnJ+WOO0A4A3DF0BgBJeFjCwWwdWRGQ6ADEAapZATS2404+Hd7g0fvFbz3526pmoAJAYwEKAJsPYGbEOisyZc9WB1BzkB1A4tBJmYGG5yWVQ5q2negADDDiJQYAp0w5xuYE2tXrrHQXAPp3S6mMm2+6CcccPQldu9UhCEwRq3oG3SzSSmzCiJYsAOJCA+57YiazfgofE+GjMNn/T6OC6hdQXcPdqSUr+5UDyHOLWEg4gFhTHVkB1XQAWleTTzuzYwBUcgUTB+gMAJT1i9Jn/AEZAKRLtxLHR8oT5ugAFLcnACxe8GecdsJxHQLAJRT7B16ajWaurvXxyrPP4aLz/8MGltwVX0k0iBaXhIFdEYCaRgZASE6gbwkAuo1r/mUBIBygYwC0pwNYP8BWo8bEf5rxhOzbJ0+o0pzf2+YDiA7QEQD4IahW3jpyROtXB5DNCdTwppMJww9szB5COMn8XCiyXhNFlix4D6euIgDI8TPr1dfRRP191MNWbsH3R+7KCRQuK042tjRRR9ukqDIAEg6gGUQBcwCzVWUqY4gLS6q04cvWN6gjKJkPU0VNxajEAdqIgLQfoD0OMOmkU+ENGzU2njPj8U4DYOpVlBBCwaC2DRqY6E4On3IAzWNXAHCSo7XzTXw7kwoltXJ0PQFA3njrFDiLP5rPACAlUGW3aOlp2W/NuShizx8BYAWr5OqaLKNL4GPwRhtyDZ32DagGgCRTSIDAvZCoUtqKgDQAJEs8SRlTc5A8gtmXKwL0N811TObjHwcA9gQO3/MH8cvTHrETmR1UpYwg9gR2AABrkxsrgIlpYgAps89wgKRc26Q/a3Inebr8kDmAcAaZ+KWf/BmnTJls8+ctCCoAgJWwKGIP4fOvzMGKMB35pPHU1gBP3Hc/brzhegQ500fX2eLW1TFcscLEJQDUNODeJ2ZyAorkEEoDDU0Z05xB6w9gnSM92xUBoOagLgieL6MXVeQAaSWwWkoY+T+OOfF0eFvvPiZ+dfpjKTnoDiurA3SGAyTaPSkp2oLFjQEkjg1WbihXP1OkmbiCKwNgyQLiAMcj5xQ1KgfgvHuj1CkHICIJAF7DslJiZythiRbUUCkXtmKPHUagtt7kR5iBZAHA+oIBlgLgnsdnsNnwTQCQRAKzVc9CDasEMtIlg/nbiAACwKSpp8HbZo9x8aszfld1+ydNCVMrgAHQuC7CIIkCurIzqflLHEBq6iUpYG1FQFUAGICQA4b1ByPzFn/0Dk49YSr3w3Ptdy5YzgAgzQFex9JWp/LIJmUaUPhAfVTGsMED0WuNNeEbbqAcRgmv7J8mkgCQq+uGux55knVEBgCZgqFvdz93zUA6VxNFVBRlS9usCMhyAJskWk0JXEUOQDrAn55+pFMAoIk84cqffSsAaIk46QCsC5jgRiUA0KqwKWW0a7jZRJrEy9d/f7sqAJRISnglFnGAZ19+DctLnObr6A58RpLahhB5H7jrxp/gt7/5re0lqCBw9QoFQKG+O3758LQ2ALDZwhoGNtnE/wwAtJ8TaETA0JGj47nPTLeux6wO4HIABsBVt3BaeJQrpVymOjnVHEGVdIDOAoAcQeTdVwDQvRYveBdnHD8VnumE4eoA7QHgudmvYxn5azPFpHJ+IhqoDUhN4GGnrYZgjS7dLFh0fojrcMNKwwHydd1wJ3MAKn0hHcBL+QHacoCsQypRCNIc1YzL8QR21hHUEQA4I6gjALTRAUxdQCUA0OTYBA7HFczmG9W3tWMGtscBKgHg6wXv4MzjT2wXALYez6RxkRXw7OzXsTwDAAseJwgTUwOmwMOwTTfEOmv1bZN7kBUBCQf43wOASVNP7xgAWU/gCVfehrjrupIV7FFgRIMssjYsIU07M+sHMEoe6d8pT6DNhDFmnwY7nBIvuo9yEDUzFy94B6edcGJFHSDrAVStvaWlhQGwwhEB7ooWIEjbusArc8oXhY9dpVaP12umRMDvpoOaN/MmTavIAZQDuaKI51PnZxU4gFYGEQegjKA1THaTy91p3MedfBa8LXfbM3796aeqioAsAI6/4lZ43ftzXUDM9bAZrVW1+W8BANZ6Nf2ZM4/bAmDpx2QFiBKYrOC2fgCXC7gAkH000lnFzHq9GPUoYufh26NrQw0nhigA3Am0XkDTrdsvNOKex56xAOAYQCQ9fzUWwFzDhAc77Qf4BgB449knUVNTw7pLNQCQDnIMcYBt9hjLZmA2KKIPmxUBnQaAmVytc9dJ1HTwdBVwmuB8rImCBaRWkw6gIjOmSL+HJeQKnnp8pwFAQCAR8NzLc7GMk0CSlxDeA7V8aGlehH1H74UgLz1/KxFfz2QnEBWZUH1jTRfc+fAM0QnYFexGA5OgEPkJ6NUeAFxg2i5lmk7n6CqV6wIoKVSqg2n8Q4YM5a5l2ReNmTnAtnuOi2c/+UinATDl8lvg91hfKoPaEwE28SPd+EABoF3J1UTMZsGqpzRnOAB1FmHWHlOnDmDJx3/G6VNP4GZMLouu5Al0zcCZf/wTVoRp05Empy4IscdOO3HhBXUIdaOVWTGhEVCOAhoAEAe465GZjgig/AInH0AjkyYcrABoG41MWydq/6uI0KRQzg7qoDCEOMDgwVtUBABx7klTz4C37V77xi//4XdVW8R0hgO4iNUHSrJ50yVh6aRQp/jRNm0yxDHRQmoYKVFDyQkMTEYNAeDsE05AbCqOqkUDbQIGdTUrlfD0i3OwrCi1YuSAImWva97HyG2Ho66OevWlRUobTuGEmV0O4OUbBAAedf+kglEBrFspJCs/nS5eSQdJRFpSaamOIi0QTQBg6ixM84hseXhVAMTAxBNPgTf8+/vEs6c93GkAuBygkg6gg/82AOBraFMkBwD0PXEEIvbST97H6ZMnWysgC4CsIkjEIgA89/JrWNxCYoXy6iLki00YM2oUcjXSGsZ2Gk1ccxYD1mvodCDXHTvioE50AGq9LzXPqRhAUjbethjVBYEbDGI/iPmxswCglDAVAe1xAGJIkwgA23xvXPzKk490CgA0qVOuuBle1404FsCTnC0NMzWB7BH0idGZ/vZOF6wkK9hxCeuK1x7B5ond1DDRBYQjLPv7ezjz+OOskpZV6DRsq02s1Gf/3AsvY2nRR00eOHz/sfj6y0WS3avJKapUdpBBlLUC4nw97n70aVCNICeEaF2A4RiaCURRQD23Lft3excL5W3UwkZHk7yAajqAVge3pwTSNE+ipNBho8fGcygWUCVJ0RUBqwIA7uVjAFCtMCQVFLKpUBnXrjZuMKlh2mL26w/fwrmnkB8gXSRqV5OWihnlMYkJAI2NjVi6+GvkqITapJRxVjgPyFgH7QDADQYRwFgUBHW49/FnLABsMqjtCGKigtoo0okEua5slfXZpFDNClalkN6r6QCdKQ2jYR099WQJBs2Z+XjVrWGzOsAJV90KdJEOIWkOkPQHcB9IawOTmnYhWBt/gNMsSiYhUyGksplarwURPn33NVx89hnpPnCuZp+t/rVWRFKl665At5k03z+z0VWl1DCR8SbS6NfgvmnP22RQjgqSCWiUPgvATGWQFokqi9f3Nn0SOskBSAcgANQUxAysnhYeY/zkk40ZOOOxbwQAnm9bF5BuEKGaeRYAgWkEWQ0ASWGJKn/p+gBq2Ej3XL97DfYasSMDoJIJm00Czcb329hFdjs8+SV7vvb20e9dEUAcYPuRozH53EvIhST1ABEVk0r/eJdjqPavloRbJey6otO9kZJoIHUq4ZqAKuXhLgDazQhCjAlTSAfYY7/41Rm/4Xq+Sq+sI2jKFT+F320TaYzAmnnmLFOMmQDA6AKa82fCw5rkSdEeqgxKVrxc0HYD0/54poGkVhB1q/Mxbsct0dhQx42snW7wFZ8j+2VmI5NE0ctyjmzQ3hxpARCHaGqhtjSz0VSuMQRXbV96BmmnUDpVS8OS22jBbOJRFasnHZsQrV88g1wM7IgA3p+IAmWmQYRygPYAQI6gowkA2+55QDz7qQc6DYDjr7wRXteNpTEC9+TLzpgASQFgdQHb+dJ0u9bmT5TaxMRVlp8AQD1zYv4lMpq/90OsUQD23HFbrNmQgxck7c6VOJWQQL+RTpIFABGJrQDT/ZzuoauUruNeU5+N/ADLm1vw9CuvY1GRgj90ICWCJOFetziUFVHVAezgVIdJzE/6SRpg8p1t5nQ1ALj9ATrLAYijsQgYPnr/+JXpv+40ACgaiC5iBaREQJXuYGzW+KHZjNLdMEITRLRSyEyA7Q6W6QWsfgJrp9MWKhHq8wUUolbUF2SzS8oapi1s1MGkaTeumGDCmibRtkGEIbIyNCKARAUkq48qfqkCV2U55f63kvIX1aC55KR88YpPmj8lOoCcaxtGVG0QkV4IiUPKcADTDYyepzNKYNXKIAATprASuG88Z+ZvVwEAiRLYWQBwUie1tTIpXbqCxBw03ML45lX2uc2hmKNUAAABizqW8jaoKipC2sqONlDQ/gVpn7/rqNKVTd8pYbRvgP4WqYzjlZ24kElsUT9g+p3aw6rSaFO/Mtq/dQwZhGmvoJQiyvdKd0ZJ/AKyN4G2g3MB4IoA8gO88ZwogR31B2AADB25T/z6s+QJTLcsUQ7VJhjkigA9yBUFDiew1gCnfCVigVm6mSDerp57cqe3elEPoGYpJ/4AU0xiGyqa3UNNi1j5Wrr+UdkUEYhbrVMPH9pbh/enojayRDaqH0zag9A5ZdVVaGMqYsNU/EkdjImLUbFHILt5ISoh5HIyCfrQS1i/ERdWbAjFbfBIgVGVA5g+BbZVrMmR1B5BFThAFgCuH6A9DnB0ZwDQxhX8DQFgGx3oBgh26xhpfmz7AhoZaV3AyvKtP0AmSDvanH7cUfjlnXdihZZvI4d6r4zN1+6Ntxd+jaHr9cXc9//Ou3cN6tVFAjdBgO133B4P/mEalrYIa24IPAzo3R1h1CJtYFCDoJDHnz/9Gi2hh7r/acF6wOhdMP/tNzgR5JobbsDYw4/BijKJE3L9iotXAaC7k6pH0CqNJjqY1v6V6K5Fo0Wy0gbWNokyKXJul7BvCoDxx50Ib8iIsfGbsx61ffGzilObfADHD5ASAYkeTRv+Jp/MikqUwnSrWGZl3C/QdPKgtuyk8OkKV8+gBpc0qGLMv54NMUYOHYInXnnb1DTksF63GI88+hh22mschvXugpcWrEChJsKQNbrg3S+X8dioK/mEA8bh2ptuREPvDVBXG2LSPvvjlgcfETlvdkIheR4UfAzt2x1v/G0hwnwNK2i962MM6NsPL77/GUrMddKNIpNVn5b9bsNIVjRtTmICAnM1W0XNuRDaJbQCAHgOK5SGte8HoD6BJ8IbvPOYeN4L5AfonAhwHUGrCgCeeN0ezikhcwFADRi0CTJPhI0FCKYUWnbDxVpguzUaMf+rZVhWClDIR9i6T3fM/2Ix78uzRa9GvPLJSuTyZQzt2RVvfb5c2LUfoKEOGLjmGnjzs4XI+yGO3e9AXHf/Q7IdrgEehXXfe3kmdhq5BxYW87bLOQoRDhuxAx57ehaWlAtJtzGtATTxApX9qmO06RNoKqZYcLn7HNi9FdPdwFlnaqdPYKfLwyPgyCknwtt8570ZAG33mZYJ75AD2LXuWAWZjqGuZzDbN1B742rLWDdPQKS5YYUZIIjThERHgEXvvYQthg3HpytjdK/PYZMedXjzy1bU5iMM7tWAlz9pQo3vYWivGsz9solHnItzbEVst2E/vPzhAjTkAxy5z1544HHK66OMIJn4Rc0RhvbpgvkLl2BlmXYBNWYu6F4x3n/rTXRZfyvbSlZXvrX3DcdKWsuqjpDpc5jJbtadQtQcdDkA2fvVrYDOVQaRQnvk5FPhbT5iTDxv1qNVAdDWFZwxAxUAnunUwd6vdMvYSgCQLJ+kh4/tGWwyJnQ1uNvNCSA0i8e0gPU9dPFbscXaa+LVT5ah9MUH6L/BJvhsRYDGWmDIWnV48aPlqM0FGNanDq99vpy1eQXArptvwADIRSFOGn8Ybv7V/fxEpOXTdvZftXoY2q8b/vxlE5YXiTOo1RKjId+Kd15/Hb0H7ciKZsL2HXtfewxnOoZmdQB3Y0uZF1MmbvYP6iwAanIx5lJGUEeuYMQ44thTSAfYL35j1oOdtgKoWbTtFp5yBFXuFayszeLEegTNRhA2eVQqh7R1jFUKTe9d2+fPmknqIJGdM3deuzveWPQ1hvTpgTc+Xo6W0OddM7bu14AX/rYUdfkctlqrDnM+XSp1A34ONTkfg3vX492vWpBDiCkH74+r73uQh0rrU3z9OVx3zlRcc+st+HRpAFJOWUfIAQ/dehWmnnwG/t5SsB0nq/UGtlvrcN8A2VfAnRu7s6rTgYStJZvvIM01KrWKzTqC5lGvYNp+t51YALW/P3TiWasOgBOvoW7hpl18JwDg2rkuCHTfAEU+TbgWjjABnPJx0QV05YuoUbOQfADkZxjQNcCfZj+PSy+9Gj99YBq7YAkAw/o1CAcIAmzdtx6vfLyYCVCTzyPf8hXO/9FFOOfaW9GtJsTUww/CFXc9IACxO5j76NIYYLPuXfHBkmYsXlFk0VCPFgxdvz9e+mgRiiVSKaVQplJvYA0a8e+qXGZkf+V0ehJxiVOrmh+gEgCo2FU3jKjUI8gCYOCoA+P5M3/r7C5shXpFHeCkK3+GUrc+tjVLUt9epVt4hcQKYv9J+zOzkrVDKG26yH66xC+ggKB3Wzat5qHRXWsDYLv1u+PtT5dgcTPvtIN8AdhmvV549cOPUairwdDejbxSPdpg2svjiekzsfamw9AahejW4GNQzy7sRBJvH63yAPP+thCtQQ5dcsCgPr3Rt19vLPrqKwReHq98+CmWlZNiVBcAagZSjSCbgA7h3Rl2xSOveNs23+xmpnkSHAOQtjrt6QDkCp77zDTU1ta2CwByWux73I+oT+DB8bvUKrZtsSqPs01dwOW3IVqDtiEzzg97XvsAUBcqsz3SYrUDpiJceweb1Z51DKku4O4foLECmdDAbKxEqeoUHCHXLW3WSCMNEJPXkJo35Mj5QwEo2vVMduPmOINPQqCEyJOaQPXs0c5IHHnnKiXPNLXyUCwS5SPEpmWuyn87L2oFVAGALpx0PYRE+VQHcPMocrptDDmyTNyuUrt4jgU895QVAdWKQxMAjD4yfpe6hWv7qTQDcKwAofTUy25C2GNtuxeO7s4lWxnRxKk5qbWDNqnJBI6kwYMXUacuyZxNs3yj4Rq3a1b22+3UMlvLJMoh6Rb0yfjUreggIutmcxrAShwvuuopnz8BgFNkavdWFQ5RrfevbSNvVhR/NuXhdN0wU5Gkyq46vmizKyW8OHtMOFyzpE0YWKKBpCOY+eJMatrEOsJrzz+NhkKedYCNN94Yffv2zVCVPpbwg2Np17Axx8ZvP/bz7D4P9oQ5c+bgggsuYO8ZPfhuR5yOAdvtzCyNXpFvZJ+zc4acbMMqfB5tzaZcgINDZuUnvm/dLEnatkqvzWT7eFqXHLHTHUUy+wqol14TT/QB3DAxXTfdkkWijLp6+Xe1/zMckYUCo1UATeVfBrp6AWkRp25fbTljPlPvIHMjO7fW0jGBHXXoiHmrpp4RBXbjSGpuQYuHdg2j2ZCFRBty0LyStfP6rOdQm5P5XH/99bHBBhu0BUAMjD3uInjb7Ht6PPuha20r1+yRBIDzzz/fAmDUhNMwYMvd2IMlrdKTOLY7QbIdcqIUWS03lXVbtjLPEkxz88wK12ZkugeQA6t0IoimctnNpdJFH3r9rFLKlUBOzN8t2mRA2N+0vZwx94znzz2XV7i5kQJErQIbLMpkHSf9E4XgpOipCFCdgJU8FQ2+bK1LK104hdmY0/xem/OYA9QWpDvb+uv1rwgAGucPJl4Kb7uDL4xn/foiVN4wBiAAnHfeecxO6GFHH3Mm+g/eJQWANGhkotwNJemzym417/g7kssOgOg77fhh7WBHOczwldRKsrl8amam9gcwewQaYqZBkC4SIQ+jRP0Mwe1dKm/44OYJuADg8jCzY4grMlzG4vpBJN27LQDcLWP5eNMaJqd+FNJhgsAupLq8hz8RAPICgP7rrocBAwa04QBFxBg38Tp4Ox55afzQreeiT2NlV7ACQLpoAt+f8h/ot+kOzJsZ5XYLGJ1IUgbzberfdQQ2XdwS3nQDM44klfEJkeS6HDFkhU7zBpQBJwkpohQmx7uErmaOarTRynQnd1DH7HI23T2Mwr7C4eT+YaYhRaL9y7wlO4bKVdUDqptnazpLzpSiuXsGqy5AeyVQY0wOo9s8i3QvZgbAc09z1jNzgCoigFTbccdcC2+Xo66Jz58yBqN3GNQGJfTF3Llzcc4551gRMO7ki9BrwDYdAsDdQNG9sAsAngh16YLs66QBhMpHGw4m7ZcAoA4Uc1EFhFwrDQD7nfUetn1EzUSyu4GpbDcOm+QMY86Z+2vcX0GgANDPCgDyirp+AL2eLZo1K5kAII6fRAQw4Q1r502iDQC4yjoDAO3GXpcDXpv1DAo5AcCGG26I9dZbr82DL24BDjr+Bni7Trgh3mnTbrjsR+MrAmDevHk4++yzeXD0IPuccjF69h/OKVm8ebSacZohpMoOrcRYTKqsnGRlx+b/60o2GrfjKUxAIDEBBoBVAjVzxnAEwwjcJlWVAOB24nAf2GbqGAKrkpsQMq0DUJ4Bp4Nrgkcm3dtaA3aDyXTqnMQaSNmTzbNVBFgAsLbvIWf8/uIK1nZ7ZLRR80wSmeY6hoHXGxFQMErgxhtvinXXXdc+qo7ihXcX4tIr7oU3csItca74Oabfe1HF7Np33nkHZ555Jv9GD7z/6Zeh+zrDUgCgq5NzQqit3bcSACgIFET0Wc0bm/9uRAL37maHiLECDCCoqzg3iDC30U2nbLDIsQ6qrXwBVFrpU9GQ5QBZAOhnfRYCAAHbBYACnWW/40p2v9exdQYApOWTK1g9fZzyZpJCaX54y1izfTw9FwGkNgfMeZZEgCwo2j7e3TxaAXDdvc/jD0+9A2+XCbfEcfNCPH3/BbaRs7sy5s//M04//WSWPQSAg865CvVrDeGEULICdAJp73p34tUM1G3WEv+AXN32ErJl2vK93STJsm2zT4AZVBIjMNchjdjNKjbHZXvuZHUKfUYivP7GhHNTtgz7dn33SkwujM8ez9+pzE+UQLqX6gDEweg8NttMZhQrf7yNvSiBRHDaGTyr/TOn0NgA1R+bHUNFdAjBaffwV56ZgRojAjYZOAhr9+lnrXIFwJiJN2AZauCNOOq2uFAu45F7p4I2Usl6bt97732cdtpJNuHhwLOvQkOfBAAJ0XUbdCWkkZlGm84CwHr6HGWwIwAIy1Ql0CiPBgB2N3GL3nSatX7t1t4pYV3ORJVG3MiRo4FCTF7pTvazrHyT7+8ARsChMj8NALewhDmgAwCx40uG8AIA8ktyo02zsikrSM0/nQcBkIoGAURdHgwAsgKYA1QBwI6H/hR5KoYdcdQvYkLWkQcOxsRx27jVaPzw77wzH2eeeaqZiAAHnHUVGvtuzq5VmcBs0qWdal2zcq6z5FxlTVKdpNScAeAEfVwdQIGmQSTNKSxYMzETLHIaQKStgbQsThxDidnHz2VWshLebg9njB27CbRt+iSEV8CQ7kCv0PEx0O/E+umlm0NTOTptA58zHj3y7NGGGqT8KQcgczlg7Z9kvvEMqv3PzbbIHyDfU3e7F5+ebv0AmwzcDOv0STyBNMstEbD3pDsk6KUAWL5yAWb/9j/twJReb775Fs46izZdphUd4MCzr7YA4AdUj51lHSoW0o4gZS3ECWgikpQvVetNEmYm6peIGJ04k0dgHEUKAF5hvBuoiRZmOoBkrQpXBMjfyrHEpa3BHAWAbhBp07yNnU+ppUx4Iy70upUAIHdRX79po0fZh0Gy8wfZ+QULANlriaqYCTACAJMk6ngKlSOwDhAAs5+byWYgfb/xpoOwbl+K3ciLZufOh1/CnY98IHTY5Ye3x3RgixfhxTsnJdWo5gSyAs466ywrJ/c7+TLUbbi1rGmzTYuuTnlAU1DhuFSZiE6eoCz1JBuXiaOZP2ZJkl+br+f68p1ECRqopJWbxBByiIhbwow18Qfw7cz1qqW+adVu282hE1Yv5py4tLkLSBShrGFj6xqXidNMYdpLWF4mxpDpg5DzApb3pBORu5wIXKDPhgMQc6TfSdsrmC7rdDXxGDqdVUztYL0fY/asGSgYf8JmWwzFWj3XTAAQhdjh4IvgN24K3vWVAECrm2ru9h03BCcfMNweTH/893//NyZTHb5Z4T0GbIYDfnQdZUUDntMqzjynuDwjxMZxRIEQ5h5UWGFMH+UA9E55/fyuZiGtbDb2Da+1nTolcKTKI71L+5ikrTxbCXYrF2Mm8tSTS1f850lPI5lAYdsEP+PpM8UfqrxSmjhxA2L5EqFQTyFdKebu4EJxjY6qjqKJH4bj2S1x5Dh1ANF4af9Eep7aIKWAXCcAAAreSURBVAeK+uUYEOLhYwDQ3el7LrZXEWDe1WXui3Xz0YfvYekXC+AHeZ734TvsiIYaan4jr1YAu4+/DeW4G9PP2+XIu2IKIlCSZOvKj/HyA2fDp5QuG7uIMWrUKNtNm1jtshKxI9nGjfLqhQVJFIvDJdwhq4jm5mYM3WY49hm3Py74z/9EQyGHsiqFGV7DbgPSqg0BRDv3kTP5+ap0cfNo02lTXLaJc5WBpNYop6bRWDRRQ4ltAMmWAxHOKHpKeNPSTieME04yCqAAmbR4AZdwOGNe8nUYEXwMZRUryKUHsdQl1MYRh2y32HorWRhBgHyhwCFstgZyktHTs1cfHkrel/nVRaSeQjKPrXjzgIacLzoBxw9qsMsuu5jxyeYXN/3qSdw77Sur7FsAFEkOlT08cOt4rFNvepIZDXibbbbhzQeZ4MYZEXpmowWfJlSQSiuMnp06co4bMxYbbrQR/LCZMb6y1IrbbrrB5AsS3dI++JJRmvK2iZJRqMyeQDmTK1gye/rQcVkHk2uuMQnMZlLCgY2SqUWwmc+6kBVAruKYYomZDwR2vrz6K5Th2wzfJPlTCGU2pyp4GD58OHMxAkTOD5DL5TnplD/namS3r8Bszu10KxPzz4g+4nlu6r1u0UMWQW0XbL/99oZDiSk6/JDLkC8MYNnK4xlx1D2xu/dfqfgJXrzvbPgmMULFwJTjj5EJ94S16F5AdCHW4k24duKk8YxsdXXyGuGlEuDaa6+1LJrSv3ni2ptdw1orEYO5RYXKXSa6cVrJDeQ+7rFZxZJ/NyLKDkdFT9poEFA5Lue0hSF7F7mASBGHzLacmHmbbDyQmzdRgQsTlAI6zj8ivtulTFe/ywUSxTYBIHNj3vLWx+67757MbhTj9b8uwgmXPgpEdXaMbQBArPaRn49HL2czcWK1F150HkcG41DsTbKX6Z1a73ft2hXjx4srmbLjRFlKlB8FAOWoXXPV1RLXNzJRgyvVcKAtZvR319TKfudewxLc0xSv9B2qEa4aAPR6IvYSK8YltgBaREr2+kqsXN5HfX09thw6TI5T84/lvS9sv2dPTunS6yhRLft3REEWBEz8KMJuI0fbhUnXIWV1xKEXIypsKP2XDOfwdhx/D+sAdpUQgJs+xbO/OhN5r2BlBV104cKFOGXqFCxatAg1DY0YPXo0hm27Azdf8qMSs37aj9edILsi44D1iPnv/hlPTXuc9+Gj83RCdeKpFs8lLLFmXQk8RhtmlePczuDZlc0OHVspnNEVHN+9SyzexFTZeQUO40443V+tHrdRBXNEs8mVjlejqXT+mL334Z6FNFd6vSAnnLSmtl4yeIxMSla8UWqpxFEVbAM0ugYBh97X7NUXQwYPcdDOfcow9y+fYurFMxCS989srMHvO024NyUCqG1aLmzA1ReMxnYD1qjIoy0bzCzb9HyleWfSoUNs3qpbpmRZbkZGZDt3VOMcHX2f6g/g3LNDkZR9ZnVjVBl3xfFyvmJlsZQ2GtM3U+XS/TYrHjPDYLulFQFGHnYJ4vxGINHLHEWtpZ2OvjtzjonKtazAM/ceh4KphOloQlf//q81AxZIcYTT/9+v8NI7RXhxvWm7S2zO6CqVAMCsNQ9s178rrjt335Q8+9d6zNWjqTYDCoC/fLkcR//oAZQjaXrNQoubXxjrhQAgcjjxzDEA/Bg1ZeCOG47EBt3oco5W2M68t8fC2iNXW9YlR2e/t7pClhVnPneWlXd2vNXGkX2mzt73O4cubWcLD3scdQWioH8iyq15agCw88R7zLNltGXuxuUhal2JGfcej4ZKBR4VnqKzE5o9dTUA/rGQKMcR9pp0NZqi9YwprCtfldwKAOCVb12y6tnyUV7yN7zw4IXso66WP97ZFfKPfcx/n6utCmchk++MK+7EK+9TNbNxTRu/Dju+ncXsuRzABQBVjrBo4B0qffTqUsKvrp+EQhVOsBoA3y0YOwsAIv5P7n0Gj8z8CPAK3AybXp4DAPlsOMCuE+5hHYAaHomnTwPe6mnTYIuP9dZsxR1XHSvRqTZp09/tBKy+usxAdSBQ0kqEB6bPw60PzENk8jE9AwCt3LI6lAJg5NH38uIlAIjSZZRB65AxLIRy8pBDvx4l3HX10TZ9bPXK/+dCsxoAaOXf8dg83PXoG4i5YXe6RM8FQBsRIGZB8lJOkPKfGx84ZafURYvx8O1noI7PSg9pNSC+G0BUXfm0RxF8nHDZnXjvv2tQNgSwK19D3hW6tzFHIR1AfPXpPvacBed2tLZNnCjkSnVhS/Dw7adgzUyH2dUA+OcCoCkCxkz6KSK/F/Nuqaxgni4cXT9mGnlaUTBikogAdfq7UbZUtM22UTdhxIgaF7Zi223WxY+P34stBEqi0AH8XwNCdgV+189XXdZzHJxX/cN/nI/b7noWLXH3lMdElHcj0m0r1GSlcgzAIMAjALgiQAGQDbXaHjaa+EjlyZHELPzyYlx+3gRsu0k3zloRXeL/1utfBQCUZLqw6OGw469E0e8HjxNa0rPtAkCpoBnOstYFABzT2dkAwB7oxNkrxdDJSuATM4WWeYruFT/DtT+eiq026iEtYJkpcRqkRcO/GjA6Mq8669iq5sjqaBl06OmkBc+7kMX4bHmIyWffjKZib5QzOZWWfu08kBvS1iBVRQDwCjZA0HCt1RwdUeBekOhMrVTDOEQcLsWwzdfDWSfsi16NPmo0ieJfkDP8qwKADG1qV7M89nD7r5/Bo4+9Bq9LP8TckjxddZUKqdv99dqHngOAX3E+gCYyVNUBsteznCCdjaJAyVFPHJTQGq5El4YW/GDvHbH3iK3Rp2ueuQN3A6X4Aw048hBRShqJFN6HQHgGPWrlXQw6Wlff/nerO6ms7OCSbVeyyUSq0oCTk5SJOfIKpyxjSqLx8fHSVsx88Q08Oe1VfLHEg1/TAES1NqXNvU82FNzeEFMgcQ70RhxzvySEmFSmShk3FS/sAEDlitUsOd5MwaOQe+jkY8k69klMUC5tuQUt/hLUBj76r9cXGw/oi4EbrYsN1++DtXo2olteOoJSHrytqDFZud+etJ27wjcFAFcFcqdZkbOattpCenkELFrcgo/+thDz/voB5r//ET7825doavGQjxrh19RyEg6lp1HuozSsFxmv/pmsK7czT9MeULwRx/xazEDb5kwwZlO11Hww2bVtEhyqZGhQd24xL8WlrK1YtCRMk0riyGQQadEmH09brFKDpwjliNtwIUYRVPnapWs9uvdoRLeGenTtWo8e3QtYs3sPdOnSiMYuNehaV4ee3bqhS0MN8lQjb3vLylRJYYawGEoqVkIzy42AYgkIyzFaiiU0N7dgRVMTWlsjNBVb0LRyJVasKGFJ03K0rChz1vNXK5qxePFiLGtqRstKD8XWGGEYIQK1j42RNztk+j7tJkL5f1p4YoIzqq2b5NfQpjWnK6uJm9oHUK7kZi5pvwKb0VRZe8luDubtcuxvzJHa1Cld9Gi1R7M9XHYzpWrBIa0F1CJNt9WLy464Uz+7oE2Wq0nO1LJz2oePOUxU5g0ZpaMXnUOEpBq+Gv6ON7H2qfOXD48JQFuEU+NJgwDlWGZHEOV0tAOqWEG6Taz4zXVDSmKMUURp8EIpn/YDJnFlAEuta+hakqcrG1HyK5Ln4ms5OpCyNE09s+aaAQCl58vxSU8lmQDtxZQ08tCVLccnCzeNlHT2tQsAOv//Aw4A3DGXD92kAAAAAElFTkSuQmCC`;


        GMaddStyle(`#vip_movie_box {cursor:pointer; position:fixed; top:` + top + `px; left:` + left + `px; width:0px; background-color:#2E9AFE; z-index:2147483647; font-size:20px; text-align:left;}
            #vip_movie_box .item_text {width:0px; padding:2px 0px; text-align:center;}
            #vip_movie_box .item_text img {width:36px; height:36px; display:inline-block; vertical-align:middle;}
            #vip_movie_box .vip_mod_box_action {display:none; position:absolute; left:36px; top:0; text-align:center; background-color:rgba(255, 255, 255, 0.2); backdrop-filter:saturate(1) blur(15px); border:1px solid gray; overflow-y:auto;}
            #vip_movie_box .vip_mod_box_action li{font-size:14px; color:#DCDCDC; text-align:center; width:80px; line-height:27px; float:left; border:1px solid gray; padding:0 4px; margin:4px 2px; background:rgba(0,0,0,0.6);border-radius:2px;}
            #vip_movie_box .vip_mod_box_action li:hover{color:#FF4500;background:#00ff00}
            #iframe-div{width:100%; height:100%; z-index:999999; position: absolute;top:0px;padding:0px;
            }
            .add{background-color:#00ff00;}`);
        $(function () {
            $("ul").on("click", "li", function () {
                $("ul li").removeClass("add");
                $(this).addClass("add");
            })
        });
        let mobile_html = $(`
            <div id='vip_movie_box'>
                <div class='item_text' >
                    <img src="` + mobile_ImgBase64 + `" title='视频解析'/>
                    <div class='vip_mod_box_action' >
                        <div style='display:flex;'>
                            <div style='width:295px; max-height:300px; margin-bottom:10px;'>
                                <div style='font-size:16px; text-align:center; color:#DF0174; line-height:21px;'>全网VIP视频解析</div>
                                <ul style='margin:0 5px;'>
                                ` + mobile_innerli + `

                                <div style='clear:both;'></div>
                                </ul>
                                <ul style='margin:0 5px;'>
                                ` + mobile_outerli + `

                                <div style='clear:both;'></div>
                                </ul>
                                <div style='font-size:16px; text-align:center; color:#DF0174; line-height:21px;'>B站大会员番剧解析(专用线路)</div>
                                <ul style='margin:0 5px;'>
                                ` + mobile_innerlis + `

                                <div style='clear:both;'></div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`);
        $("body").append(mobile_html);
        $("#vip_movie_box").click( () => {
            $(".vip_mod_box_action").toggle();
            /*let display = document.querySelector('.vip_mod_box_action').style.display;
            if(display == ''){
                display = 'none';
            }
            switch(display){
                    case 'none':
                        $(".vip_mod_box_action").show();
                        break;
                    case 'block':
                        //if(blocknum < 1){
                            //$(".vip_mod_box_action").show();
                            //blocknum+=1;
                        //}
                        //else{
                            $(".vip_mod_box_action").hide();
                        //}
                        break;
                }*/
        });
        $(".vip_mod_box_action li").each((index, item) => {
            item.addEventListener("click", () => {
                if (parseInterfaceList[index].type == "1" || parseInterfaceList[index].type == "2") {
                    if (document.getElementById("iframe-player") == null) {
                        let mobile_player = $(mobile_node);
                        mobile_player.empty();
                        mobile_player.append(mobile_videoPlayer);
                    }
                    innerParse(parseInterfaceList[index].url + location.href);
                }
                if (parseInterfaceList[index].type == "0") {
                    GMopenInTab(parseInterfaceList[index].url + location.href, false);
                }
            });
        });

        switch (host) {
            case 'm.v.qq.com':
                //--------------------------------------------------------------------------------
                const txDisplayNodes = [".mod_vip_popup", "[class^=app_],[class^=app-],[class*=_app_],[class*=-app-],[class$=_app],[class$=-app]", "div[dt-eid=open_app_bottom]", "div.video_function.video_function_new", "a[open-app]", "section.mod_source", "section.mod_box.mod_sideslip_h.mod_multi_figures_h,section.mod_sideslip_privileges,section.mod_game_rec", ".at-app-banner", ".bottom-banner"];
                setInterval(() => {
                    txDisplayNodes.forEach(node => {
                        $(node).css("display", "none");
                    });
                }, 500);
                //处理相关推荐视频
                document.querySelector(".recommend-long").addEventListener('click', function(event) {
                    const clickedElement = event.target; // 获取到被点击的元素
                    if(clickedElement.nodeName == 'IMG') {
                        const imgSrc = clickedElement.src; // 获取这个元素下的所有子元素
                        const cid = imgSrc.split('/')[5].substring(0, 15);
                        const playSrc = `https://m.v.qq.com/x/m/play?cid=${cid}`;
                        const aNode = document.createElement('a');
                        aNode.href = playSrc;
                        aNode.click();
                        aNode.remove();
                        //window.location.assign(playSrc);
                    }
                });
                break;
            case 'm.mgtv.com':
                //--------------------------------------------------------------------------------
                const mgDisplayNodes = ["#app > div.with-gray", "div[class^=mg-app]", ".video-area-bar", ".open-app-popup", ".ad-banner", ".cetbox", ".mask-bg"];
                setInterval(() => {
                    mgDisplayNodes.forEach(node => {
                        $(node).css("display", "none");
                    });
                }, 500);
                break;
            case 'm.iqiyi.com':
                //--------------------------------------------------------------------------------
                const iqiyiDisplayNodes = ["#player_bottom", "div.m-iqyGuide-layer", "a[down-app-android-url]", "[name=m-extendBar]", "[class*=ChannelHomeBanner]", "section.m-hotWords-bottom"];
                setInterval(() => {
                    iqiyiDisplayNodes.forEach(node => {
                        $(node).css("display", "none");
                    });
                }, 500);
                break;
            case 'm.youku.com':
                //--------------------------------------------------------------------------------
                const youkuDisplayNodes = [".callEnd_fixed_box", ".callEnd_box", ".h5-detail-guide", ".h5-detail-vip-guide"];
                setInterval(() => {
                    youkuDisplayNodes.forEach(node => {
                        $(node).css("display", "none");
                    });
                }, 500);
                break;
            case 'm.bilibili.com':
                //--------------------------------------------------------------------------------
                const blibliDisplayNodes = [".launch-app-btn.home-float-openapp", ".visible-open-app-btn", ".openapp-dialog.large"];
                setInterval(() => {
                    blibliDisplayNodes.forEach(node => {
                        $(node).css("display", "none");
                    });
                }, 500);
                break;
        }

    }
    else {
        var node = "";
        var player_nodes = [{
            url: "v.qq.com",
            node: "#player-container"
        }, {
            url: "www.iqiyi.com",
            node: "#flashbox"
        }, {
            url: "v.youku.com",
            node: "#ykPlayer"
        }, {
            url: "www.mgtv.com",
            node: "#mgtv-player-wrap"
        }, {
            url: "tv.sohu.com",
            node: "#player"
        }, {
            url: "film.sohu.com",
            node: "#playerWrap"
        }, {
            url: "www.le.com",
            node: "#le_playbox"
        }, {
            url: "video.tudou.com",
            node: ".td-playbox"
        }, {
            url: "v.pptv.com",
            node: "#pptv_playpage_box"
        }, {
            url: "vip.pptv.com",
            node: ".w-video"
        }, {
            url: "www.wasu.cn",
            node: "#flashContent"
        }, {
            url: "www.fun.tv",
            node: "#html-video-player-layout"
        }, {
            url: "www.acfun.cn",
            node: "#player"
        }, {
            url: "www.bilibili.com",
            node: "#bilibili-player"
        }, {
            url: "vip.1905.com",
            node: "#player"
        }, {
            url: "www.56.com",
            node: "#play_player"
        }];
        for (var i in player_nodes) {
            if (player_nodes[i].url == host) {
                node = player_nodes[i].node;
            }
        }
        var videoPlayer = $("<div id='iframe-div' style='width:100%;height:100%;z-index:1000;'><iframe id='iframe-player' frameborder='0' allowfullscreen='true' width='100%' height='100%'></iframe></div>");

        var ImgBase64 = `
        data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAAAXNSR0IArs4c6QAAIABJREFUeF7tfQmcFNW1/ldV3T0rmwgCLihuoCCK4o4iiolCcN+NIKiguK/xqU/jvvw1GtckT+MaTaJxTVDADaOiRFFcMGryEnEHZR1mprur6v/OOffculXTPTNozC/vhVZ+Pd1dy617vnv2c6434qj7YgCIAx9xHPM/95X9bH/zyvA8jz/SMUEcmZ/k/Eh+4mP0H30Ogpz8bo72Y8/cV08IzXkyljCW7z0/kuM8H77vIxfJdQEPMf3H16FPPCBEfmo4/AMdH/MfAP3sydGIeOwRf4pjPVHOj32+OnJmwJ4n85TMQ5j6HAWB/cyHxX5qnrxYnsOPA0RRBHj6vGa+opCPp/mT4/R+Hj+3zmnZjB2xzCcdq/Ohn+VdZ1qeP3nRZJXgEQD4hn7yY1WiOzeKUXIuhhQAmODuHBkQyOBlQgxdgbiMMKRJiHhCopCmG4ghE+vFRf5EF5TrhgiCAMjnmDD5vM+fA8Q8QfkgQD6fp5/h+x7o0FyOJon+ybVBExFFdkJ4PIYwfN8oRrlMh0QohQZ4BKooQrnkyTiJeIhRKoYMqCiU7/wyjdVDWWAJRAW+T+gTYIEwyvHnnC8EJeAapMk7f0fPbb62E2kWgiFi5AfmAAGAg8jMIk4v6AS4RSDOEwDuEgAEZjUbImeuqnhDHAti9RVkrk/D8nwhXhSHiFGPKC6hGLZgje4+Nt+4D4YMGoDBG6yHNXs3oGtjHg20IgncMnfmwXghI/Bj/t5nIqVvlkZ0dsT/vM/tLhjzPBERmv+X5wnNgzbHQNOKGAsWL8F77y/Aq2/9Be+893c0rfDR1e+GEnEBgrdPnEE4KwPKvCMWIMTmel6U5sAJr207H7ygdhl/N5/hAoAvmJ1se76siCwAlBg+odcP0dSyFDsMWw9Tjx2LtbvVohYhfGb8eR6TZdFCdwswZVi0ounl6Qpxudc/j7bf/k6ZlcwiyBWB5g5+RESkeSgzt6Mp/srL43czXsXt905HfdAbUblAEsUCQFeKcFQjws317HRZVpsVAXKgBUBoOAC8RGa4hBY2SawpswpJrkcQmYYWdO0a4dKLxmPjnrWoIbZsSfvt5/Lf8Qo020VEWBEBF9z8a7zx6pfwg24ISIvxiiCdgxcsazXuK7NiMrqNHtkuAPSgRMEgcKYBQMpRzmtBfeNK3PGT09Doh6ghdsTIE7a3+vXNZ4CWI0nBsgdWRJt94OePzsKDj74GP+6G0HCEVQWAcmxv1/F3xsT+VWu3RM+yEsuS9QcPfhQiKn6Fu286FWt395GD3waH1R69imrS6ZlyVIVOn/O/8cBq62cpgEnn3IovFhWAKA8ExhrxjHJINg4vxLTVktWbLABiP61kKYEc24DnT1l6DcrYbmgPXHDifqjxIkREfFeYdzDbqwHQOThWAwBZGGRwzPnLFzj3ygcQxl2NmauiIAEAi4gM5xYLJIa309F3shUAYwa21WhVGZMBs8FV+grXXzwRW/SvR2ARV/mBvi2hOzdN/z5HtQVEjK/h4eBjr0Yx6g0vJg9Bnq0EWbGGAxgdICXOyQpQALAGWsHUEh09WflR8xd44o4z0K3GY9eJMpzvitX/+5C2c0+aBYAw3RjFGDj+4jvw4d/JzMxZM1HNQzYfUg4juV+nOECOFTofpZa/Y8a956HWI5OufdKvXvmdI+g3PaqSDtQah7j8F49j5stLkI9iNu0tHYwXUYGg920DAHdAwi7EzIuaF+DpX52HAOQIImWvffV+NQC+KWk7d14WADTfpINFKOO6+57FYzM/QOTVWc+aywlcRdDbeeI9xjsqJEv8Bir7faD5M8y47yy26zuy61YTvnME/KZHVbN+3OVIrvULbn0EL766hIMerA8YXY2AoNYBiwACgCDCOIBsTMDI/tJSTLv9RNRxUETDJ9WHvxoA35S0nTuvMwCgK5FddvDp12Ph190YAOySY49hAgB2Be88UWIBViaoT9knjT/EzT8+EAP71LL+X+m1muCVCfdd+yk69q/FaIWH3Y64GF5+HSsKNBinVl8KANY75JF2H2O/3Xpj6qF7SiSuisxfDYB/TQBwaNyLsTQCfjDxFoRevQ0Xsw/AWH3eLhPED0DhYIlDk0cvQq70Oabdfjpyqj12jkPZo1YVGNkV801XUMcrQ4aYHV+1+1d77Grnr+rxnZ2nzj6X3j+5bhl3PPYa7nrwbUQB+Y1FFHheIO8jxv8yJkQoAEgFCLwV+P3PpnRK6evsA3eEn9UAaH+GvikAyBAk7W63Iy5EubARc4U0AI66O/YDSY4Qj2CAEw8biv1327wjmv1Dfm/rcv6HXPbf/iJZzrJwZRn7Tr0TvpdPZWl5IzIACIsL8fRdpyLnZcOL382crgbAdzuvydVD7HPST7FoeXfORKJ/vOhHjr8z9gOj5PkxrrzgB9hm3Z6VzX12A9hsvg5G3tnjOjkB1YTlqvLGTt6ujTLzXd2ns8pIdqVUXTnpebeHxTE+bYpwyEl3iRtPdQECAOVpUjJkXPwY0+8+mwM+qfxBozW1eDHKkYdymaxMymmTWckEmsy5yQjp90ppU3R+9je1RLLHa7g65kRKzpjkHLzAPIhL1zYhTyd5tT36u/ek+Du9KMOJ7seRTpPk2plr2AQaJx8yUT913pLcPz3eJkCZ+9mUQcphJK+/jstYZWGs+Yly9Xw+QFef8iSTuxnysXdg18OugFe7LluBzAFGMQeglB4Pkw4aikP33CrzfBHC2EdTOUSZgLEKK4g8UprJqiOPzEyy25KISZqpySIm4lIWrLwEyZStLITQb5OwNWfNaqZsLHqMAiXJ4k2PuL38PUn0NBNnRKDm4FGuHV3fji6KEWgWFY2Wxm7GUjF3scKKTR+XPBfn/JlVZYFnx5UmgM3FNsfTbfIUpPNjNBTaRmymz/kQF980CznVBUZN+C8GAM3z4z87FvkMhekGzcUYxahEQUb8/g+/R2trK48iyOX4oQv5PL/TJNBDlcOQ/w7LmvFLUcm8rCDIbzTQKJI8Fs/3EFLG7f+4MEOTJex7cm1KBqUhsQeLYBt6nEWcIhanj5f590JOxqKrVblBzqRrS7KqpJwHlF6ekyxdzfSl38rlMoqlEP3790e/Pn3ZZiy2ruDvyxFlAVO2sSwxn1yshiP5QcDjZY5B81Io8NzQKwxLKJVKCMMYtbW1jmNGjvXiAH7gI/ADmUuzQpkDcXa5OY7uEVAkRgiV8wO0tDRj5cpmLF++HLU1Baw3YBBHbApehC6FtC5XBjDi8J+ikO9uOMCE/4pzXohy6XPMvPuCTO44pTHHWNq8EldfeTn+NPtFxKGsNN8XAtGEMStRhCqbzMoFu64l791zwpP8gJmkhcoio3p2C0+8ubfGvPUa7rt7H2v5OOe5HIAIPnbcOJRbWhHFRQuSLMAIDDInolxxmnqQ/M3f5YiF57HOuhuY+ZK1q2NI3tvWUui15d3cKwh4IS5evNSCUa9BYB6y4wjU5+qQD4C6IKEPLZS9J9+E1lYDgNETbotzKGOfvYfiuIN2aBPhbyqX8dTjv8ctN98ILywyyxYNUgaiygLHG7jwQl68aisUmUSUHcyIN75pcwxxADv5BlR6flam68Tp8S4As8fSNbIAcM/LXssFAP1dLJfx/T1GUwWA5ToqetKEkXmpBgCqO9h4o0HMQRigpEU5RTOVAFAJIAQAAubiJSuY8GTXu8+gukSxHGGX3fdEVC6jsZBzRHcJj/zxr7j+Fy/L/UdPuDUuBC144BenosHUTFhKAJyX/v09RjG7JGTlyUmcAzFyQ2lT+UNuI0NMkZXEbKjqJi1TOO3ZgMXVAfxICBUbIa6MSzNblGMogWkC3ZedQIcTpQBoKnBE6UwmP47UMybjLHNKtkwqPTP9ve46G2CDAevz38QtWUZTaIxEgS8iR/+RqOG/c0ZMEKf0PGyyySacqFGiPEqUkTOAJ9bO88WSRKudqNCGRBNQ1t9jnwm/sqlFVjwF57hmQF40HvqfvLik4VNeEM3vzruOSnEAerSVHjD2yLsQ50vwvj/+xhjxMjx257nIt1H9gSUrmnD4wQegXJJKoOYIGHv02chTlj/J7Thi+U2hB3cF5+KCENqnyp7kFYU+V8zwQ5OiRUSnkRsZXajJsexUP4TlKqZkio4nXcELpTSNy8RoJeh7TmQzqat25UtmlF2dTDjDtnM5o1vQGiFdAqTcBSyLc/kc8rk8Ghq6scLHZW1xK8KQrA8hFhXKqP5An/N+wOMPCjXC6YI8yqUyFi1aAgJb926NGLB2IxUmGWtKqqGiqIzQcAdRnuvw+bJmtBQFoFz5RIT3RNnMB0bPoPEbK4X0mffmvYJ8uYnjN/TaaeRoqsRIOABlGCPCLofeiHxdI7wxP7w+Xhl/iafvvrSN/KcLvPHmWzj3nDPsZG75vQOwxdjJtnTLrjKbTpbW4lUUqP3gW1ekgIKKRQQMRjegsqnYAxV+MYDYCmMDKDU+ZX3KTViZNGFt4UCJTsDnG3VaOQiNR1acjMMVHZbLkIQzjyPj0GMlSYbHTzTxhShcxETp24bLlGkUYYiWkJRhH19/1cSh2TUbPazfh3L7Q1shxCKUr+fjwy+a8VUzmYh1iH0FugGwmb98zkfeC1Egq548uT5pGB5q/BjvzpmFwMzn0K23QY8uXVOLkGj2vUnXoVzqDm+vI6+Ot9yqNy4/fXzqIP3w4kuzcdklF/JHOnH0UWeg93Z78oOllCzLPTT3zBDAsHyaHCG0ElLNqsoA0GJTS0gHAC6hdZy6IrnI09QmKiF5Jao9ZYitACC+mRA8EVeW2AYAWb8oAYBVIOIsQYS8H4Mkbc6PWOZ+8sXXqGnsxrV/BACqNVz89UrEXgGxV0ZjbYxN+nWV830Py8oxPlywEi0RFTWSu1au76vO5InJm1NO5qMNAAqej9ocMO/lZ5H3ZR4GDdkCa/Vcsw1tL7zxccx69TN4I4+8Mr7szEOw49D1KwLg+Vl/xFVXXGLhv/MRZ2CT4SNB6KaKFa1Fa1uYkFxOVrAsGWJl8llWvBJOOQCVSBFxAiOLbeqZLQ5Nr/QsB8gqRQngklUuLFWjnwaIBqBJVbOMn0x9BoPxghJv4TIurmWUyqf6QowuNTGef+ZZzHr+JfTo0QOlljImn3oKSswBxIH21eIy4lh0IKrE6paPUVfrY+GKIlrjupRSSIJSOJeMW8ZBABNOQOa6H4TsCMt7ZMaTHhKjPu/jzT8+gxry7noeNt98CHr16tWGts/P+RvOu3EGvF0Ovih+7O4L0a2mIv3x7HOzcM1Vl7GMpRW//aGnYrPtd2cAlIgoqry1UxKiAFBZlgUAs1LDsqi4hApOFTAqAlSmJaw9LRLacAA1Cw1HUKBkRYD6nVRE0Wfj/EsDABFq45BXe9d6SY77cnnMetOSlSux/ItP8OK038ErNIiy6Pv4+5cL8aNzz2MAtIQBliyLEMUEAipfJwD6iH0qJ5fSdJcTVQMAyX7WNaoBIOdj7gszmRMQUAYN2hxrrbVWG+J+viTEISffCm+Hg86On//NVawoVHo9++zzuPrqy61GvOOBJ2DALmPgUek0USfKFJRkgkhKQCIwsUhlrbTCeYXRcjIvRrhJQlWRoYZlIqOdyCWv0MQMSrNyua7rH3B/t+PQChq1gMz16GxXF2gIYpw56QgEhRzW6NEDH/7lE/zi3gfg53L4YulyFIIATzxwB2oi4Yw+xUxCH4OGbYMNBm+LpSvLojQb3UQTMtxnd0WbiAASmULwhANEyNMcBwQC+dujLG3SCWIPdQXg9VkdA4BU9j3H3wxvu31Oil98+Kd8g0qv559/AVdddZkFwPb7T8GGu45lHzk9qFtTxmZcBgC6YisBQB5QRIGuUBIRUmzaVvljzpGxewkACTgSU4q0c53A7CTL8caMc0CpMp04QNrI5E4AePah+/Hhm3NF6/cjrLPh5hg/8Wh8ubQECqjkvBh333gN1uzaHUG+gE+XLEXPPv0xap9D0VpsNX4EoyNpebcDfgWA+FnSACCOIDpAxAupEgDI/V1X42GuA4CBAzdDnz592pCWBNHoI6+Ht+3eU+KXf39rVQb+xz++hMsvv9gCYOd9J6P/yH0RoIwSNVUwnSz4Dk63CiWoKmWq1WvOqRKO0Mt1B0ZJK8TqV9CGENVkfpoTaKKDS2weQ0bJs8CyoJOJZZZLLnHE6NaQY5X8y2Uc8hJwxhFqciEO33sPDBuyJSI/ZJf0VTfcjEXLgJXFIspRgNpcC/7w0G9xxOSJ+PCTleyZVxd5doEQwW05vGMduStfRQHJfuYERgeg4QsHEFYvuk6EhkKQAsAmmwxEv3792q7tOMYeE66FN2zPCfGcp+6omuf/ElkBl/3YevZ23ncKNhi1L/y4zF0wyANgTUEDgPTdhFDcz4P978ZutZ6wBAAs21i7Tsw+V+a7clx6DchLVkwSVxVOIVzFaXxi7WU9h6/HZpaPLrkSHnvgflx96SXos2YPfP7115g2+x3EeUqIJUYXMVB7d/cx9ajx6Eb+fAAff74Iv3zgfny0pIzIL/B8kMukzEpiBD9KWu+w7HdEDl/XPAMtCF35Mm6dJ/lefydC63EdAYD8GRtttAnWXnvtygCYeBm8wXv+MH7rybsSIzdz6OzZs3HJJZfYoe5w0HHYbKf9EHplco4itPOerkHTvAHVtn31vnGYgoIYhtAmokcrk1gbdRkQwshAiK4ugROZLzfWidJh0/0kmJIWDXTBHPU1MqFpcmXLBUIUvAB9u/nYc9tt4dfWyoQjwMHHHIPRYw9Biwm9MIDjEh684w588PabWLJ8KRYuWYpzLrsSfftvytFSlvEm4GQuLwAynj8Jg9FLs6xVBCphBbicvsf+DHG503zx94GYm6SsEgDI7mewGxleX/Aw7/lnUJeX62244cZYZ5112gAgDmOMOe5ieJvvcWT89vS7qwKAIkwHH3yw9XiddcMvsLKmP3vxSrSqTbOopNeNeTBtfmSeVx0+RBgGgMajDSdg048cgmr+GAKRjsBAMBOh11HR4ip5PEHm+IAaVtgYPjloIhRo8vyIPX3NJePJoxVFEbqaGHvvsB26NNTKhEYhFi5ehul/eh1NRerzo7GNCI21HrbbbCAeemo61lpnfbQUgXJIHjYxF934g7rCFQAk7ehvrarUcInrkBIRoN5SIqQczX4BBoCYynnmBqJY0+/k7cwHMd5+YSZypBTm82wF9OzZsw0AiDuNOfYieAO/d3g8fxoBoHLe/9KlS3HIIYcwiyWZd/LVtyLstjGzRH7eKlE/946ukqYESh5QOYE8lPUAaqAkY8bZ1jGWxasrNJkwQwdZMR7572P06BJg0uGH4a/z38fKlSvx3FtvYWWYQ6AszPfQvaaIvXbaHjW1teya9cs+9jzgEBx83Km2a5d0/SLfu5iLmtBCYGMWb4EiHFEBoKCwHKBCxw6eJ884xoyoEJYvRCbCs+yn91zMIokcXKT9E/HJI0kgf+uFGezCJgCQErjmmm0dQRSUO3Di+fCGjD0qnvfYHVUBsGzZMuYA2sXrpKsIABtZALSFVvqbrIbOYYoUwg0nZuJT5k1auaMJYA6gMtH8rt8pB7ArxFgz9JmJ48fo0xhj9y23RF19vc2oOefCSzB4l91tsyKSzrS6mr9YgAmH7M+++kVLmvDw44+gfu2BJOwM4OUGoTZn0kQVAwAOyvC9Tfs7Ui81ps86gPnd6ciRmiOjDJODh15ZABChydVcCQA0fvIAKgAoNkAcoBoADj72QniDxxAAbofnZ9uNCWGyADjxmtsQN26IUO13Tzxb9pVRBPXhdOVbQiqLI7lqPH/8wOqj1xiBMQeV5SvhmV9xWFm094Y82Pv1xtw3sPHmW0sql0AHQS7GB2+8iB+feRrnMdCruaWI38+ajZawIG1WDGGIYzQGRZS8WpRKIVopaMOizvTiM1wviXLmmMDUz5BXuQlCdQQEnS83WMpzo+aviVNI6pb8o5EnAJDIn7B+yfwhABAHmDdreoccgKB48DH/QQD4YfwWcYBOAIAGPfXqWxE1DPhGAFDii5YvK50HTrJfAWEcJbryE7MtzQkCP0TXPLBwwV9w8Lj9sEb3LuyPj4olPPrCS2imQIomq/gRaup87DpwENZaUzpplMIYi5evxKy589BUosCLydPjNyGEsO1Iau1NXoEuXAKArGwBAHVIEZafFgWqG7miwBKf/SYGpirSUkEssWIUAGTyMRCYA1QGAHOAWTOY/ZOuQxygkiuY4p6HHHs+vC3GHBG/yQCQ8G325eoApLycePXPEXVZB6FV05UDkIOcMZq6BLWMo5eGd91oIMkwdvs6gHB993SeKoW68gu5EJedfTLmv/466mIffj7diZRWw9KVKzDj1dewtJWuL7pB7MVYo97Dbltvg002H4Tbfnk3ihHQUmrbf9DVa1TG63dKSPaCOjqQcIAc30c4gkyDigI9lqIH7elNlkMq/2JrwMh+Ij4HoOSZKnOACG8+Px2FfKFdAJAGd8Tk0+AN3uuw+K0n7qwKAFcE0MBPuuYXCBvXXiUAyEo3ypoTDVQAsJmnfgEnU0YBoIii6/RqLOPw3XblHDlaPYnVoC1nY3ZQrT9gQ1z581+RsivuZ66QpTy5GK3swPKQc5oVpiKbDoRVkqgSpwSkVDh6kfhIOEBgRQUBQM4xnMUcn02Q0fuqqLT+CwcAWp1HHEAacUr4WQHAYOCkEtEBSAQQAEgH2GyzwRWtAALAD6ecAW+z7x0av/OHu6oCgMzAgw46iK0A4gAn/7//Qqm+L7RVKblIddDKcgX6kmzJLJxCkyY8qTI+b1aI9Qg64WJ35ScearFrG/JFTNxrJP+tXEHfraYdRVixYgWenfsWlrTITLoNLcSkS/O6agBg7cBR4lxOwCvdMLzIKIVk4LEr2VEK5Zy0MlgpepqAIPFgqoeT5pHlv4aJ6W+jA7gAyOoA1cxA0lbGH38mvEF7HhK/O+0ejkFXejU1NeGAAw6oCAB+MKdptHs+pScIIclUSQOAvicAMGcwrDSJGWh41lgDNigj39fkI4wfvTPqa8RsdTVoFwA07umvzsWKssnedbLi1OYWtqysOo2IhE2nV3CWE1AKGV/H6gJyPAGAsp5dZdCKAVYWM6LSuqZlPOrC7gwAskrgm88/1SEHoBEcPfVseAN3PyCeP/3XktpS4ZXlACwCGtZGqG3GTF/gLDGUA2hmSiL7RRRw1o8J2KR0gAwnyEbtKAZ+/Xmn4sO335CJVyXKccCQv2JFSxF/ePk1lEyFh61o8GO2FkguMJEp5dtpf0xE4/RyQx9qHE1jKIVAiYimSaZewP6FiIJZpmCjWA64WZPa/67+oP6BrEjI+l/0eW3MxIllVOMAzA09EW/5XIzOAIBEwMQTToO36aj94/dm/GaVAFBu6McigFm+UfKy2OEEJZMcqna/uHSFYgSAvLp9jV/AcgxHKUxH7SQa2MVbgqPGjTW83UQSdeJNJ+8vv16CZ+a+g8iYpWJNANMevBc33/ATSd+KY/Rbdx384v5Hbe9iWs89CiF2Gr6DuJTDEM3NLXjmtXkokuPFKH6U0bPH8O04N49epVIRM+f8CV+0mDoA0omdiijiBJV0AhcALmdS7Z97L6uFUEUEuADIBYkO0J4V8A8BgPCqjB+A10POhnPp9yRaZdyWEfmxE0dH2ixUH7/QN8ndKxrFx0MNVuCoH+ydYv8ue2Vr5cyzMWKv/ZkX0/VJNtfkQ+w+bCs0NFD+c8RZtjNfnI2mUPQVfhy/jB2GDEKfNdYQDhMCt993DxrX3tiwelIegb/OfwXnnnq6xX1YKuKpV1/Fp8spBkT5TgIA1QVUpBC/SFkBWh+h5l4qeimlaTw/xhpQbyBzA0cHYK7FHCBtBVBGUGVXMHDMSafBG7THgfG7Tz3wjThAuwAwKV9iwkgYU8SErH5JDkn8AYlfQFPGTMKIWg++KJucd1dajAn7jbPeQaWCymeqwHl+9iv4slmSQxkAfoSucRl77LqD7B8Q+1xY8czLc7AySsRfYx2w89DBaKjvwpdd0bICz730BpYZbY8cPbW5EN8bPgyNXessRwiLEZ585RV8tkL6JKcBkMQHVhUAtvHrNwQAWQGVPIEk+o45+TR4m406IH5nRud1APIDqBnIqybDAdRxozoBs38br5ZEDBcA5KolX7b6wNXXn80byEVmhxI/QL/GGN/fdUfkKX6R2eiCEkGKxSJm/PF1LCsR9xGWTEGg6y7+T7w06xn+TK7eLbfZHudffZ0A0+T8rVHnYeT228MziF2yuAnT58xDaOK2FFTqXhvjeztubzaiEOIWW5rx5Gtv4PNlciC5ipXlpx1ElXdmyZqBGt5W3cVyAGNVUXyAzWgTO1AzsBAkHKA9VzBxp2OJAwwcuX88/+nO6wAKALtjxTcAANUUqAOoEgCEM6SzhvNmowo2BesC3HTNj/HBO/NRbjU7l3iSa0fuW3rd/9ATWFGmNDR5+fkAh479Pro0kou3hHIY4+HHnkBTmZO/TKVShAP3HYOe9XVcwUPK5KXXXY9ea2+CODKFLn6AT/7yLi4671zkC3neXaRULmHf/Q/CHvschrKpGaSC2iRY5JqFabNSF4pysQQILgcUP4b1ArIfQACgWcMJANJKIAWDKnoCiQOceCq8TXfbP35vZnUAqBlIMpMmuLMA0DRsLd9WViwyTHLdOLVaw/I2NqBp4jIl7OihNCitzjXHNS/6BD/76U9QKrWIMkqFpkbJZOUNebtfDiV8kJNEZG/EhNWYAAdnypTDJwUh6sm09j7b78QxjK6j43VctgSo5v8peLnw+lts4WlkACAOIQGAcB7DIarUWTP4WR1xs5UTRqd5AaoDkI7ATiHjCMpygGoAoPFMJg6wqlZARwBIWJauYE1qFCVAq48IAOTT5n0PeFuBtP2fMoMqAGDpgvdwyuRjuEbRKhdmGTGqFqlSAAAgAElEQVTxzIzThMpOQbpHkNYtmOieSUIlJNJ5NLGuQ0tZP2VFM3FoGxzjw7c+gThGS5zDQzNfBO9wxDEBAY66hPU6NjEks/mU6wQSrmA4l7FerFWgeQFGCWwPAO3HAoApJ5MnsJM6QMIByBXcD1F2syBlterbN/5qWvmSc2fMP0P4pFmRIF4B0EYHyHIARkwZyz77ECcdO5FzFXnydHeuCnkKTNCM509ZrvuelcNKaKsjZE5SABBHKXkF/Gb6LGmgwfEAwzkUEEb7J9e06xl0d1cTwiesnz/rAjFA0OTdrBVQjQNUcwWzDkBK4Oa7Hxi/Pb26FUAu1QMPPJBXEA2cHEHsBzAAcFcLy9p2AMDWAPsA5EGF3bUFgKsDMCAocdQ2aKAqnxBLP/kAJ0+eVBUAzEQdF26SCpZOf3Y9iVl5zNfIOJoUA+7qJwCEQS3un/YsN9MggyHidHnJ+WOO0A4A3DF0BgBJeFjCwWwdWRGQ6ADEAapZATS2404+Hd7g0fvFbz3526pmoAJAYwEKAJsPYGbEOisyZc9WB1BzkB1A4tBJmYGG5yWVQ5q2negADDDiJQYAp0w5xuYE2tXrrHQXAPp3S6mMm2+6CcccPQldu9UhCEwRq3oG3SzSSmzCiJYsAOJCA+57YiazfgofE+GjMNn/T6OC6hdQXcPdqSUr+5UDyHOLWEg4gFhTHVkB1XQAWleTTzuzYwBUcgUTB+gMAJT1i9Jn/AEZAKRLtxLHR8oT5ugAFLcnACxe8GecdsJxHQLAJRT7B16ajWaurvXxyrPP4aLz/8MGltwVX0k0iBaXhIFdEYCaRgZASE6gbwkAuo1r/mUBIBygYwC0pwNYP8BWo8bEf5rxhOzbJ0+o0pzf2+YDiA7QEQD4IahW3jpyROtXB5DNCdTwppMJww9szB5COMn8XCiyXhNFlix4D6euIgDI8TPr1dfRRP191MNWbsH3R+7KCRQuK042tjRRR9ukqDIAEg6gGUQBcwCzVWUqY4gLS6q04cvWN6gjKJkPU0VNxajEAdqIgLQfoD0OMOmkU+ENGzU2njPj8U4DYOpVlBBCwaC2DRqY6E4On3IAzWNXAHCSo7XzTXw7kwoltXJ0PQFA3njrFDiLP5rPACAlUGW3aOlp2W/NuShizx8BYAWr5OqaLKNL4GPwRhtyDZ32DagGgCRTSIDAvZCoUtqKgDQAJEs8SRlTc5A8gtmXKwL0N811TObjHwcA9gQO3/MH8cvTHrETmR1UpYwg9gR2AABrkxsrgIlpYgAps89wgKRc26Q/a3Inebr8kDmAcAaZ+KWf/BmnTJls8+ctCCoAgJWwKGIP4fOvzMGKMB35pPHU1gBP3Hc/brzhegQ500fX2eLW1TFcscLEJQDUNODeJ2ZyAorkEEoDDU0Z05xB6w9gnSM92xUBoOagLgieL6MXVeQAaSWwWkoY+T+OOfF0eFvvPiZ+dfpjKTnoDiurA3SGAyTaPSkp2oLFjQEkjg1WbihXP1OkmbiCKwNgyQLiAMcj5xQ1KgfgvHuj1CkHICIJAF7DslJiZythiRbUUCkXtmKPHUagtt7kR5iBZAHA+oIBlgLgnsdnsNnwTQCQRAKzVc9CDasEMtIlg/nbiAACwKSpp8HbZo9x8aszfld1+ydNCVMrgAHQuC7CIIkCurIzqflLHEBq6iUpYG1FQFUAGICQA4b1ByPzFn/0Dk49YSr3w3Ptdy5YzgAgzQFex9JWp/LIJmUaUPhAfVTGsMED0WuNNeEbbqAcRgmv7J8mkgCQq+uGux55knVEBgCZgqFvdz93zUA6VxNFVBRlS9usCMhyAJskWk0JXEUOQDrAn55+pFMAoIk84cqffSsAaIk46QCsC5jgRiUA0KqwKWW0a7jZRJrEy9d/f7sqAJRISnglFnGAZ19+DctLnObr6A58RpLahhB5H7jrxp/gt7/5re0lqCBw9QoFQKG+O3758LQ2ALDZwhoGNtnE/wwAtJ8TaETA0JGj47nPTLeux6wO4HIABsBVt3BaeJQrpVymOjnVHEGVdIDOAoAcQeTdVwDQvRYveBdnHD8VnumE4eoA7QHgudmvYxn5azPFpHJ+IhqoDUhN4GGnrYZgjS7dLFh0fojrcMNKwwHydd1wJ3MAKn0hHcBL+QHacoCsQypRCNIc1YzL8QR21hHUEQA4I6gjALTRAUxdQCUA0OTYBA7HFczmG9W3tWMGtscBKgHg6wXv4MzjT2wXALYez6RxkRXw7OzXsTwDAAseJwgTUwOmwMOwTTfEOmv1bZN7kBUBCQf43wOASVNP7xgAWU/gCVfehrjrupIV7FFgRIMssjYsIU07M+sHMEoe6d8pT6DNhDFmnwY7nBIvuo9yEDUzFy94B6edcGJFHSDrAVStvaWlhQGwwhEB7ooWIEjbusArc8oXhY9dpVaP12umRMDvpoOaN/MmTavIAZQDuaKI51PnZxU4gFYGEQegjKA1THaTy91p3MedfBa8LXfbM3796aeqioAsAI6/4lZ43ftzXUDM9bAZrVW1+W8BANZ6Nf2ZM4/bAmDpx2QFiBKYrOC2fgCXC7gAkH000lnFzHq9GPUoYufh26NrQw0nhigA3Am0XkDTrdsvNOKex56xAOAYQCQ9fzUWwFzDhAc77Qf4BgB449knUVNTw7pLNQCQDnIMcYBt9hjLZmA2KKIPmxUBnQaAmVytc9dJ1HTwdBVwmuB8rImCBaRWkw6gIjOmSL+HJeQKnnp8pwFAQCAR8NzLc7GMk0CSlxDeA7V8aGlehH1H74UgLz1/KxFfz2QnEBWZUH1jTRfc+fAM0QnYFexGA5OgEPkJ6NUeAFxg2i5lmk7n6CqV6wIoKVSqg2n8Q4YM5a5l2ReNmTnAtnuOi2c/+UinATDl8lvg91hfKoPaEwE28SPd+EABoF3J1UTMZsGqpzRnOAB1FmHWHlOnDmDJx3/G6VNP4GZMLouu5Al0zcCZf/wTVoRp05Empy4IscdOO3HhBXUIdaOVWTGhEVCOAhoAEAe465GZjgig/AInH0AjkyYcrABoG41MWydq/6uI0KRQzg7qoDCEOMDgwVtUBABx7klTz4C37V77xi//4XdVW8R0hgO4iNUHSrJ50yVh6aRQp/jRNm0yxDHRQmoYKVFDyQkMTEYNAeDsE05AbCqOqkUDbQIGdTUrlfD0i3OwrCi1YuSAImWva97HyG2Ho66OevWlRUobTuGEmV0O4OUbBAAedf+kglEBrFspJCs/nS5eSQdJRFpSaamOIi0QTQBg6ixM84hseXhVAMTAxBNPgTf8+/vEs6c93GkAuBygkg6gg/82AOBraFMkBwD0PXEEIvbST97H6ZMnWysgC4CsIkjEIgA89/JrWNxCYoXy6iLki00YM2oUcjXSGsZ2Gk1ccxYD1mvodCDXHTvioE50AGq9LzXPqRhAUjbethjVBYEbDGI/iPmxswCglDAVAe1xAGJIkwgA23xvXPzKk490CgA0qVOuuBle1404FsCTnC0NMzWB7BH0idGZ/vZOF6wkK9hxCeuK1x7B5ond1DDRBYQjLPv7ezjz+OOskpZV6DRsq02s1Gf/3AsvY2nRR00eOHz/sfj6y0WS3avJKapUdpBBlLUC4nw97n70aVCNICeEaF2A4RiaCURRQD23Lft3excL5W3UwkZHk7yAajqAVge3pwTSNE+ipNBho8fGcygWUCVJ0RUBqwIA7uVjAFCtMCQVFLKpUBnXrjZuMKlh2mL26w/fwrmnkB8gXSRqV5OWihnlMYkJAI2NjVi6+GvkqITapJRxVjgPyFgH7QDADQYRwFgUBHW49/FnLABsMqjtCGKigtoo0okEua5slfXZpFDNClalkN6r6QCdKQ2jYR099WQJBs2Z+XjVrWGzOsAJV90KdJEOIWkOkPQHcB9IawOTmnYhWBt/gNMsSiYhUyGksplarwURPn33NVx89hnpPnCuZp+t/rVWRFKl665At5k03z+z0VWl1DCR8SbS6NfgvmnP22RQjgqSCWiUPgvATGWQFokqi9f3Nn0SOskBSAcgANQUxAysnhYeY/zkk40ZOOOxbwQAnm9bF5BuEKGaeRYAgWkEWQ0ASWGJKn/p+gBq2Ej3XL97DfYasSMDoJIJm00Czcb329hFdjs8+SV7vvb20e9dEUAcYPuRozH53EvIhST1ABEVk0r/eJdjqPavloRbJey6otO9kZJoIHUq4ZqAKuXhLgDazQhCjAlTSAfYY7/41Rm/4Xq+Sq+sI2jKFT+F320TaYzAmnnmLFOMmQDA6AKa82fCw5rkSdEeqgxKVrxc0HYD0/54poGkVhB1q/Mxbsct0dhQx42snW7wFZ8j+2VmI5NE0ctyjmzQ3hxpARCHaGqhtjSz0VSuMQRXbV96BmmnUDpVS8OS22jBbOJRFasnHZsQrV88g1wM7IgA3p+IAmWmQYRygPYAQI6gowkA2+55QDz7qQc6DYDjr7wRXteNpTEC9+TLzpgASQFgdQHb+dJ0u9bmT5TaxMRVlp8AQD1zYv4lMpq/90OsUQD23HFbrNmQgxck7c6VOJWQQL+RTpIFABGJrQDT/ZzuoauUruNeU5+N/ADLm1vw9CuvY1GRgj90ICWCJOFetziUFVHVAezgVIdJzE/6SRpg8p1t5nQ1ALj9ATrLAYijsQgYPnr/+JXpv+40ACgaiC5iBaREQJXuYGzW+KHZjNLdMEITRLRSyEyA7Q6W6QWsfgJrp9MWKhHq8wUUolbUF2SzS8oapi1s1MGkaTeumGDCmibRtkGEIbIyNCKARAUkq48qfqkCV2U55f63kvIX1aC55KR88YpPmj8lOoCcaxtGVG0QkV4IiUPKcADTDYyepzNKYNXKIAATprASuG88Z+ZvVwEAiRLYWQBwUie1tTIpXbqCxBw03ML45lX2uc2hmKNUAAABizqW8jaoKipC2sqONlDQ/gVpn7/rqNKVTd8pYbRvgP4WqYzjlZ24kElsUT9g+p3aw6rSaFO/Mtq/dQwZhGmvoJQiyvdKd0ZJ/AKyN4G2g3MB4IoA8gO88ZwogR31B2AADB25T/z6s+QJTLcsUQ7VJhjkigA9yBUFDiew1gCnfCVigVm6mSDerp57cqe3elEPoGYpJ/4AU0xiGyqa3UNNi1j5Wrr+UdkUEYhbrVMPH9pbh/enojayRDaqH0zag9A5ZdVVaGMqYsNU/EkdjImLUbFHILt5ISoh5HIyCfrQS1i/ERdWbAjFbfBIgVGVA5g+BbZVrMmR1B5BFThAFgCuH6A9DnB0ZwDQxhX8DQFgGx3oBgh26xhpfmz7AhoZaV3AyvKtP0AmSDvanH7cUfjlnXdihZZvI4d6r4zN1+6Ntxd+jaHr9cXc9//Ou3cN6tVFAjdBgO133B4P/mEalrYIa24IPAzo3R1h1CJtYFCDoJDHnz/9Gi2hh7r/acF6wOhdMP/tNzgR5JobbsDYw4/BijKJE3L9iotXAaC7k6pH0CqNJjqY1v6V6K5Fo0Wy0gbWNokyKXJul7BvCoDxx50Ib8iIsfGbsx61ffGzilObfADHD5ASAYkeTRv+Jp/MikqUwnSrWGZl3C/QdPKgtuyk8OkKV8+gBpc0qGLMv54NMUYOHYInXnnb1DTksF63GI88+hh22mschvXugpcWrEChJsKQNbrg3S+X8dioK/mEA8bh2ptuREPvDVBXG2LSPvvjlgcfETlvdkIheR4UfAzt2x1v/G0hwnwNK2i962MM6NsPL77/GUrMddKNIpNVn5b9bsNIVjRtTmICAnM1W0XNuRDaJbQCAHgOK5SGte8HoD6BJ8IbvPOYeN4L5AfonAhwHUGrCgCeeN0ezikhcwFADRi0CTJPhI0FCKYUWnbDxVpguzUaMf+rZVhWClDIR9i6T3fM/2Ix78uzRa9GvPLJSuTyZQzt2RVvfb5c2LUfoKEOGLjmGnjzs4XI+yGO3e9AXHf/Q7IdrgEehXXfe3kmdhq5BxYW87bLOQoRDhuxAx57ehaWlAtJtzGtATTxApX9qmO06RNoKqZYcLn7HNi9FdPdwFlnaqdPYKfLwyPgyCknwtt8570ZAG33mZYJ75AD2LXuWAWZjqGuZzDbN1B742rLWDdPQKS5YYUZIIjThERHgEXvvYQthg3HpytjdK/PYZMedXjzy1bU5iMM7tWAlz9pQo3vYWivGsz9solHnItzbEVst2E/vPzhAjTkAxy5z1544HHK66OMIJn4Rc0RhvbpgvkLl2BlmXYBNWYu6F4x3n/rTXRZfyvbSlZXvrX3DcdKWsuqjpDpc5jJbtadQtQcdDkA2fvVrYDOVQaRQnvk5FPhbT5iTDxv1qNVAdDWFZwxAxUAnunUwd6vdMvYSgCQLJ+kh4/tGWwyJnQ1uNvNCSA0i8e0gPU9dPFbscXaa+LVT5ah9MUH6L/BJvhsRYDGWmDIWnV48aPlqM0FGNanDq99vpy1eQXArptvwADIRSFOGn8Ybv7V/fxEpOXTdvZftXoY2q8b/vxlE5YXiTOo1RKjId+Kd15/Hb0H7ciKZsL2HXtfewxnOoZmdQB3Y0uZF1MmbvYP6iwAanIx5lJGUEeuYMQ44thTSAfYL35j1oOdtgKoWbTtFp5yBFXuFayszeLEegTNRhA2eVQqh7R1jFUKTe9d2+fPmknqIJGdM3deuzveWPQ1hvTpgTc+Xo6W0OddM7bu14AX/rYUdfkctlqrDnM+XSp1A34ONTkfg3vX492vWpBDiCkH74+r73uQh0rrU3z9OVx3zlRcc+st+HRpAFJOWUfIAQ/dehWmnnwG/t5SsB0nq/UGtlvrcN8A2VfAnRu7s6rTgYStJZvvIM01KrWKzTqC5lGvYNp+t51YALW/P3TiWasOgBOvoW7hpl18JwDg2rkuCHTfAEU+TbgWjjABnPJx0QV05YuoUbOQfADkZxjQNcCfZj+PSy+9Gj99YBq7YAkAw/o1CAcIAmzdtx6vfLyYCVCTzyPf8hXO/9FFOOfaW9GtJsTUww/CFXc9IACxO5j76NIYYLPuXfHBkmYsXlFk0VCPFgxdvz9e+mgRiiVSKaVQplJvYA0a8e+qXGZkf+V0ehJxiVOrmh+gEgCo2FU3jKjUI8gCYOCoA+P5M3/r7C5shXpFHeCkK3+GUrc+tjVLUt9epVt4hcQKYv9J+zOzkrVDKG26yH66xC+ggKB3Wzat5qHRXWsDYLv1u+PtT5dgcTPvtIN8AdhmvV549cOPUairwdDejbxSPdpg2svjiekzsfamw9AahejW4GNQzy7sRBJvH63yAPP+thCtQQ5dcsCgPr3Rt19vLPrqKwReHq98+CmWlZNiVBcAagZSjSCbgA7h3Rl2xSOveNs23+xmpnkSHAOQtjrt6QDkCp77zDTU1ta2CwByWux73I+oT+DB8bvUKrZtsSqPs01dwOW3IVqDtiEzzg97XvsAUBcqsz3SYrUDpiJceweb1Z51DKku4O4foLECmdDAbKxEqeoUHCHXLW3WSCMNEJPXkJo35Mj5QwEo2vVMduPmOINPQqCEyJOaQPXs0c5IHHnnKiXPNLXyUCwS5SPEpmWuyn87L2oFVAGALpx0PYRE+VQHcPMocrptDDmyTNyuUrt4jgU895QVAdWKQxMAjD4yfpe6hWv7qTQDcKwAofTUy25C2GNtuxeO7s4lWxnRxKk5qbWDNqnJBI6kwYMXUacuyZxNs3yj4Rq3a1b22+3UMlvLJMoh6Rb0yfjUreggIutmcxrAShwvuuopnz8BgFNkavdWFQ5RrfevbSNvVhR/NuXhdN0wU5Gkyq46vmizKyW8OHtMOFyzpE0YWKKBpCOY+eJMatrEOsJrzz+NhkKedYCNN94Yffv2zVCVPpbwg2Np17Axx8ZvP/bz7D4P9oQ5c+bgggsuYO8ZPfhuR5yOAdvtzCyNXpFvZJ+zc4acbMMqfB5tzaZcgINDZuUnvm/dLEnatkqvzWT7eFqXHLHTHUUy+wqol14TT/QB3DAxXTfdkkWijLp6+Xe1/zMckYUCo1UATeVfBrp6AWkRp25fbTljPlPvIHMjO7fW0jGBHXXoiHmrpp4RBXbjSGpuQYuHdg2j2ZCFRBty0LyStfP6rOdQm5P5XH/99bHBBhu0BUAMjD3uInjb7Ht6PPuha20r1+yRBIDzzz/fAmDUhNMwYMvd2IMlrdKTOLY7QbIdcqIUWS03lXVbtjLPEkxz88wK12ZkugeQA6t0IoimctnNpdJFH3r9rFLKlUBOzN8t2mRA2N+0vZwx94znzz2XV7i5kQJErQIbLMpkHSf9E4XgpOipCFCdgJU8FQ2+bK1LK104hdmY0/xem/OYA9QWpDvb+uv1rwgAGucPJl4Kb7uDL4xn/foiVN4wBiAAnHfeecxO6GFHH3Mm+g/eJQWANGhkotwNJemzym417/g7kssOgOg77fhh7WBHOczwldRKsrl8amam9gcwewQaYqZBkC4SIQ+jRP0Mwe1dKm/44OYJuADg8jCzY4grMlzG4vpBJN27LQDcLWP5eNMaJqd+FNJhgsAupLq8hz8RAPICgP7rrocBAwa04QBFxBg38Tp4Ox55afzQreeiT2NlV7ACQLpoAt+f8h/ot+kOzJsZ5XYLGJ1IUgbzberfdQQ2XdwS3nQDM44klfEJkeS6HDFkhU7zBpQBJwkpohQmx7uErmaOarTRynQnd1DH7HI23T2Mwr7C4eT+YaYhRaL9y7wlO4bKVdUDqptnazpLzpSiuXsGqy5AeyVQY0wOo9s8i3QvZgbAc09z1jNzgCoigFTbccdcC2+Xo66Jz58yBqN3GNQGJfTF3Llzcc4551gRMO7ki9BrwDYdAsDdQNG9sAsAngh16YLs66QBhMpHGw4m7ZcAoA4Uc1EFhFwrDQD7nfUetn1EzUSyu4GpbDcOm+QMY86Z+2vcX0GgANDPCgDyirp+AL2eLZo1K5kAII6fRAQw4Q1r502iDQC4yjoDAO3GXpcDXpv1DAo5AcCGG26I9dZbr82DL24BDjr+Bni7Trgh3mnTbrjsR+MrAmDevHk4++yzeXD0IPuccjF69h/OKVm8ebSacZohpMoOrcRYTKqsnGRlx+b/60o2GrfjKUxAIDEBBoBVAjVzxnAEwwjcJlWVAOB24nAf2GbqGAKrkpsQMq0DUJ4Bp4Nrgkcm3dtaA3aDyXTqnMQaSNmTzbNVBFgAsLbvIWf8/uIK1nZ7ZLRR80wSmeY6hoHXGxFQMErgxhtvinXXXdc+qo7ihXcX4tIr7oU3csItca74Oabfe1HF7Np33nkHZ555Jv9GD7z/6Zeh+zrDUgCgq5NzQqit3bcSACgIFET0Wc0bm/9uRAL37maHiLECDCCoqzg3iDC30U2nbLDIsQ6qrXwBVFrpU9GQ5QBZAOhnfRYCAAHbBYACnWW/40p2v9exdQYApOWTK1g9fZzyZpJCaX54y1izfTw9FwGkNgfMeZZEgCwo2j7e3TxaAXDdvc/jD0+9A2+XCbfEcfNCPH3/BbaRs7sy5s//M04//WSWPQSAg865CvVrDeGEULICdAJp73p34tUM1G3WEv+AXN32ErJl2vK93STJsm2zT4AZVBIjMNchjdjNKjbHZXvuZHUKfUYivP7GhHNTtgz7dn33SkwujM8ez9+pzE+UQLqX6gDEweg8NttMZhQrf7yNvSiBRHDaGTyr/TOn0NgA1R+bHUNFdAjBaffwV56ZgRojAjYZOAhr9+lnrXIFwJiJN2AZauCNOOq2uFAu45F7p4I2Usl6bt97732cdtpJNuHhwLOvQkOfBAAJ0XUbdCWkkZlGm84CwHr6HGWwIwAIy1Ql0CiPBgB2N3GL3nSatX7t1t4pYV3ORJVG3MiRo4FCTF7pTvazrHyT7+8ARsChMj8NALewhDmgAwCx40uG8AIA8ktyo02zsikrSM0/nQcBkIoGAURdHgwAsgKYA1QBwI6H/hR5KoYdcdQvYkLWkQcOxsRx27jVaPzw77wzH2eeeaqZiAAHnHUVGvtuzq5VmcBs0qWdal2zcq6z5FxlTVKdpNScAeAEfVwdQIGmQSTNKSxYMzETLHIaQKStgbQsThxDidnHz2VWshLebg9njB27CbRt+iSEV8CQ7kCv0PEx0O/E+umlm0NTOTptA58zHj3y7NGGGqT8KQcgczlg7Z9kvvEMqv3PzbbIHyDfU3e7F5+ebv0AmwzcDOv0STyBNMstEbD3pDsk6KUAWL5yAWb/9j/twJReb775Fs46izZdphUd4MCzr7YA4AdUj51lHSoW0o4gZS3ECWgikpQvVetNEmYm6peIGJ04k0dgHEUKAF5hvBuoiRZmOoBkrQpXBMjfyrHEpa3BHAWAbhBp07yNnU+ppUx4Iy70upUAIHdRX79po0fZh0Gy8wfZ+QULANlriaqYCTACAJMk6ngKlSOwDhAAs5+byWYgfb/xpoOwbl+K3ciLZufOh1/CnY98IHTY5Ye3x3RgixfhxTsnJdWo5gSyAs466ywrJ/c7+TLUbbi1rGmzTYuuTnlAU1DhuFSZiE6eoCz1JBuXiaOZP2ZJkl+br+f68p1ECRqopJWbxBByiIhbwow18Qfw7cz1qqW+adVu282hE1Yv5py4tLkLSBShrGFj6xqXidNMYdpLWF4mxpDpg5DzApb3pBORu5wIXKDPhgMQc6TfSdsrmC7rdDXxGDqdVUztYL0fY/asGSgYf8JmWwzFWj3XTAAQhdjh4IvgN24K3vWVAECrm2ru9h03BCcfMNweTH/893//NyZTHb5Z4T0GbIYDfnQdZUUDntMqzjynuDwjxMZxRIEQ5h5UWGFMH+UA9E55/fyuZiGtbDb2Da+1nTolcKTKI71L+5ikrTxbCXYrF2Mm8tSTS1f850lPI5lAYdsEP+PpM8UfqrxSmjhxA2L5EqFQTyFdKebu4EJxjY6qjqKJH4bj2S1x5Dh1ANF4af9Eep7aIKWAXCcAAAreSURBVAeK+uUYEOLhYwDQ3el7LrZXEWDe1WXui3Xz0YfvYekXC+AHeZ734TvsiIYaan4jr1YAu4+/DeW4G9PP2+XIu2IKIlCSZOvKj/HyA2fDp5QuG7uIMWrUKNtNm1jtshKxI9nGjfLqhQVJFIvDJdwhq4jm5mYM3WY49hm3Py74z/9EQyGHsiqFGV7DbgPSqg0BRDv3kTP5+ap0cfNo02lTXLaJc5WBpNYop6bRWDRRQ4ltAMmWAxHOKHpKeNPSTieME04yCqAAmbR4AZdwOGNe8nUYEXwMZRUryKUHsdQl1MYRh2y32HorWRhBgHyhwCFstgZyktHTs1cfHkrel/nVRaSeQjKPrXjzgIacLzoBxw9qsMsuu5jxyeYXN/3qSdw77Sur7FsAFEkOlT08cOt4rFNvepIZDXibbbbhzQeZ4MYZEXpmowWfJlSQSiuMnp06co4bMxYbbrQR/LCZMb6y1IrbbrrB5AsS3dI++JJRmvK2iZJRqMyeQDmTK1gye/rQcVkHk2uuMQnMZlLCgY2SqUWwmc+6kBVAruKYYomZDwR2vrz6K5Th2wzfJPlTCGU2pyp4GD58OHMxAkTOD5DL5TnplD/namS3r8Bszu10KxPzz4g+4nlu6r1u0UMWQW0XbL/99oZDiSk6/JDLkC8MYNnK4xlx1D2xu/dfqfgJXrzvbPgmMULFwJTjj5EJ94S16F5AdCHW4k24duKk8YxsdXXyGuGlEuDaa6+1LJrSv3ni2ptdw1orEYO5RYXKXSa6cVrJDeQ+7rFZxZJ/NyLKDkdFT9poEFA5Lue0hSF7F7mASBGHzLacmHmbbDyQmzdRgQsTlAI6zj8ivtulTFe/ywUSxTYBIHNj3vLWx+67757MbhTj9b8uwgmXPgpEdXaMbQBArPaRn49HL2czcWK1F150HkcG41DsTbKX6Z1a73ft2hXjx4srmbLjRFlKlB8FAOWoXXPV1RLXNzJRgyvVcKAtZvR319TKfudewxLc0xSv9B2qEa4aAPR6IvYSK8YltgBaREr2+kqsXN5HfX09thw6TI5T84/lvS9sv2dPTunS6yhRLft3REEWBEz8KMJuI0fbhUnXIWV1xKEXIypsKP2XDOfwdhx/D+sAdpUQgJs+xbO/OhN5r2BlBV104cKFOGXqFCxatAg1DY0YPXo0hm27Azdf8qMSs37aj9edILsi44D1iPnv/hlPTXuc9+Gj83RCdeKpFs8lLLFmXQk8RhtmlePczuDZlc0OHVspnNEVHN+9SyzexFTZeQUO40443V+tHrdRBXNEs8mVjlejqXT+mL334Z6FNFd6vSAnnLSmtl4yeIxMSla8UWqpxFEVbAM0ugYBh97X7NUXQwYPcdDOfcow9y+fYurFMxCS989srMHvO024NyUCqG1aLmzA1ReMxnYD1qjIoy0bzCzb9HyleWfSoUNs3qpbpmRZbkZGZDt3VOMcHX2f6g/g3LNDkZR9ZnVjVBl3xfFyvmJlsZQ2GtM3U+XS/TYrHjPDYLulFQFGHnYJ4vxGINHLHEWtpZ2OvjtzjonKtazAM/ceh4KphOloQlf//q81AxZIcYTT/9+v8NI7RXhxvWm7S2zO6CqVAMCsNQ9s178rrjt335Q8+9d6zNWjqTYDCoC/fLkcR//oAZQjaXrNQoubXxjrhQAgcjjxzDEA/Bg1ZeCOG47EBt3oco5W2M68t8fC2iNXW9YlR2e/t7pClhVnPneWlXd2vNXGkX2mzt73O4cubWcLD3scdQWioH8iyq15agCw88R7zLNltGXuxuUhal2JGfcej4ZKBR4VnqKzE5o9dTUA/rGQKMcR9pp0NZqi9YwprCtfldwKAOCVb12y6tnyUV7yN7zw4IXso66WP97ZFfKPfcx/n6utCmchk++MK+7EK+9TNbNxTRu/Dju+ncXsuRzABQBVjrBo4B0qffTqUsKvrp+EQhVOsBoA3y0YOwsAIv5P7n0Gj8z8CPAK3AybXp4DAPlsOMCuE+5hHYAaHomnTwPe6mnTYIuP9dZsxR1XHSvRqTZp09/tBKy+usxAdSBQ0kqEB6bPw60PzENk8jE9AwCt3LI6lAJg5NH38uIlAIjSZZRB65AxLIRy8pBDvx4l3HX10TZ9bPXK/+dCsxoAaOXf8dg83PXoG4i5YXe6RM8FQBsRIGZB8lJOkPKfGx84ZafURYvx8O1noI7PSg9pNSC+G0BUXfm0RxF8nHDZnXjvv2tQNgSwK19D3hW6tzFHIR1AfPXpPvacBed2tLZNnCjkSnVhS/Dw7adgzUyH2dUA+OcCoCkCxkz6KSK/F/Nuqaxgni4cXT9mGnlaUTBikogAdfq7UbZUtM22UTdhxIgaF7Zi223WxY+P34stBEqi0AH8XwNCdgV+189XXdZzHJxX/cN/nI/b7noWLXH3lMdElHcj0m0r1GSlcgzAIMAjALgiQAGQDbXaHjaa+EjlyZHELPzyYlx+3gRsu0k3zloRXeL/1utfBQCUZLqw6OGw469E0e8HjxNa0rPtAkCpoBnOstYFABzT2dkAwB7oxNkrxdDJSuATM4WWeYruFT/DtT+eiq026iEtYJkpcRqkRcO/GjA6Mq8669iq5sjqaBl06OmkBc+7kMX4bHmIyWffjKZib5QzOZWWfu08kBvS1iBVRQDwCjZA0HCt1RwdUeBekOhMrVTDOEQcLsWwzdfDWSfsi16NPmo0ieJfkDP8qwKADG1qV7M89nD7r5/Bo4+9Bq9LP8TckjxddZUKqdv99dqHngOAX3E+gCYyVNUBsteznCCdjaJAyVFPHJTQGq5El4YW/GDvHbH3iK3Rp2ueuQN3A6X4Aw048hBRShqJFN6HQHgGPWrlXQw6Wlff/nerO6ms7OCSbVeyyUSq0oCTk5SJOfIKpyxjSqLx8fHSVsx88Q08Oe1VfLHEg1/TAES1NqXNvU82FNzeEFMgcQ70RhxzvySEmFSmShk3FS/sAEDlitUsOd5MwaOQe+jkY8k69klMUC5tuQUt/hLUBj76r9cXGw/oi4EbrYsN1++DtXo2olteOoJSHrytqDFZud+etJ27wjcFAFcFcqdZkbOattpCenkELFrcgo/+thDz/voB5r//ET7825doavGQjxrh19RyEg6lp1HuozSsFxmv/pmsK7czT9MeULwRx/xazEDb5kwwZlO11Hww2bVtEhyqZGhQd24xL8WlrK1YtCRMk0riyGQQadEmH09brFKDpwjliNtwIUYRVPnapWs9uvdoRLeGenTtWo8e3QtYs3sPdOnSiMYuNehaV4ee3bqhS0MN8lQjb3vLylRJYYawGEoqVkIzy42AYgkIyzFaiiU0N7dgRVMTWlsjNBVb0LRyJVasKGFJ03K0rChz1vNXK5qxePFiLGtqRstKD8XWGGEYIQK1j42RNztk+j7tJkL5f1p4YoIzqq2b5NfQpjWnK6uJm9oHUK7kZi5pvwKb0VRZe8luDubtcuxvzJHa1Cld9Gi1R7M9XHYzpWrBIa0F1CJNt9WLy464Uz+7oE2Wq0nO1LJz2oePOUxU5g0ZpaMXnUOEpBq+Gv6ON7H2qfOXD48JQFuEU+NJgwDlWGZHEOV0tAOqWEG6Taz4zXVDSmKMUURp8EIpn/YDJnFlAEuta+hakqcrG1HyK5Ln4ms5OpCyNE09s+aaAQCl58vxSU8lmQDtxZQ08tCVLccnCzeNlHT2tQsAOv//Aw4A3DGXD92kAAAAAElFTkSuQmCC`;

        var sImgBase64 = `
        data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5QMHAjokSgSMPgAACPxJREFUWMNVl2uMVdUVx39r733uHWZggAEFRmF4CSqKI/IoWLUKFiqWtLGtbdU2mpgmTRNTTT9om0jbpPWLNunbREyjrbQkTYyiFEHEKkVEQUeFyvB+XMpzHs7j3nv23qsfzrn3jjfZuWefx17//V//9dgCYO8/DCrjY9WuRP33NYRVBIUYINSGBx+z66gQquA9pCF/L82uQ4A0zec+eyfkI4bXJY1/E8NmhFIcfATh3hKoXoXwBKTLibRkRmI2QmwACKFhJI6Y+zQ3VM2MpzEHGBoAfJqvIRUob0eHHsY27TO2qdKG5dcYXYPYFsSAWDAGRMjmAkYAA5Jk91BQBc3BBgUvEGz2DEDyMXIioUiUlcTCkyYU212MLatI4oqMcgUJmfGg+UI+MxwsuF4Ydxxa98Pk/TD6PCIBHW6GE5fDqVlwYQKcHdcwbExuuHYjB6f2Ng1DqxyWH4C0oJLvigxEjlgAkUHi7H/B7C3QsQdxlfxJxoKIQReCaoTTlyFdy+C9BejRiTUUOWs5GBTQgqp+R3jwnCJku4/a8G8VKFdYeOn73LLoWTZMeZvTaZHmc7MZc7yT1rMdSDSkGknVMthykcHpu6lOPoxvOwPnJ8HGr8CuThgwmShrWoi+LnBX943GbESFaoQyLJ+2nae++ivax5cY3L2ErR8vp+XCNDQ1xGgIUVBVJMAobafYPY9qcw/9V++k0rkNvfd5zJSLxBeXQW9CxvLnhIFwz2lFJPN5LVyqcOusrfxpzeM4W+HZf3+D7R8vZqA6Co/Hh0hQqWsvCxrNSIyGqhrSyQep3PpP4mUH4LXb0Q23Qr/Nd+/rUWSoRqjUwilT8vz2vfxm9RMUbIWnXv0eG/cso6dsqYQq1TSSeqHqlapX0jwCKyEb5RgJIWKOz8a+9AAcn4Ou2A5L9+YaUBo/xeQJIhsaKcggDy17jqnjj/P0m3fxxv4bGPKesofhVBmuKuUqVKtQSYVyCsNeMwBeqHhDOcCwKv7cZNh0H/SNg69tho5S5gbNQxhw+NAID4VV17zG6iu3sKVrMRv3foEowrRJbYxvbcq0q4IqRFUCgkYlqhA1exbyfWlULvR6DpUShnfcgd75HLJ8F3p8JVQkJ0JxxFAnpCkZZM3cbfhqwob3l9M7nDBjShM/vreTzrkTGwFUF5A0CNWRxGYy23ekn0f/uJ+9H90C1+2EZXth82I4NA5EIYIjZJgRmDn+FF+etZMPSh3sOTINDZH+AU9Xdw8+guTJRHPjRkBVMZJHsApRBVRQhAMnBugd8pCOgf/eiN6+DmZ0w5Hrsw+oAZBsyUuaS7QUB3inex6DlQKqysnzZX67YT/O2cysgDXgDAi58lVJo5AGSwgWVUMURzUq5RCQQgKl+SgWFhyE7dfnmRYcGnL6hHmT9hFVea97JuWcmKaiY9qkMYxtLSICzigm5zuo5BlciDEbqkLEELH8r89zsn8YHx3IeHSoDSZ+BibPuEQcwWf+dI6OthJqlPOV0aQ+2+2cjnH84sHrmXHZ6Dwt5yL+nMOzhDQywajCjk96eHz9UY73eay0MuZcO1TO0i+KoqARR0yzL6KSkF37kFFqgJ7+yCeH+xmqBIxk/q5ZrgFRDCqmUQSNoMZy5FyFVBJoKjLRK2v/YJHzgR95TzWXdD0MNcCJi5dCFFokEqMBge5TQzz+7H5GNRkSGzEomcQiQQ3BONQkiM0KjXEG11RECwXKKfSHhMKoUYwd7GXx8dOcHjBE14gnR4g5p8Le03MQgfkzD7Lz5BXghJbmhKmTW2huTnACQswrYNYjqEnAWYwxWCtQSHCJI4rjVB+U+8HYIq3lwMTyOd6jgxgMqM9FGHMRxsj5wcvxvombrtrHM7tWo9Zx84JJPHbPXCaMLeS5qpEFEEWQDIxkYZrp2RAibOoa5Pdv9HMhVW648AEmeF6XWUSNQMhF6H3uUs/RY+PYcnARi6d9xLUzSnSdmkNfWeguDdEzmAKCSiZOEYOxeWUzWb2XEU2Hj3BmUKFgadEqKw9tZihaPnTTs0KkNRf4XIQiDPQV2NR1M3fM/Q/3LdjGT3uu4N2jAxx45lMKBYMtOmxiwToKRYctFFBxYBxYhxiDkUyEUYThVKmK487Dm1lycg/r5UoOxbZMcMScgRjy2AJ0mJfevYG7F3by3UVbeau0iFe6b6InjbgYEAyJLeBcEQkOyhY1BjEmax2NYozkhCjiLFMHT3Lf7qc5WxH+ahdSjVrfPQQsl9y9NmtGsgYzHRKODUxn5TXvsGTqR+y7OJ+zcSrNrZbRLQ7T3ErS3IxNEoyzuMTgnCFJDC6xJIngEoctOKZUzvHoxp9x9YG3eNJ8kY3MG7H77N8y4dtr8/JW7y5OnrmEM8PtfH3BdpZO6+JUuYNTOhdbHIVLLNYmGGcy5bsMgE0szlmss9iiYcZnJ/jJi4+x5JMt/MNcw+/MzZSDyZtcnwPwWMZ+a22tF6ifBXyZT0+0Uxqawerr3mTN7C00faCUyjMZGj0OcQkkFps4bOIwSYItWGxiGM8gq7te5qGXf8mc7h383VzLWm6nNxZyw3EEAynC9PUjEqvW6zQqmMRxy9KDPDbzL1z7x5Mck+nsmn0Tb89fQ2/LOEJSwFqDiZ7WOMSNx3bR2fUqcw+/z+lqwtOFpaz3nfRFB1rNU7UH0joTQsdzW4isyAI5juhY8vRoHVOSfr45sJu70h3M6T+C2gKptQw3t4FxNJX7KFaG0ODpkTG8Yufygi7gwzgRDQqa1n3ecEEVCFtFpj5/vwb9M4RCo7HIO+RaVYlgEkO7nuWq4nlWhMN09B5hbKWPGCOVZBSnihPZETv4sNLGQZ1A6iUzrPFzohtBf9WY8EMxU9e1x2pYh5pVjUNDqB868j4sFylgAhLBUEEIoD5r06ISqybrLckNa2isVd9dbf3KZmPiAy4OpSWseQT1QuRLoMVaw1gHUT+0RJCIqhJUIQqozc+HvnFWREeE24jUnf1XIG6H8HCMxZIAmCnrAG3XtLxKg95DlNvq58QagBoTtTn5obSW1+u5xI9ou2uU1wDoNmP8CzB6E0gpxp/zf7k3KkqmZpqgAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIxLTAzLTA2VDE4OjU4OjM2KzA4OjAwZhQp5AAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMS0wMy0wNlQxODo1ODozNiswODowMBdJkVgAAAAgdEVYdHNvZnR3YXJlAGh0dHBzOi8vaW1hZ2VtYWdpY2sub3JnvM8dnQAAABh0RVh0VGh1bWI6OkRvY3VtZW50OjpQYWdlcwAxp/+7LwAAABd0RVh0VGh1bWI6OkltYWdlOjpIZWlnaHQANTCQoD5eAAAAFnRFWHRUaHVtYjo6SW1hZ2U6OldpZHRoADUwaA/+0wAAABl0RVh0VGh1bWI6Ok1pbWV0eXBlAGltYWdlL3BuZz+yVk4AAAAXdEVYdFRodW1iOjpNVGltZQAxNjE1MDI4MzE20dcB+AAAABJ0RVh0VGh1bWI6OlNpemUANTI5M0JCTzUp6AAAAEZ0RVh0VGh1bWI6OlVSSQBmaWxlOi8vL2FwcC90bXAvaW1hZ2VsYy9pbWd2aWV3Ml85XzE2MDk5MDM1NDE3MjU3OTE2XzY5X1swXVzV31QAAAAASUVORK5CYII=`;

        var bImgBase64 = `
        data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5QMHAwAkbm84cAAACZ5JREFUWMNNl2tsXVeVx397n3Pvta+vr9+J4zpOnJA0D/DUSem0TVNaUSKSMowGzTAgRqgdTR8fAAkkQEh8YDRCjOADSAgQoEI7M51RpXaoKCptKZ0yISR9JC2krXGaNonjOk7ix43t63vvOXuvxYd97rWPtHXO2Tr7rLX2+q//+m/TuftTxJXTtrbts6OU33ePFIe+lsZ9kToB8SACzoVn78FJmJM0zDsP3oE0n7P55j1ba8SRs8ti3NJ3rV/84XX5n56bc3epGQZz9a6HD7ju3V+XuPtmJe5CBFQzoxIM+8wJt+7de0gzo96BbwSDqQ8O+jUHcCn4FCNpaqkdjc3CVxIGT0bpHd/bmfR/8NvStuEQNt8GBjBgFFBQQ7gUPIAAKZgG2DqYGmiSOUEYKtnQbDQDEoBIxY4idqhgLx2Pbf+ee7W9768hB6JgNTjgbWbYgWYOtC1A57vQ8w50zGLyS4CgjRwsleHyIFzeBJUy1DWLJQtITXjWMCeSu1OkeL9p/+dJXzdlqxJs4Nfl3XusNMjHcySDx5BtL2D6z2HjGtYoRiwKqFHECOoseq0Xc2YM89oYMtMHdbKUZDiRtIWP2Na94b4rCoToRQKQsntBlvnghpPs+8ATPDdwiguNmOL8KO1zW2irbMDWS3hv8HGdemme1f4p6hvO4ToXYG4jeuwW9OQeuFbI/puBU1wLQ3HY3iz6Zr6cp+CrHB59ni/d9ghdnbOkf76BoxM3YRc3YhoFxEeoWJyAFcXiKdgbSIqLVLe+SXXPH9DDT2G6r6K/vRkWirRssZaWmJoLL5Kh3nkKrPLR7b/hax/6MYUo5Yk/fJyJyRvIVTtwAk4VryCiiIRCEY1QacdUixQWBuDSCLUbf4O77RjGpOgzB2ChjdalAeQxiQ8OSUCvEc++oT/xxQP/QcEm/Pz/P86Js3tZTXI4r6SiqJjWhomAV/CqeDGIKuryROevJ18tIwefxN/8CmauhL64P2Bi3WVbBCOhXvsK83x2/HGGyhd57KU7+N3EHiqrltVEWU2ExCn1VFltKPWGUkvDe+oNTgxpc/gYvTQMRz8K1S704CuY4dl10YcrxvkWCKxxHBw9zodGj/Pu/C3M1D7M8HUlVMFnkRsb1qcuzBljqCWesxdXAiAxiBhElO5SG9eVbsQtKO/sfISVW0/B9F2wErUwF+M9TXSUi9e4+/r/w6XC0Pse4GefuBvvBcgcUCWyNpT2OjidmbrGkS88y3zFoVhQSFLDbeOb+M9/u4XEH+Jvj03y4q7fw8ZxWB5oop4Yca2fjXS9x9jGCSZnRphfbCfNz+G9oMCurd2US3nefGeBldUUk+F2oKedXVu7+cyRHXzvv96ikPN4NRTyOXaMlGnLR/z66Awnf9cLu1PYNg3nuyGRLAU+MFAUeXb2n6NUqPLn2Rt45PfnaPx8GsnS9T///hFuGx/ky989zsmJK8SRpZYonz68kx989Va2DJYo5BVrFe8NvV2W+/9uGwAzszVW396M2VVAhy9BbifU4wyE6kE8ESnD5Rmses7NDbCSWOppAFnD0XJERPEe6kkguKnZKvPXGvzDXVsZ29FLI1VEYc/WMl2lHFcWajz85ASF5W5MtQ/6liDnQwZEsAH9QoSns7CCYlhY6SR1llQCsr2sJV0JZSegsbV69NQsr7w5R0+5wEBPO0ZBvPLJQ6P0lAvMVerMLtaxSYHuuV56XY3I+FZ/sUgKmqDiMCgKrXJy3uB9hKppAdV5g2hgMVFrBMupyXkAPv+pvURxzNBAiZ1bugD40eMTLCw1KCXwuV/AVx5VepYlEI8KFhfYT1Kh2ihigI62GqImsJuxGGNYcyEgH2OMGnBOeezZ81RrjoHedro6C+wY6eL28Y28dPoKzx2fxlpLUYXxC0sMTeWwSfY3pVmG4I0yU+lHMQz3zhHHhobEoetZQQlOWEPYAQl3VVi4lnLi9FU+fNMmPnN4O+35ALCLl6tMX2lg8zHdPmGgscgZ+qhpFEClusaEPoW3L19HPWlj7/A0pZLDRhEF68gZT0uWGIPXQDaowUaG2cU6z524BMCuLd3846GtrNYdDz91NhCXhx21WYrpKpN2gLpGYVIFG9qigzTl/Gw/E1dGGdt8ge2b5shFSqwpFgnNi9B4VMFYSxQpxhpyecvrZ64yc7XKPx3Zxo6RMsvVlLMXl/FiKFo4WHmDeqr80QySaqaaABuER+jTi5UcT79xK52FOh97/8t0FhutiJv0bVSJrRJFgRmNCG2R4/WJy0xdWmmx5EO/PMP0lRpxLsfNOs1NV1/nJYY4q31ZTQvgszJUD+rxieOFN97Pyxf2cGTvCW7fNYnJ5XFiWK4mLC418KJgFBUfuNwKRpXUK4+/cJ65Sp2llYSpS1VcnGd3Z8q/TD1FvdrgMftXLEm8Ti8KhrEnlXW6M4rhzvFJvvmJh6hJkW+/eC+vntlMb0Fpj5WZuRr1xAcOyxY1l0fWsKm/gzhnWKGNDbHnwbceZd/pZ/kJ+3nI7GfVmaA7CBiI6P/kN4L+D6hUp8wudlOTDu7cdYrxkXe4Fm3h7PwAs5dXcM6jajHWoMYi3rYKVNRSaShprp2xcsqDbz7Kvrd+y6/8KA9FN7LoC6CONfnsiej9+2+0ZHR2EEkTz9mZIaquzMHtr3H7xlNE77VxdXmAWlQgVYuXgAslVILNFSh1l9g2kOduOcd9L/2EkcmXeUq28f3oALO+I2gO1ZByBHAYdvy3rgkEXaMbhVJJuGPfJPf0Pc/OX05zfnWEE4P7OVHcxgXXQUpMPoaeIoy0O/bXphifOcWWi29wtao8Zj/A/7KbWSkGoKuEHWjtgsPkd/zIJ649a/JrSqV5qIhzsDlf4W8qr3Fk9RQbXYVGlGc+30Ut107BCD1uiXJjGZumVHyOY3aEJ9jLaemn5ixrQJcsegek1ShyJ0zb6De/05CBB9REna0NMNqq02YbLFjP5qjCmM6wO5lis6/QKTUQT01jLmkHb2s/f2Qj77ouVpxFfdZGm4Zbhw+HMY0zUVT/XGz93I+tFse8dhyi2XTWdaumdGqkwlnt5JzZzjNspY0GORLA4zHU1NJwJig8zc6NousMrz8lWTHGPJvL1V6NDrgTC7OF8XdV402obkClLcjdzPOW7g6HUhUhdUrNQ9XHVF3MqrekThEvmbjNJD6yllbT9EGWrHW/iqLkW2na81602H+Yon96WuzwschIiuGA+NgGzg1iZa1KmofMdc41o/Sy7rumxSynxgCWOE58HNf/NY5Wv5WmA1ODg8/zF8FY+ScIMgTQAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIxLTAzLTA2VDE5OjAwOjM2KzA4OjAw2QIc1wAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMS0wMy0wNlQxOTowMDozNiswODowMKhfpGsAAAAgdEVYdHNvZnR3YXJlAGh0dHBzOi8vaW1hZ2VtYWdpY2sub3JnvM8dnQAAABh0RVh0VGh1bWI6OkRvY3VtZW50OjpQYWdlcwAxp/+7LwAAABd0RVh0VGh1bWI6OkltYWdlOjpIZWlnaHQANTCQoD5eAAAAFnRFWHRUaHVtYjo6SW1hZ2U6OldpZHRoADUwaA/+0wAAABl0RVh0VGh1bWI6Ok1pbWV0eXBlAGltYWdlL3BuZz+yVk4AAAAXdEVYdFRodW1iOjpNVGltZQAxNjE1MDI4NDM25q51/wAAABJ0RVh0VGh1bWI6OlNpemUANTg4NkJCu/KUxwAAAEZ0RVh0VGh1bWI6OlVSSQBmaWxlOi8vL2FwcC90bXAvaW1hZ2VsYy9pbWd2aWV3Ml85XzE2MDk5MDM1MTM2MTk4Mjc0XzYyX1swXYYyMz0AAAAASUVORK5CYII=`;

        var aImgBase64 = `
        data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5QMHAwEtDqixlQAACb5JREFUWMNtl3uMVcUdxz8zZ865r32zC8uyssDyEERBcEGxaixCa43W1qJ9CLaxaU00UZNqjK3VxKQv2mLiC2qh8VWVWrVWFEVqbX0BAoIIKstjd2FX2F0eu3v33vOYmf5xzt4Lpif55czcM3e+v/n9vr/HiPf2DrJgyg4abz3jnLDAt/0gXDY0bCeiLRgNekQiiEw8NhZ0AFEEoU7WhfFYawjDZB7Fa3QsmZQ9klbyqVzWfbZr70+3rl69yQq+10VjrTu/Px+tikw0y0bEGxoTizZlBbQug5hT5lGYAAUxeGgSBXVZgZE1RuB54e7GBnFT5+Hif+WYJjGrv2hWhpZZFgekBDHyFiCStxSABOHGv2HBWrCJstpCJEA78TcAkcjIRAgQmqCoZ3R3Bw/WVdfOVoGvlkSS2UgRm1boGFzbZKMoBtYOqBNQ0wlVe6BxD1T0IYTGFrLQ1QyHW6F/FBytKQNLWQaHZE9BFIpZ1gwvVb7hBls6oU2+65L6AhAij5m8HiZvgJZtCOUnX2IrCCGx54G1BnrGIXYugA/nYA/Wj2iRWC1RBgsYioXwesGPey2S+PTGlv0bAEWf80Zv5ZK2Nawd+w49YYps72QqO2dTdbQFYSShNYTWIV9xjHzLFoLG/UR1R6B3DLx6OXwwG4ZkTMoRLpioRHAVm0aANbEYC4GBIiwc/2/+eOWvaKrtJr9lPm/uWkiufzw2lBgj0UZgjMFocKOxjNo7k+zoPPmZ73Ns+hsM/+BJZOMxzEsL4IQLVnyZGCiGw9g/2pbChQAubX2Th6+6F+X4rFj/Q3bumkc2yBAQobVBW0NkLL6xDBZhanMV1y+ezHWLJtM3cC23P/csb9WvxC5+GUEBu/ZSGHD48qMITMKNERcIzmnaxoorfoPn+Pz+1WW88/lsQmMxNiDQ4GuLNgZjJX4oaR5TzaN3Xcj8GfUcG/B58LFP2by+Gq/2RsLLV2MXvgVdNfD6vHKEJISU6MQfRoM1eCLPrQue4IzaTla9fQ1v7ZlLQUcUtWA4hLwvGCo4DAWSoUDiRw63X3cm82fEhFv54uesWbcP3wqi3kZm7r2ZdLEern4dWrpjN9gkhAFJpMsZK9J8ffobXHHmBjbsnMcr28+nqC1B5BKEUAwEhVDgGwetFSaSCAvjx+RKZ6rIuDjCJQotl1/QxEPLvsOUzmuhbgCxcBOoKFEitoDC6NKf026eq6b9iyhwWbt1IScKHoIoIZwlMoLIxAHoOiClQVt7mk9/cvUUDh0p8lnnAA/cPpe0p/C3XQi59bBge+yGfTUgLBhQaB37RcCk2sMsbn2fj7pb2HZgPFZrtBFoa9FWEBowRuBKg8KigUnNlTQ1ZMuH8Bx+eeNMioGmvibF29v7+OILDz5bgF20BibuhQPnxnwDZBz3GrShIdtNLjXEB3vPIu97CeHA1wLfCEItsQiMAD+C5rGV/OG2+cyaUneaFSqyivqaFF1HC/z8z58y4BtE9yzAgTntcT5KLKewOvGH4KwxuzHW8uHeSRQTwxgrMEZgAGMkOA6BsUwdV8kDt7VxWVtTCThfiNi1/yTWwsm85ok3DrO5fQiZTYOoxQ7XQf0gyCTjYlDoKC4VStFS142Vlj6/gjCSgMGQKGBig1khaR1XzYrb57Bo/ung96z6mGde7wKhGAhgGIlMewjlImUVlb1N4B9lQFgsFqxBYcJ4B2NxiceRtoQGJAJtJFhJXZWL67k01GdZfvPZLG4bUwYvRtz96E5WPt9OYB3wJDKbRaU9hKNAKRoCn/sedqBPc0sUESQ1QRHFUWA1dB0bDUaQEwZjJBZoaazgzqXTmdlajXIklTmXGROqSuCDhYh7H/uEh/7WjpUuMpNG5HKIVArpukjHQSpFTb6Pts4eeoYkRo0UJItCmyQxCLb3TEUIOGdSO+8dmkIqLbnzhhncdHUr/+8pBJp71uzhoef3Y1UamcshcxlkKoXjKhylkI4Drkelb6gv9vIhLRgtwUZJFIx0P9rQl28mitJcNH03KLh47li+v7iFfFGz74thjDk95pc/f5BH1h2CVBpVW4VbU4FXkSGdTZPKpvEyHm7Gw8kI5vTvQOqIjaIVYw2gAYMstUymyMGOGja0tzFn/EFmTuihujpLxnNY+dohrrp/B69u7T9NgYP9ESZbRaqhFq+6Ai+XwcumUVkPlXFxMy4q45BzDF/b9zrDxmGHnAAiKrlAxr1aXKOHTrq8tvNi6rODLJu7keEw4P61B1j+4iE+6/G5ZfV+Pj1cKCedqgpyo0eRqa8lVZUjVZEhlUvhZV3ctEKlHLyUxxX732L+oW38U5zJPlNXCsHYAkkRwmiICry8eS7vHJjNsvM3kjMbWfFKDwOBxXMVSqlSLe/oD2k/ZslUpknl0qQqM3hZDzft4noKNyVRacUZhUMs3bKKo77gKXkeQakQWUAj4+426VZMSL7P8tt1Szk+XMUdX3mMuc3tyHSW5nEVLF86galNaayFFz7Ks/toiOdKlCvxUgo3HQO7KYVKeTSFfdy1/j4mdmzjUTmP7XZcfFhGxOIw6rv3lXuBuCU7dKSBI4UmvjXnbc4fv5Pu4gS+ecl5fOPcUWzaX+B3b57gxV1FDBKlBI6SSOWglIOjHJyUZOJgF3e8dDfzP9nAc3ImD8qLKWpJ3ORGCQkjBK0vWNDlXtHGDaOTSrFk4U7uv/YRPGX4x0s3ss5cQ0dVI4ORg/IkSoq4Y3ckwhFIAdV6kK9+vJ4l/1nN2I7tPCfP5j4u47hJxckGXQKHAEHLMxYxEl62VKexAukqLrmgnbsnPc7MR7o4ICayecpFvHf2lZzI1aA9D0dKpImoMsNc2LGJ2TtfZdr+rfQELqu8C3gmms1Jo8AGCX8iICxZQmSmPdlTGLaNce9uTulYkvToKMa6AywZ2sI14btMHTiIdVxCx6GQrQOpSBdPkvKHsTriuKhknTONv9o57DD1WG3BhqW4L7sgIJPhiKg96+nlx4/rn8ULKIeIPbVXBOlKmuxRpqf6uEzvp+XEAar9kxhj8N0Mh1P1vGta2OHX0W5HEUYiBi6RTp9CPg2E1NS4fxTj5jzV1ttb/Evgc1aZCEktLllihKSA1AgDEh+BBhthrcAaiwlkHM4kwCWfn+Li5LbleWb36NEVP3IGewa6mye3fVIYDufpMGoo3fVMokDpgjpyM47FahPfLyKBjQw2iuL8bsPEgroUavEjSm/Pc3aPG5e5ubPz1+86Dz/+d55+ZFHnqic/2+TKMI/VrZFvcmgbbzhyRTenXFy0TcaJWF3udK3+EuDIWJDJOEerqpw/1VQ33N/R8Yv3zxh9Gf8Da0YxuA1sN9EAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjEtMDMtMDZUMTk6MDE6NDUrMDg6MDAN7WRtAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIxLTAzLTA2VDE5OjAxOjQ1KzA4OjAwfLDc0QAAACB0RVh0c29mdHdhcmUAaHR0cHM6Ly9pbWFnZW1hZ2ljay5vcme8zx2dAAAAGHRFWHRUaHVtYjo6RG9jdW1lbnQ6OlBhZ2VzADGn/7svAAAAF3RFWHRUaHVtYjo6SW1hZ2U6OkhlaWdodAA1MJCgPl4AAAAWdEVYdFRodW1iOjpJbWFnZTo6V2lkdGgANTBoD/7TAAAAGXRFWHRUaHVtYjo6TWltZXR5cGUAaW1hZ2UvcG5nP7JWTgAAABd0RVh0VGh1bWI6Ok1UaW1lADE2MTUwMjg1MDVVSB2xAAAAEnRFWHRUaHVtYjo6U2l6ZQA1MzAyQkIOnyXlAAAARnRFWHRUaHVtYjo6VVJJAGZpbGU6Ly8vYXBwL3RtcC9pbWFnZWxjL2ltZ3ZpZXcyXzlfMTYwOTkwMzY2Mjc5NDIzMDZfMTNfWzBdw0U41wAAAABJRU5ErkJggg==`;

        var qImgBase64 = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH5QMRATAdmNpapgAACVNJREFUSMddl2mMXlUZx3/nnHvvu8w7GzNvZ+l0pp12mNKWrsCUrSwpi4DKIhibAhoIJgYUvipijfGTiiYYMWpACQQ1BIoIBSq1UHbolCmlLdOWrrN01nZm3u3es/jh3qHFDycnNzn3+T/Pc57zf/6PuPKRIe5akxLf31y4VpRL39VwuzUojAVrwGjQGoxJlgMbgY4gMmB1ciYCbUGH8XcUxbsxEFXIZeW2dCr12A/ua3z5+IkoEqkfOezE4G0G93vromYsCaABa2NjRoOJwFjQ5iyD5gyQKUM462B0lsM6OSPxVGV8Qbv8aX/vg38S6p6h6522T1pnm3HEYMaAM2eAbGIkBGwZ1GkEIWiNqxgoKqiIGMRVvgo6m40kMx52rDpXd4/nU7mnHKSbiWQMKhxIBdqBsOAk0nngn4L2ndi6ndDcj6ybRkiLKQa4w62I8YXQ3w0jeXAaJ1T8P+IrS2saw8rY/ULde8IZ4YN18f0ZE6c6MmBC6rxpSvNfQC/Zgm34AhUJMlNNSJvCOY1WjkpmAlMzjhidi3/8fMxrazGH28BUktQn12FDMAZhNJ6RKRACnAVlwQFYqARckP+An6x/gu3V7/P0cBu5nd8kGG2l+mQn0vhEzlEBpqsGiZqOUG47wszy7bi2Xcj/XId76yKcNbF9iKMWAuckXpxSzhRRpKEkWJXfweM3/5x5+Un27biBjl2XY4uNQIUQh7UCbR3GOFKlNuTwPII9F8PBpZQueB17+2a86iLRSz246CxgBzhQnPvgJiIDoY1BQ8PKlk/4862PUB2U+fVrG3j9k0vwyVJbEzBZCAm1o6O1lusvnc/B41NMlQxzGlJ4KiA6MhfvyHnYxiGiC9+B6Rwcaokzai04B84iv3wazoIVNKRP8cvrfsW8c6Z47I0NbN+zjLIJ0Q5uuLyDGy9fiLGCzrY61q9tJ/A9UimPhzau5qqeNiqE6Ik88pUNMDAf97U3EB0jYGejdiBBYkxS+hHYiNtWPEdP616e2L6ebftWoG1EWMlwcqLMto+GuHX9Ilrn5GhvqeHdvpMcHixw2aq57Ds6xb/eGiC0Htpa3KlW5PYbUZkQ9Y1tSD+KQS1gHRJtmF3V6Uk2rniFY2MtbO5dSxhVCLVHdY3ivIV5hFR8tHeUjtZali5qoFC29KxopaEuy/ufjrO6ew4PbVhK/pwqjItQR1Yh+y6DJV+gukbBiATcIbEanAYs6+b1sazxCM/vvpjhU9VYY4k0ZNNp5rXUksmmeLN3hKaGavL1GQbHymRSPseGStRUBWQzPiMTGs8LsNbHuiwcWIPxQ6Lu3SCj+MUAHiYhDWB+7SHKOmL/iTq0Bg0IITg8XKD/RIFQQ2QEd93Yyc7PJ9j85gChdjgEQjisU3S01nB1TwubdwxxakYjR85HjM+DVQfh1YtwpwIQ4OE0OIEvIubX7+PYSJ7eg51oZzAWVi1u4IKlTUwVNM5CQ12Gr6+by9t9o3zr2gWkAo/2pioGRguUQ0NjXZq25mqOTmh27J7EKYUqN6Eb+yDl4iJzFi9OtSRbFbK4aYAKGYqmGm0F1gk+3j9Jb/8UxkqqsgEP3NHNS28P8scXDxFaifQ81q3M054P+O+uMYbHi8iqaoJ0Gr8qg3GS7NgiFLsYtxLnHDiHh4nASYSrEEiDtY5otje4hF+to64u4M6bFtI1v5anthxhaXceh8I6wUwIly6fwy2XtfLcjkH2j8HhMU0Fi3CCe/8tueqI5a5KyKhQCbA2gKE0LegfmsOqlgMoYyi5NCDwlGRJZy0Xr2xm4JRh79ZBFnfWs25NC/98Z5SZYkQu7TG3qYqdhwpc2TOP+1oybNl1mie3TTJRMKwYOcz8UYOq0l8SiIcxAFS0z96Jbm5a9i6L27/gw2OrkNLS0VZNW1sd7x0OOTZSJjKO87vqOT5WYsfn02jn0VjvMVawbP60zMhUkZVtRfoHQ2aMRzYjaA7HGRA5Qu3H7BU/p1llAcPTC6ivEixdMIHxUxAEDJ02vH9ghtFQETTU0tFez+ruOrb0TlAuRShPkMt4pAKPkpZMFR3b95UYLjhMLsXyqV7apw+zRXUzgQJMQpk6kTFmhu19S9h3sos7Vn9Avm4a6WcIZQBK4JymPudz5/oW9g+GfDoAtXVZchlLR96jFMFM2SE8SZD1CTI+Vcpn7bFdeNMz9HqdIFTcciHhamfAaSYnA5798ApWNO/n9pU7sdkUKghQ6RQLGgO+01NDKbT8fVcRm0kxr6WG+65u4YH1eY5OaApW4KcVMu1BxuOCyb1s7HuOj7w23tZ5iFzC1wZF07c34ZJeFWkGC3muPn8f6xb10Te4gjHbzpquWjpbc+wdKvD+0TJl4yOEYqag0ULhBz4v9BWZiUAGHl6gaK6M88NXfoY62c+P1fUcdw3gorgZOYtizh2bcCZWIM4wNRWwZ6CTG5fvYG37HvZPrORAsZF9Jy1jZYUVPlIoJA6UZKgg+eBoyEwk8HyJTKfIF0d54OVNXP7ZqzyevpTn3bJEj9mEMg2Kc+7YFHvhEvkTMXQyz8GJhdyy5k2u7XqP09O1nCgsxmazSOWhlEB6HiLwUL5C+goZZPEDwyUje7j/xYe5ZM8W/hj08Fu5jlAT9wNn42slQtD1DxeLMr58Y0I4kBkuW97Pbzb+lfnPDvPq7uv5ePHFfNZ1JcO5JqQSKCFwCupdiQuHdrKw/y2uffdZpiuap9Vq/mB6KOoyznhJHRniDhAhWPCEQ6pEBbozu3UgJF2tk3zv0FauKeyhvTTAYGM3Q1XnMJZrQIiAutIYzeVJOiaPcbrk+EjN5XfqIna6NkyYyNpZUBcBEVBBVC/729bpGbkeoc9EzaxEiUld+LAwdZqrpj7gmmCMtrED5IpT4KDiBwznmtlRaebt1Ll8WGlEa2Jh4dxZoPHdQkQuF20V9cufunl6uvwXrWVDHG1ywCWlb5OKNw4hHTXCEERTeFER5zTWCwhFiumSh7FRDDJLSu4sO8ROeL6ZrK1O36s+27rp4OvvjBcnp8KLrDbZL71zLhFnNp4oktmpoi1Fq5ghzYxLUzAe5cjijI7Tav8PUAgQEpD4gZxY1JX+xaOPXvGMKmSus5esyfXuPUS/r0pzy8WwHSsSYa/PGmncWUoxGeisTc6c5aQlAZqNVCKAmhrxVm1t48N3393yTN8n4+H/AIZ4QYH/d3pvAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIxLTAzLTE2VDE3OjQ4OjI5KzA4OjAwPa6YJQAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMS0wMy0xNlQxNzo0ODoyOSswODowMEzzIJkAAAAgdEVYdHNvZnR3YXJlAGh0dHBzOi8vaW1hZ2VtYWdpY2sub3JnvM8dnQAAABh0RVh0VGh1bWI6OkRvY3VtZW50OjpQYWdlcwAxp/+7LwAAABd0RVh0VGh1bWI6OkltYWdlOjpIZWlnaHQANTCQoD5eAAAAFnRFWHRUaHVtYjo6SW1hZ2U6OldpZHRoADUwaA/+0wAAABl0RVh0VGh1bWI6Ok1pbWV0eXBlAGltYWdlL3BuZz+yVk4AAAAXdEVYdFRodW1iOjpNVGltZQAxNjE1ODg4MTA5/RRjigAAABJ0RVh0VGh1bWI6OlNpemUANTQ5NkJCxr4eowAAAEZ0RVh0VGh1bWI6OlVSSQBmaWxlOi8vL2FwcC90bXAvaW1hZ2VsYy9pbWd2aWV3Ml85XzE2MDk5MDM1MTE3MjE2NTg4Xzc0X1swXXTdtQ4AAAAASUVORK5CYII=`;

        var mImgBase64 = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAYAAAAe2bNZAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5QQcEjgoWJoNaQAAC7BJREFUWMNNmGmQHtV1hp+7dH/r7ItGI42k0TZiEQNCCxahhIMKkJHignihEqpsA1X54XIqScUOTv6QKgenHDsGlxMnjhcMdlwFjouQKDiyMEsstIBVQgIhpJEQszDSaPbl27rvPfnR/c1MV92a7jtf93nvOe957zlXifwt6o8fJszVOnwtuMdGs39aVZkdEgt4D96Bc0t/nQPnwQv4GFyUzMUOxIGLk/vF30bJcxwl3/AOahGZsDbU0tjyjY4O859njvx06F9++Diq+NAHKGVaqir8x8hXPi8eEFl60fvUeGqoDiZ2ID4FIxDHKTgHcTV5jgV8LZ2Ll30jTt7xiuaW8gsH9q7+ijX2gglvf6y9XPXfdEo+J4hCKZJLQEl6m86JJAMFolOgGpxKDMeSGokSz+GT4X3yvnfJdyWdl5hqOdoyODjXrVhxxJZrap/Php+X2CmcBeXTYVLDAC4BgAHxKB8h4QisOI9umcaYMuIMbiZEBleiZjsQb1NAsLjAxYWmlxLEWaameGDg0uArNsvMYwuqWWF08uM4tat84pH6vQsIcsP4lnNw/Uu45nNQGMPnYsQ4QCM1YKoZSh3oszfD6e0wV0SmQLSB2CcLwqfeAbSAd2p6ZvIrSj06KqJ1snhJ3exSMkYOfEwTczR3v8XItqdwHcOILRNOt9J4dTNhVES8wyuhYoVS04dEKy+hfIiutBC8fSvVFz+OTLSAq6b8S0nvo/QecDFWlE08ouuxFBCAGObzdGXe54m9T7Gp7wR/c22K9y7uoHmkHzPTQn58LTYKiEnoUhUo5UeJW0eJ2iaZ3nqI6u6XYMV5zOF78Ce2IqqWhqtOAb0YBZvEVS15xDmIYigbunPv8O37nuDAjccYmNjAuqMHmB3ZjY4CxNeIlFBVDicKJ4J3QjDTiZ7qIhwQ5NIW5ra9RNR3Cv1HL2AKJWq/uSmxhV7ijgggKB4cEbRKXOUduBo4RVtujH8+8Bj3bT7BywPbefa3B7g42olD4Tw475PESRNMPDgnxB4EjwICXaB3rab97tMcLPwTtYpCnvkM/simNNHiJQmJHDb58jKEYsiHU/zDJ77GH9zwFoff28F3/+dTjC9kqMYxsTOIxETO4kVwKRgNZDOKjsYsPZ15tvQ2ctuNK9l+QxdXJz/Ob54Zo/x7z8O+QzDQDcPFJDkklREtWHycxFDqBFbs3XSUT2w6wntja/j+qw9ybS7EUWGhliFyHiWK2IM1mtYGy7ruAjdsaKW/r41Na5rZsKaFpmKW2HkWSjWYjuDkXehwEH/7Yewnf4378T6kZFL9Sfhjk3RL0Ymiq3mYL//+k2SU57sH7+PcRy2EepZSLUs1gshpVrTk2LOtg503dtDf10lXe5725hwCzMzXuDQ8x/Ez53nz7BTaGD69dwNaLPbNu5H1l3A73iY4tJ3a+c5EPgRwgk1Ucenqa79If+sIr13YzonBLXhfoeo0lUgnJBXFrps6+Ps/u53GYoY4doxem+elI4O88uYIZy+O88FojWvTjkok3La1Fec9Igo1uR5O3An3P4O+9xQM3Ae+nISL5WC0QxvNwzf+ikxY5b/ObGOmFGJxONFETuMRREFgNZMzZZ4/9D4nzk4yMLTAhaE5pmerZAKhuTHH9usa+NhNndxxSxfFQgbnLV6DGu1Dxlup9LwHhe0wU1xU+oTAqeQHpkxv2wUGZ/J8NJFNMh3BeUXsFYJGvBAYODMwydd+cIap+RiPobs9w+6tK7mlr4kNq4vksjlOX5jktZNX2bOjBxOCch41vRk13I9c9zLq5svwcj+iIlAau5hKsaarMEwxO87Jcxs4fXkVXhzOK5yolPgKlEYpReyEhqLl9lva2LV1FRtW5SmVqgyMlHnu8ChvD0wzMl5j+3Ut7N62GhModGxB57C1dqKMQ3Uu4EUnnPFuWZh8hr7OIdoapzgVbaZUawYd40ThJdm68CAIsXNsWdfIt/58J5nAcnF4hmcOXuR35yYZn3FUY4M2CXijFVqBNgodWtAWX+4FlcEHcSJ+aSZbXJyGydLeMEc+8FQVVEVhvEJE4dEo8ek7iUZWIvj5/w5y/N0JRiYjXBShtSDKoqwBLXjv8GhAYaxFS4BYQ9+lTu4cCXnzPc8x7dMNtK4zABIxP2+IYoWJNd5pRCtENIhCYfEIHlBK8cFHC/zyt2PUHGgFShmUEjrbs2xc3ciKloCxqQqNxTxKK5QxaBUimRy7Lk3z1RNVvhPGHDNpjYNgidMwSZWzw12MzTWSsfNYqRJJCEqhtMIpBcqjfLKvKK0IQouLkr1NBG7dUuSvv7CVnTd0kc1YytWI8akK4/MelckSqDwuk6e3PEjRlYnrgifJvmIXqzARBktrmKm00987wMaeId4dvQ5jBKXA+aQw0kaBMSilUcYk4JShkDM8eM9a7tq5ZlGzApuhsZChMlTC5BsICDGZDG1unhLwkTSmtbQHEfTyotu5Ilfm17Cla4Y1K8rENosEhjBr6GoLWduVo7EhA1ojKnW9URCEqDDLbMXhFqVi6RoaL6MLeaQhR1/pLP3jb/G+WsGvZS1QXSy2EgKnouNmF3j6jfu4d+MbHLjlFK9c3sbatjyfu3sNW9c3k8tohq5VyAUerww6DEAprLJUgZ+/PkWYvcid/Z10tWaZnK9x8oMSPzs6i7MWbXOsnf6QTVfOczC4iYmwDaoLST2diF5UL0hBhNOXe7lwpZt7N7zBy5v2cP/+T/OHd65La9H6HgYvnphA5woE1iXaoxWDC4pvHZrn2ZPQVAyoOJgoCWWXIQgUa0sTPHLsOSDke9yAr2rwOq2xBY1PWw5xQMTIUJbvHHmApkKJv/vsYXZtKS4DkoJWMDVXhjDAFPMEDUUyzU1km1uRfANjVcvAtGJkQVPVljCjIWfYf+55brx6lBftRs6qleArS9UCPgXj4mWFzgwHT93Ki2f30KSOUrn6Y8am5hd3jUot5uTADP9x/BomzBDm89hCAVvIExSzBLmQIGsJM4YgExCGFpW1PHD6F3z21Sd5hyLfNLuZ842pEyT1dj2blCzuCkqEiWHLX/7oYZr/ZILdG57g6LFphsyj+Ewbl6/M8cr5ea7MFAkbFMbohD9Go7QGrdAqET1rFC6w7D/+7zzyq6+TqS7ws/w+zsbdINVlEfGAw9D2mceTyeU5HzM3n+ed0eu5rucyt60+yLXJ8/zk1RW8NtzJnMpggwATWIy1GKuTERiUNaggi8laVlWv8sixZ3nopSfIlWb5eriHf2MXvt5d1ps88UCEofVTjye8kaV+WmK0VLgy0cjRC1tYv2qMfeErfOxfXyecrNHAHHFzJ/P5JghCTGDQQYhkApq0Y+fUGW6+/BqP/uKr3PG7XzJgm3gyuJ0fqNtw0RR4k5BW6kASYIr1TwtKL3V7i10ki+Tq6IFH7UEeev3/aJVrYDWnN+zlw6aVXC22EgdFrDjaFq6yujbNzpFTZIcHmLd5Dpn1fKN4B+/PdyK+ulSAi4BESUuUDpXd/P3BSpTrQaUuq4Mg7ShJusVQHD25MvvNafYNHmajrtBQmkzIryxaBHCUghyXXBPvNF/PT+LNDNTamI7VEj/q2UudLzEQEYYLQ2rlrue+NHql9BS4pOn3LIulLHlLBLRCuYhCWGVPYY7uqUsUZ8cIXZUYRanQxFiuixNzrVyJDZELktXXj1RIpZ/6oUDS4ygcXStzX7IrW/wL1fnorqlZ/0mpFzqqvpPWgaTuigRxwnxsODjXBqoVwlpqJAZnYLYGNZWGoJomRd2wW0aHpOdW2tHSbA6u6m75b3Pg/i/Mbu4tnPlwaKGnXPF9SMr0uhj5lGT1jrP+LGmvLPXDI790cCR13SL5fz1bF3XTJHWjsrS22oP7D6z7q67uwgV9a3+BH337+Lure9q/2Lum8OVCtjaaHOZ4iKLkxCmOEkPeLRlfPL2SZYdJ9bnlCzGJcZWqN0lrksv4kVWrGv+id23vF595+nvvbru5g/8H/1tofW6IUwkAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjEtMDQtMjhUMTA6NTY6NDArMDg6MDD1Sxs+AAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIxLTA0LTI4VDEwOjU2OjQwKzA4OjAwhBajggAAACB0RVh0c29mdHdhcmUAaHR0cHM6Ly9pbWFnZW1hZ2ljay5vcme8zx2dAAAAGHRFWHRUaHVtYjo6RG9jdW1lbnQ6OlBhZ2VzADGn/7svAAAAF3RFWHRUaHVtYjo6SW1hZ2U6OkhlaWdodAA1MJCgPl4AAAAWdEVYdFRodW1iOjpJbWFnZTo6V2lkdGgANTBoD/7TAAAAGXRFWHRUaHVtYjo6TWltZXR5cGUAaW1hZ2UvcG5nP7JWTgAAABd0RVh0VGh1bWI6Ok1UaW1lADE2MTk1Nzg2MDB46SPQAAAAEnRFWHRUaHVtYjo6U2l6ZQA2ODUyQkLIBZZoAAAARnRFWHRUaHVtYjo6VVJJAGZpbGU6Ly8vYXBwL3RtcC9pbWFnZWxjL2ltZ3ZpZXcyXzlfMTYxOTA3NzA4NTU3MTI3MTNfNThfWzBdSOrVQAAAAABJRU5ErkJggg==`;

        var left = 0;
        var top = 100;
        var innerList = [];
        var outerList = [];
        var innerli = "";
        var innerli1 = "";
        var outerli = "";
        originalInterfaceList.forEach((item, index) => {
            if (item.type == "0") {
                outerList.push(item);
                innerList.push(item);
                outerli += "<li>" + item.name + "</li>";
            }
            if (item.type == "1") {
                innerList.push(item);
                innerli += "<li>" + item.name + "</li>";
            }
            if (item.type == "2") {
                innerList.push(item);
                innerli1 += "<li>" + item.name + "</li>";
            }
        });
        parseInterfaceList = innerList.concat(outerList);
        GMaddStyle(`#vip_movie_box {cursor:pointer; position:fixed; top:` + top + `px; left:` + left + `px; width:0px; background-color:#2E9AFE; z-index:2147483647; font-size:20px; text-align:left;}
    #vip_movie_box .stitle {position: absolute;font-family: "楷体";font-size:16px;right: 0;top: 130%;width: 1.3em;padding: 0px 2px;text-align: center;color: #fff;cursor: auto;user-select: none;border-radius: 0 10px 10px 0;transform: translate3d(100%, -20%, 0); background: #0000ff;}
    #vip_movie_box .item_text {width:0px; padding:2px 0px; text-align:center;}
    #vip_movie_box .item_text img {width:30px; height:30px; display:inline-block; vertical-align:middle;}
    #vip_movie_box .vip_mod_box_action {display:none; position:absolute; left:30px; top:0; text-align:center; background-color:rgba(255, 255, 255, 0.2); backdrop-filter:saturate(1) blur(15px); border:1px solid gray; overflow-y:auto;}
    #vip_movie_box .vip_mod_box_action li{font-size:14px; color:#DCDCDC; text-align:center; width:80px; line-height:27px; float:left; border:1px solid gray; padding:0 4px; margin:4px 2px; background:rgba(0,0,0,0.6);border-radius:2px;}
    #vip_movie_box .vip_mod_box_action li:hover{color:#FF4500;background:#00ff00}
    #vip_movie_box .selected_text {width:0px; padding:2px 0px; text-align:center;}
	#vip_movie_box .selected_text img {width:30px; height:30px;display:inline-block; vertical-align:middle;}
    #vip_movie_box .vip_mod_box_selected {display:none; position:absolute; left:30px; top:0; text-align:center; background-color:#272930; border:1px solid gray; overflow-y:auto;}
    #vip_movie_box .vip_mod_box_selected ul{height:455px; overflow-y: scroll;}
    #vip_movie_box .vip_mod_box_selected li{font-size:14px; color:#DCDCDC; text-align:center; width:28px; line-height:27px; float:left; border:1px dashed gray; padding:0 4px; margin:4px 2px;}
    #vip_movie_box .vip_mod_box_selected li:hover{color:#FF4500;background:#00ff00}
    #vip_movie_box .qelected_text {width:0px; padding:2px 0px; text-align:center;}
	#vip_movie_box .qelected_text img {width:30px; height:30px;display:inline-block; vertical-align:middle;}
    #vip_movie_box .vip_mod_box_qelected {display:none; position:absolute; left:30px; top:0; text-align:center; background-color:#fff; border:1px solid gray;}
    #vip_movie_box .vip_mod_box_qelected li{font-size:14px;text-align:center; width:150px; line-height:40px; float:left; border:1px solid gray; padding:0 4px; margin:4px 2px;}
    #vip_movie_box .vip_mod_box_qelected li:hover{color:#FF4500;background:#00ff00}
    #vip_movie_box .belected_text {width:0px; padding:2px 0px; text-align:center;}
    #vip_movie_box .belected_text img {width:30px; height:30px;display:inline-block; vertical-align:middle;}
    #vip_movie_box .vip_mod_box_belected {display:none; position:absolute; left:30px; top:0; text-align:center; background-color:rgba(255, 255, 255, 0.2); border:1px solid gray; backdrop-filter:saturate(1) blur(15px); overflow-y:auto;}
    #vip_movie_box .vip_mod_box_belected li{font-size:14px; color:#fff; text-align:center; width:100px; line-height:27px; float:left; border:1px solid gray; padding:0 4px; margin:4px 2px; background:rgba(0,0,0,0.6); border-radius:2px;}
    #vip_movie_box .vip_mod_box_belected ul{height:355px; overflow-y: auto;}
    #vip_movie_box .vip_mod_box_belected li:hover{color:#FF4500;background:#00ff00}
    #vip_movie_box .aelected_text {width:0px; padding:2px 0px; text-align:center;}
    #vip_movie_box .aelected_text img {width:30px; height:30px;display:inline-block; vertical-align:middle;}
    #vip_movie_box .vip_mod_box_aelected {display:none; position:absolute; left:30px; top:80px; text-align:center; background-color:rgba(255, 255, 255, 0.2); backdrop-filter:saturate(1) blur(15px); border:1px solid gray;}
    #vip_movie_box .vip_mod_box_aelected li{font-size:14px; color:#000; text-align:center; width:200px; line-height:27px; float:left; border:1px solid gray; padding:0 4px; margin:4px 10px; background:rgba(0,0,0,0.6); border-radius:2px;}
    #vip_movie_box .vip_mod_box_aelected li:hover{color:#FF4500;background:#00ff00}
    #vip_movie_box .melected_text {width:0px; padding:2px 0px; text-align:center;}
    #vip_movie_box .melected_text img {width:30px; height:30px;display:inline-block; vertical-align:middle;}
    #vip_movie_box .vip_mod_box_melected {display:none; position:absolute; left:30px; top:0px; text-align:center; background-color:rgba(255, 255, 255, 0.2); backdrop-filter:saturate(1) blur(15px); border:1px solid gray;}
    #vip_movie_box .vip_mod_box_melected li{font-size:14px; color:#DCDCDC; text-align:center; width:80px; line-height:27px; float:left; border:1px solid gray; padding:0 4px; margin:4px 2px;}
    #vip_movie_box .vip_mod_box_melected li:hover{color:#FF4500;background:#00ff00}
    .add{background-color:#00ff00;}`);

        $(function () {
            $("#vip_movie_box").mouseover(function () {
                $(".item_text").slideDown();
                //$(".selected_text").slideDown();
                //$(".qelected_text").slideDown();
                $(".belected_text").slideDown();
                $(".aelected_text").slideDown();
                $(".melected_text").slideDown();
            });
        });
        $(function () {
            $("#vip_movie_box").click(function () {
                $(".item_text").hide();
                $(".selected_text").hide();
                $(".qelected_text").hide();
                $(".belected_text").hide();
                $(".aelected_text").hide();
                $(".melected_text").hide();
            });
        });
        $(function () {
            $("ul").on("click", "li", function () {
                $("ul li").removeClass("add");
                $(this).addClass("add");
            })
        });

        function selectedList(episodeList) {
            var selectedList = [];
            var innerli = "";
            if (!!episodeList && episodeList.length != 0) {
                episodeList.sort((d1, d2) => {
                    return d1.name - d2.name;
                });
                episodeList.forEach((item, index) => {
                    selectedList.push(item);
                    innerli += "<li title='" + item.description + "'>" + item.name + "</li>";
                });
                $(".vip_mod_box_selected ul").empty();
                $(".vip_mod_box_selected ul").append(innerli);

                $(".selected_text").on("mouseover", () => {
                    $(".vip_mod_box_selected").show();
                });
                $(".selected_text").on("mouseout", () => {
                    $(".vip_mod_box_selected").hide();
                });
                $(".vip_mod_box_selected li").each((index, item) => {
                    item.addEventListener("click", () => {
                        if (document.getElementById("iframe-player") == null) {
                            var player = $(node);
                            player.empty();
                            player.append(videoPlayer);
                        }
                        var num = "";
                        if (host == "www.bilibili.com") {
                            num = 0;
                        } else {
                            num = Math.floor(Math.random());
                        }
                        innerParse(parseInterfaceList[num].url + selectedList[index].href);
                    });
                });
            }
        }

        var html = $(`
    <div id='vip_movie_box'>
    <div class="stitle"><ul id="m">解析线路</ul></div>
    <div class='item_text' >
        <img src='` + ImgBase64 + `' title='视频解析'/>
        <div class='vip_mod_box_action' >
            <div style='display:flex;'>
                <div style='width:295px; max-height:450px; margin-bottom:10px;'>
                    <div style='font-size:16px; text-align:center; color:#000000; line-height:21px;'>视频解析</div>
                    <ul style='margin:0 5px;'>
` + innerli + `

                        <div style='clear:both;'></div>
                    </ul>
                    <ul style='margin:0 5px;'>
` + outerli + `

                        <div style='clear:both;'></div>
                    </ul>
                    <div style='display:none; font-size:16px; text-align:center; color:#DF0174; line-height:21px;'>B站大会员番剧解析(专用线路)</div>
                    <ul style='margin:0 5px;'>
` + innerli1 + `

                        <div style='clear:both;'></div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class='selected_text' style='display:none;'>
        <img src='` + sImgBase64 + `' title='视频选集'/>
        <div class='vip_mod_box_selected' >
            <div style='display:flex;'>
                <div style='width:295px; padding:10px 0;'>
                    <div style='font-size:16px; text-align:center; color:#DF0174; line-height:21px;'>电视剧选集</div>
                    <ul style='margin:0 13px;'></ul>
                </div>
            </div>
        </div>
    </div>
 <div class='melected_text' style='display:none;>
        <img src='` + mImgBase64 + `' title='听音乐'/>
        <div class='vip_mod_box_melected' >
            <div style='display:flex;'>
                <div style='width:auto; padding:10px 0;'>
                    <div style='font-size:12px;color:#40FF00; line-height:21px;'>偶尔慵懒的听着音乐，喝一杯咖啡，享受午后的阳光</div>
                    <hr>
                    <div style='font-size:15px; text-align:center; line-height:10px;'>&nbsp;</div>
                    <ul>
                     <iFrame src="https://music.waahah.xyz/#/music/playlist" width="320" height="500" frameborder=0 scrolling="no"></iFrame>
                    </ul>
                      <!--<ul>
                     <a style='font-size:12px;color:#2EFEF7; line-height:21px;'>双击歌曲名字播放，如果没有播放请稍等一会，大概2-3秒就缓存完毕，不要连续点击，服务器小，小主手下留情
                    </ul>-->
                </div>
             </div>
         </div>
    </div>
    <div class='qelected_text' style='display:none;'>
        <img src='` + qImgBase64 + `' title='优惠券'/>
        <div class='vip_mod_box_qelected' >
            <div style='display:flex;'>
                <div style='width:330px; padding:10px 0;'>
                    <div style='font-size:28px; text-align:center; color:#F78181; line-height:21px;'> <p><a href="https://www.zuihuitao.cn" target="new">优惠券查询</a></p></div>
                    <div style='font-size:15px; text-align:center; line-height:21px;'>&nbsp;</div>
                    <hr>
                    <div style='font-size:12px;color:#088A85; line-height:21px;'>注：我们只提供好的优惠券查询网址。</div>
                    <hr>
                    <div style='font-size:15px; text-align:center; line-height:10px;'>&nbsp;</div>
                    <ul>
                        <li>
                           <p><a href="https://www.zuihuitao.cn" target="new">淘宝天猫优惠券</a></p>
                        </li>
                    </ul>
                     <ul>
                        <li>
                             <p><a href="https://zuihuitao.top/vip/index.html" target="new">关于脚本</a></p>
                        </li>
                    </ul>
                </div>
             </div>
         </div>
    </div>
    <div class='belected_text'  style='display:none;>
        <img src='` + bImgBase64 + `' title='直播电视'/>
        <div class='vip_mod_box_belected' >
            <div style='display:flex;'>
                <div style='width:360px; max-height:300px; margin-bottom:10px;'>
                   <div style='font-size:20px; text-align:center; color:#DF0174; padding:5px 0px;'>电视节目观看</div>
                   <div style='font-size:11px; text-align:center; color:#fff; padding:5px 0px;'>使用说明：进入电视频道如果一直转圈，先点一下暂停，再点播放，就可以正常观看了。&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
						<ul style="margin:0 5px;">
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=https://n24-cdn-live.ntv.co.jp/ch01/index_high.m3u8">日本全天新闻</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=https://jpts.sinovision.net/livestream.m3u8">美国中文</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=https://movie.mcas.jp/mcas/mx1_2/master.m3u8">日本Tokyo</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=https://movie.mcas.jp/mcas/smil:wn1.smil/master.m3u8">日本气象</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=https://nhkwlive-ojp.akamaized.net/hls/live/2003459/nhkwlive-ojp-en/index_4M.m3u8">日本NHKworld</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=http://ebsonairios.ebs.co.kr/groundwavefamilypc/familypc1m/chunklist.m3u8">韩国EBS1</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=http://fhs8037.bd-61.ktcdn.co.kr/mtnlive/_definst_/720/chunklist_w1038526770.m3u8">韩国新闻MTN</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=http://58.234.158.60:1935/catvlive/myStream/chunklist_w1609082033.m3u8">韩国TBS</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=http://1.222.207.80:1935/live/cjbtv/chunklist_w394587109.m3u8">韩国CJB</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=http://in.getclicky.com/in.php?site_id=101420445&type=ping&res=1549x872&lang=zh-CN&tz=Asia%2FShanghai&tc=&ck=0&mime=js&x=0.6584599927109089">韩国太妍03</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=http://1.245.74.5:1935/live/tv/.m3u8">韩国TJB综艺</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=http://119.77.96.184:1935/chn21/chn21/playlist.m3u8">韩国中央</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=http://brics.bonus-tv.ru/cdn/brics/chinese/tracks-v1a1/index.m3u8">俄罗斯中文</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=https://php.17186.eu.org/phtv/fhhk.m3u8">凤凰香港</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://m3u8.waahah.xyz/?waahah=http://tv6.blcu.edu.cn/liverespath/c940a4bb57b88b3280c6da07f85649fc69629230/3a31be87ad-0-0-ddeb3cb91d9827471933e1c139694276/index.m3u8">河南卫视</a></li>
                        <!--<li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv1">CCTV-1综合</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv2">CCTV-2财经</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv3">CCTV-3综艺</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv4">CCTV-4国际</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv6">CCTV-6电影</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv7">CCTV-7军事</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv8">CCTV-8电视剧</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv9">CCTV-9纪录</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv10">CCTV-10科教</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv11">CCTV-11戏曲</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv12">CCTV-12社会</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv13">CCTV-13新闻</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv14">CCTV-14少儿</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv15">CCTV-15音乐</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv16">CGTN</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cctv17">CCTV-17农业</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=btv1">北京卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=btv2">北京文艺</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=btv3">北京科教</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=btv4">北京影视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=btv5">北京财经</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=btv7">北京生活</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=btv8">北京青年</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=btv9">北京新闻</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=btv10">北京卡酷少儿</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=btv11">北京冬奥纪实</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=zjtv">浙江卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=hunantv">湖南卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=jstv">江苏卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=dftv">东方卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=sztv">深圳卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=ahtv">安徽卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=hntv">河南卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=sxtv">陕西卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=jltv">吉林卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=gdtv">广东卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=sdtv">山东卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=hbtv">湖北卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=hebtv">河北卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=xztv">西藏卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=nmtv">内蒙古卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=qhtv">青海卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=sctv">四川卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=tjtv">天津卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=sxrtv">山西卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=lntv">辽宁卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=xmtv">厦门卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=xjtv">新疆卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=hljtv">黑龙江卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=yntv">云南卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=jxtv">江西卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=dntv">福建东南卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=gztv">贵州卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=nxtv">宁夏卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=gstv">甘肃卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cqtv">重庆卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=bttv">兵团卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=ybtv">延边卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=sstv">三沙卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=lytv">海南卫视</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=sdetv">山东教育</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cetv1">CETV-1</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cetv3">CETV-3</a></li>
                        <li><a style="color:#D8D8D8;"target="_blank" href="http://ivi.bupt.edu.cn/player.html?channel=cetv4">CETV-4</a></li>-->
					</ul>
                </div>
            </div>
        </div>
    </div>
    <div class='aelected_text' style='display:none;>
        <img src='` + aImgBase64 + `' title='实用工具'/>
        <div class='vip_mod_box_aelected' >
            <div style='display:flex;'>
                <div style='width:230px; padding:10px 0;'>
                    <div style='font-size:16px; text-align:center; color:#DF0174; line-height:21px;'>实用工具</div>
                    <!--<ul>
                        <li>
                            <a style="color:#D8D8D8;"href="https://greasyfork.org/zh-CN/scripts/422277-%E6%97%A0%E5%B7%AE%E5%88%AB%E8%A7%86%E9%A2%91%E6%8F%90%E5%8F%96%E5%B7%A5%E5%85%B7" target="_blank">无差别视频提取工具</a>
                        </li>
                    </ul>-->
                    <ul>
                        <li>
                            <a style="color:#D8D8D8;"href="https://m3u8.waahah.xyz" target="_blank">m3u8 视频/直播在线播放</a>
                        </li>
                    </ul>
                    <ul>
                        <li>
                            <a style="color:#D8D8D8;"href="https://maple3142.github.io/mergemp4/" target="_blank">视频合成工具</a>
                        </li>
                    </ul>
                     <ul>
                        <li>
                            <a style="color:#D8D8D8;"href="https://https://nextchat.waahah.xyz/" target="_blank">GPT镜像站点</a>
                        </li>
                    </ul>
                </div>
            </div>
      </div>
</div>`);

        $("body").append(html);
        $(".item_text").on("mouseover", () => {
            $(".vip_mod_box_action").show();
        });
        $(".item_text").on("mouseout", () => {
            $(".vip_mod_box_action").hide();
        });

        $(".selected_text").on("mouseover", () => {
            $(".vip_mod_box_selected").show();
        });
        $(".selected_text").on("mouseout", () => {
            $(".vip_mod_box_selected").hide();
        });

        $(".belected_text").on("mouseover", () => {
            $(".vip_mod_box_belected").show();
        });
        $(".belected_text").on("mouseout", () => {
            $(".vip_mod_box_belected").hide();
        });

        $(".aelected_text").on("mouseover", () => {
            $(".vip_mod_box_aelected").show();
        });
        $(".aelected_text").on("mouseout", () => {
            $(".vip_mod_box_aelected").hide();
        });
        $(".qelected_text").on("mouseover", () => {
            $(".vip_mod_box_qelected").show();
        });
        $(".qelected_text").on("mouseout", () => {
            $(".vip_mod_box_qelected").hide();
        });
        $(".melected_text").on("mouseover", () => {
            $(".vip_mod_box_melected").show();
        });
        $(".melected_text").on("mouseout", () => {
            $(".vip_mod_box_melected").hide();
        });

        $(".vip_mod_box_action li").each((index, item) => {
            item.addEventListener("click", () => {
                if (parseInterfaceList[index].type == "1" || parseInterfaceList[index].type == "2") {
                    if (document.getElementById("iframe-player") == null) {
                        var player = $(node);
                        player.empty();
                        player.append(videoPlayer);
                    }
                    innerParse(parseInterfaceList[index].url + location.href);
                }
                if (parseInterfaceList[index].type == "0") {
                    try{
                        GMopenInTab(parseInterfaceList[index].url + location.href, false);
                    }catch(e){
                        console.log(e);
                        window.open(parseInterfaceList[index].url + location.href, "_blank");
                    }
                }
            });
        });
        document.getElementsByClassName('item_text')[0].addEventListener("click", () => {
            var play_jx_url = window.location.href;
            if (/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {
                var mobile_html = "<div style='margin:0 auto;padding:10px;'>";
                mobile_html += "<button type='button' style='position:absolute;top:0;right:30px;font-size:30px;line-height: 1;color: #000;text-shadow: 0 1px 0 #fff;cursor: pointer;border:0;background:0 0;' onclick='location.reload();'>×</button>";
                mobile_html += "<div><iframe src='https://tv.wandhi.com/go.html?url=" + play_jx_url + "' allowtransparency=true frameborder='0' scrolling='no' allowfullscreen=true allowtransparency=true name='jx_play'style='height:600px;width:100%'></iframe></div>"
                mobile_html += "</div>";
                $("body").html(mobile_html);
            } else { }
        });
        (function () {
            $("body").append("");
            setTimeout(function () {
                $("#loading").remove();
                $("#bilibili-player-wrap").after('<div class="bottom-page paging-box-big"><span class="current" style="background: #f45a8d;" id="go">站外下载视频</span><span>免责声明：请通过合法渠道购买VIP下载大会员内容,插件仅对网页布局进行修改，不含任何漏洞利用，入侵，绕过等方式实现的功能</br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;因使用插件造成任何后果的，插件作者不承担任何责任。</span></div>');
                $("#go").click(function () {
                    var aaa = $(".mediainfo_mediaTitle__lu7u_").attr("title");
                    var bbb = aaa.replace(/\s/g, "");
                    var tempwindow = window.open("_blank");
                    tempwindow.location = "https://www.wxtv.net/vodsearch/-------------.html?wd=" + bbb
                })
            }, 4000)
        })();
        switch (host) {
            case 'www.iqiyi.com':
                //--------------------------------------------------------------------------------
                try{
                    var num = 0;
                    unsafeWindow.rate = 0;
                    unsafeWindow.Date.now = () => {
                        return new unsafeWindow.Date().getTime() + (unsafeWindow.rate += 1000);
                    }
                    setInterval(() => {
                        unsafeWindow.rate = 0;
                    }, 600000);
                }catch(e){
                    console.log(e);
                }

                //--------------------------------------------------------------------------------
                document.getElementsByClassName('qy-episode-num')[0].addEventListener('click', () => {
                    setTimeout(urlChangeReload, 500);
                });
                //--------------------------------------------------------------------------------
                const iqiyiDisplayNodes = ["#playerPopup", "div[class^=qy-header-login-pop]", "section[class^=modal-cover_]", ".toast"];
                setInterval(() => {
                    $('div[style*="top: 74px"]').attr("id", "absolute");
                    $("#absolute").css("zIndex", 0);
                    iqiyiDisplayNodes.forEach(node => {
                        $(node).css("display", "none");
                    });
                }, 500);

                setTimeout(() => {
                    var episodeList = [];
                    var i71playpagesdramalist = $("div[is='i71-play-ab']");
                    if (i71playpagesdramalist.length != 0) {
                        var data = i71playpagesdramalist.attr(":page-info");
                        if (!!data) {
                            var dataJson = JSON.parse(data);
                            var albumId = dataJson.albumId;
                            var barlis = $(".qy-episode-tab").find(".bar-li");
                            var barTotal = barlis.length;
                            if (barTotal == 0) {
                                barTotal = 1;
                            }
                            for (var page = 1; page <= barTotal; page++) {
                                GMxmlhttpRequest({
                                    url: "https://pcw-api.iqiyi.com/albums/album/avlistinfo?aid=" + albumId + "&page=" + page + "&size=30",
                                    method: "GET",
                                    headers: {
                                        "Content-Type": "application/x-www-form-urlencoded"
                                    },
                                    onload: response => {
                                        var status = response.status;
                                        if (status == 200 || status == '200') {
                                            var serverResponseJson = JSON.parse(response.responseText);
                                            var code = serverResponseJson.code;
                                            if (code == "A00000") {
                                                var serverEpsodelist = serverResponseJson.data.epsodelist;
                                                //console.log(serverEpsodelist)
                                                for (var i = 0; i < serverEpsodelist.length; i++) {
                                                    var name = serverEpsodelist[i].order;
                                                    var href = serverEpsodelist[i].playUrl;
                                                    var description = serverEpsodelist[i].subtitle;
                                                    episodeList.push({
                                                        "name": name,
                                                        "href": href,
                                                        "description": description
                                                    });
                                                    //mylog({"name":name, "href":href, "description":description});
                                                }
                                                selectedList(episodeList);
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }, 2000);
                break
            case 'v.qq.com':
                //--------------------------------------------------------------------------------
                var num = 0;
                var qqTimer = setInterval(() => {
                    try {
                        $(".txp_ad").find("txpdiv").find("video")[0].currentTime = 1000;
                        $(".txp_ad").find("txpdiv").find("video")[1].currentTime = 1000;
                    }
                    catch (e) {
                        console.log(e);
                        num += 1
                        if (num > 100) {
                            clearInterval(qqTimer); // 清除定时器
                        }
                    }
                }, 1000)
                //--------------------------------------------------------------------------------

                setInterval(() => {
                    var txp_btn_volume = $(".txp_btn_volume");
                    if (txp_btn_volume.attr("data-status") === "mute") {
                        $(".txp_popup_volume").css("display", "block");
                        txp_btn_volume.click();
                        $(".txp_popup_volume").css("display", "none");
                    }
                    //$("txpdiv[data-role='hd-ad-adapter-adlayer']").attr("class", "txp_none");
                    $(".mod_vip_popup").css("display", "none");
                    $(".panel-tip-pay").css("display", "none");
                    $("#mask_layer").css("display", "none");
                    $(".tvip_layer").css("display", "none");
                }, 500);
                window.onload = setTimeout(() => {
                    var episodeList = [];
                    var COVER_INFO = unsafeWindow.COVER_INFO;
                    var VIDEO_INFO = unsafeWindow.VIDEO_INFO;
                    var barTotal = COVER_INFO.nomal_ids.length;
                    for (var page = 1; page <= barTotal; page++) {
                        var i = page - 1
                        if (VIDEO_INFO.type_name == "动漫" || VIDEO_INFO.type_name == "电视剧" || VIDEO_INFO.type_name == "电影") {
                            var F = COVER_INFO.nomal_ids[i].F;
                            if (F != "0" && F != "4") {
                                var V = COVER_INFO.nomal_ids[i].V;
                                var cover_id = COVER_INFO.cover_id;
                                var name = COVER_INFO.nomal_ids[i].E;
                                var href = "https://v.qq.com/x/cover/" + cover_id + "/" + V + ".html";
                                episodeList.push({
                                    "name": name,
                                    "href": href,
                                    "description": ""
                                });
                                //mylog({"name":name, "href":href, "description":""});
                            }
                        }
                    }
                    selectedList(episodeList);
                }, 2000);
                break
            case 'v.youku.com':

                var youkuTimer = setInterval(() => {
                    if (!document.querySelectorAll('video')[1]) {
                        console.log('已跳过广告！')
                        setTimeout(() => {
                            clearInterval(youkuTimer); // 清除定时器
                        }, 5000)
                    }
                    else {
                        document.querySelectorAll('video')[1].playbackRate = 16;
                        //document.getElementsByTagName('video')[1].currentTime=1000;
                    }
                }, 1000)

                //--------------------------------------------------------------------------------

                document.getElementsByClassName('new-box-anthology-items')[0].addEventListener('click', () => {
                    setTimeout(urlChangeReload, 500);
                })

                //--------------------------------------------------------------------------------
                const youkuDisplayNodes = ["#iframaWrapper", "#checkout_counter_mask", "#checkout_counter_popup"];
                setInterval(() => {
                    var H5 = $(".h5-ext-layer").find("div")
                    if (H5.length != 0) {
                        $(".h5-ext-layer div").remove();
                        var control_btn_play = $(".control-left-grid .control-play-icon");
                        if (control_btn_play.attr("data-tip") === "播放") {
                            $(".h5player-dashboard").css("display", "block");
                            control_btn_play.click();
                            $(".h5player-dashboard").css("display", "none");
                        }
                    }
                    $(".information-tips").css("display", "none");
                    youkuDisplayNodes.forEach(item => {
                        $(item).css("display", "none");
                    })
                }, 500);
                window.onload = setTimeout(() => {
                    var Num;
                    var episodeList = [];
                    var videoCategory = unsafeWindow.__INITIAL_DATA__.data.data.data.extra.videoCategory;
                    if (videoCategory == "动漫" || videoCategory == "电影" || videoCategory == "少儿") {
                        Num = 1;
                    } else if (videoCategory == "电视剧" || videoCategory == "综艺") {
                        Num = 2;
                    }
                    if (!!Num) {
                        var data = unsafeWindow.__INITIAL_DATA__.data.model.detail.data.nodes[0].nodes[Num];
                        var barTotal = data.nodes.length;
                        for (var page = 1; page <= barTotal; page++) {
                            var i = page - 1
                            if (data.nodes[i].data.videoType == "正片") {
                                if (videoCategory == "综艺" || videoCategory == "少儿") {
                                    var name = i + 1;
                                } else {
                                    name = data.nodes[i].data.stage;
                                }
                                var vid = data.nodes[i].data.action.value;
                                var title = data.nodes[i].data.title;
                                var href = "https://v.youku.com/v_show/id_" + vid + ".html";
                                episodeList.push({
                                    "name": name,
                                    "href": href,
                                    "description": title
                                });
                                //mylog({"name":name, "href":href, "description":title});
                            }
                        }
                        selectedList(episodeList);
                    }
                }, 2000);
                break
            case 'www.mgtv.com':

                var mgtvnum = 0;
                var mgtvTimer = setInterval(() => {
                    try {
                        //document.getElementsByTagName('video')[0].playbackRate = 16;
                        document.getElementsByTagName('video')[1].playbackRate = 16;
                    }
                    catch (e) {
                        console.log(e);
                        mgtvnum += 1
                        if (mgtvnum > 20) {
                            //clearInterval(mgtvTimer); // 清除定时器
                        }
                    }
                }, 1000)
                //--------------------------------------------------------------------------------
                document.getElementsByClassName('episode-items clearfix')[0].addEventListener('click', () => {
                    setTimeout(urlChangeReload, 500);
                })

                //--------------------------------------------------------------------------------
                setTimeout(() => {
                    var episodeList = [];
                    var str = location.href;
                    var index = str.lastIndexOf("\/"); //斜杠 分割
                    str = str.substring(index + 1, str.length);
                    index = str.lastIndexOf(".html");
                    var albumId = str.substring(0, index);
                    //mylog(albumId)
                    var barlis = $(".episode-header").find("a");
                    var barTotal = barlis.length;
                    if (barTotal == 0) {
                        barTotal = 1;
                    }
                    for (var page = 1; page <= barTotal; page++) {
                        GMxmlhttpRequest({
                            url: "https://pcweb.api.mgtv.com/episode/list?_support=10000000&video_id=" + albumId + "&page=" + page + "&size=30",
                            method: "GET",
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            onload: response => {
                                var status = response.status;
                                if (status == 200 || status == '200') {
                                    var serverResponseJson = JSON.parse(response.responseText);
                                    var code = serverResponseJson.code;
                                    if (code == "200") {
                                        var serverEpsodelist = serverResponseJson.data.list;
                                        //mylog(serverEpsodelist)
                                        for (var i = 0; i < serverEpsodelist.length; i++) {
                                            var font = serverEpsodelist[i].corner[0].font;
                                            if (font != "预告") {
                                                var name = serverEpsodelist[i].t1;
                                                var href = serverEpsodelist[i].url;
                                                href = "https://www.mgtv.com" + href;
                                                var description = serverEpsodelist[i].t2;
                                                episodeList.push({
                                                    "name": name,
                                                    "href": href,
                                                    "description": description
                                                });
                                                //mylog({"name":name, "href":href, "description":description});
                                            }
                                        }
                                        selectedList(episodeList);
                                    }
                                }
                            }
                        });
                    }
                }, 2000);
                break
            case 'tv.sohu.com':
                var sohunum = 0;
                var sohuTimer = setInterval(() => {
                    try {
                        $("#player").find("video")[1].currentTime = 1000;
                    }
                    catch (e) {
                        console.log(e);
                        sohunum += 1;
                        if (sohunum > 20) {
                            clearInterval(sohuTimer); // 清除定时器
                        }
                    }
                }, 1000);
                //--------------------------------------------------------------------------------
                document.getElementsByClassName('series-tab_pane')[0].addEventListener('click', () => {
                    setTimeout(urlChangeReload, 500);
                })
                //--------------------------------------------------------------------------------
                window.onload = setTimeout(() => {
                    var episodeList = [];
                    var albumId = unsafeWindow.playlistId;
                    var barTotal = 1;
                    for (var page = 1; page <= barTotal; page++) {
                        GMxmlhttpRequest({
                            url: "https://pl.hd.sohu.com/videolist?playlistid=" + albumId + "&pagenum=1&pagesize=999",
                            method: "GET",
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            onload: response => {
                                var status = response.status;
                                if (status == 200 || status == '200') {
                                    var serverResponseJson = JSON.parse(response.responseText);
                                    var serverEpsodelist = serverResponseJson.videos;
                                    for (var i = 0; i < serverEpsodelist.length; i++) {
                                        var name = serverEpsodelist[i].order;
                                        var href = serverEpsodelist[i].pageUrl;
                                        var description = serverEpsodelist[i].name;
                                        episodeList.push({
                                            "name": name,
                                            "href": href,
                                            "description": description
                                        });
                                        //mylog({"name":name, "href":href, "description":description});
                                    }
                                    selectedList(episodeList);
                                }
                            }
                        });
                    }
                }, 2000);
                break
            case 'www.fun.tv':
                setTimeout(() => {
                    var control_btn_play = $(".fxp-controlbar .btn-toggle");
                    if (control_btn_play.is('.btn-play')) {
                        control_btn_play.click();
                    }
                }, 500);
                setInterval(() => {
                    $("#play-Panel-Wrap").css("display", "none");
                }, 500);

                window.onload = setTimeout(() => {
                    var episodeList = [];
                    var data = unsafeWindow.vplayInfo.dvideos[0];
                    var barTotal = data.videos.length;
                    for (var page = 1; page <= barTotal; page++) {
                        var lists = data.videos[page - 1].lists.length;
                        for (var i = 1; i <= lists; i++) {
                            var name = data.videos[page - 1].lists[i - 1].title;
                            var url = data.videos[page - 1].lists[i - 1].url;
                            var title = data.videos[page - 1].lists[i - 1].name;
                            var dtype = data.videos[page - 1].lists[i - 1].dtype;
                            if (!!name && !!url && dtype == "normal") {
                                var href = "http://www.fun.tv" + url;
                                episodeList.push({
                                    "name": name,
                                    "href": href,
                                    "description": title
                                });
                                //mylog({"name":name, "href":href, "description":title});
                            }
                            selectedList(episodeList);
                        }
                    }
                }, 2000);
                break
            case 'www.bilibili.com':

                setInterval(() => {
                    $(".player-limit-mask").remove();
                    $("[class^=playerPop_wrap]").remove();
                }, 500);
                //-------------------------------------------------
                document.getElementsByClassName('numberList_wrapper__xihGs')[0].addEventListener('click', () => {
                    setTimeout(urlChangeReload, 500);
                });
                //-------------------------------------------------
                window.onload = setTimeout(() => {
                    var episodeList = [];
                    var data = unsafeWindow.__INITIAL_STATE__;
                    var barTotal = data.epList.length;
                    for (var page = 1; page <= barTotal; page++) {
                        var i = page - 1
                        var badge = data.epList[i].badge
                        var name = data.epList[i].title;
                        var id = data.epList[i].id;
                        var title = data.epList[i].longTitle;
                        if (!!name && !!id && badge != "预告") {
                            var href = "https://www.bilibili.com/bangumi/play/ep" + id;
                            episodeList.push({
                                "name": name,
                                "href": href,
                                "description": title
                            });
                        }
                        //mylog({"name":name, "href":href, "description":title});
                    }
                    selectedList(episodeList);
                }, 2000);
                break

            case 'v.pptv.com':

                window.onload = setTimeout(() => {
                    var episodeList = [];
                    var data = unsafeWindow.webcfg;
                    var dataJson = data.playList.data;
                    var barTotal = dataJson.list.length;
                    for (var page = 1; page <= barTotal; page++) {
                        var i = page - 1
                        var name = dataJson.list[i].rank + 1;
                        var href = dataJson.list[i].url;
                        var title = dataJson.list[i].title;
                        if (!!name && !!href) {
                            episodeList.push({
                                "name": name,
                                "href": href,
                                "description": title
                            });
                        }
                        //mylog({"name":name, "href":href, "description":title});
                    }
                    selectedList(episodeList);
                }, 2000);
                break
            //--------------------------------------------------------------------------------
            case 'www.wxtv.net':
                $("#vip_movie_box").remove();
                $(".searchlist_item").find("a").attr("id", "bbb");
                $("#topnav").remove();
                $("#mygod2m").remove();
                $("#mygod2pc").remove();
                $(".bgi_box").remove();
                $(".content_btn").remove();
                $(".hidden_xs").remove();
                $("#bofy").remove();
                $(".fo_t").remove();
                $(".list_scroll").remove();
                $(".title").remove();
                setInterval(() => {
                    $("#bbb").click();
                }, 500);
                break
            //--------------------------------------------------------------------------------
            case 'wetv.vip':
                setInterval(() => {

                }, 500);
                break
            //--------------------------------------------------------------------------------
            case 'www.eggvod.cn':
                $("#vip_movie_box").remove();
                var yhq = $(`
                <div>
                    <ul>
                        <li>
                          <a href="https://www.zuihuitao.cn">淘宝天猫优惠券</a>
                        </li>
                    </ul>
                     <ul>
                        <li>
                            <a href="https://zuihuitao.top/vip/index.html">关于脚本</a>
                        </li>
                    </ul>
                </div>
                       `);
                $(".tips").html(yhq)
                setInterval(() => {
                }, 500);
                break
            //--------------------------------------------------------------------------------
            case 'music.163.com':
                $(".item_text").remove();
                $(".qelected_text").remove();
                $(".selected_text").remove();
                $(".aelected_text").remove();
                $(".belected_text").remove();
                $("#m").remove();
                setInterval(() => {
                    $(".melected_text").show();
                }, 500);
                break
            //--------------------------------------------------------------------------------
            case 'y.qq.com':
                $(".item_text").remove();
                $(".qelected_text").remove();
                $(".selected_text").remove();
                $(".aelected_text").remove();
                $(".belected_text").remove();
                $("#m").remove();
                setInterval(() => {
                    $(".melected_text").show();
                }, 500);
                break
            //--------------------------------------------------------------------------------
            case 'www.kugou.com':
                $(".item_text").remove();
                $(".qelected_text").remove();
                $(".selected_text").remove();
                $(".aelected_text").remove();
                $(".belected_text").remove();
                $("#m").remove();
                setInterval(() => {
                    $(".melected_text").show();
                }, 500);
                break
            //--------------------------------------------------------------------------------
            case 'www.kuwo.cn':
                $(".item_text").remove();
                $(".qelected_text").remove();
                $(".selected_text").remove();
                $(".aelected_text").remove();
                $(".belected_text").remove();
                $("#m").remove();
                setInterval(() => {
                    $(".melected_text").show();
                }, 500);
                break
            //--------------------------------------------------------------------------------
            case 'video.zhihu.com':
                $(".item_text").remove();
                $(".qelected_text").remove();
                $(".selected_text").remove();
                $(".aelected_text").remove();
                $(".belected_text").remove();
                $(".melected_text").remove();
                $("#m").remove();
                setInterval(() => {
                }, 500);
                break
            //--------------------------------------------------------------------------------
            case 'tv.wandhi.com':
                $('#tip').hide();
                $('.container').hide();
                $('.135brush').remove();
                $('.table .table-bordered').remove();
                $('.unit-100').remove();
                $('#vip_movie_box').hide();
                document.getElementsByClassName('panel-body')[1].style.display='none';
                break
            //--------------------------------------------------------------------------------
            default:
                break
        }

        var localurl = location.href;

        function addScript(url) {
            var s = document.createElement('script');
            s.setAttribute('src', url);
            document.body.appendChild(s);
        }
        var reg_acfun = /www.acfun.cn\/v\/.*/;

        function acfun() {
            var ele, content = '',
                h1, videolist;
            try {
                h1 = $('h1.title');
                videolist = JSON.parse(videoInfo.currentVideoInfo.ksPlayJson).adaptationSet[0].representation;
                if (videolist.length > 0 && $('div#downloadlist').length == 0) {
                    for (var i = 0; i < videolist.length; i++) {
                        content = content + `
            <div style="margin:5px 0px;">
                <div style="display:inline-block;font-weight:bold;width:10%;">${videolist[i].qualityLabel}：</div>
                <input type="text" style="color:#5FB404;width:68%" value="${videolist[i].url}">
                </div>`;
                    }
                    ele = `
                <div id="downloadlist" style="margin:15px 0px;color:#DF0174;">
                    <div>\u8BF7\u4F7F\u7528m3u8\u4E0B\u8F7D\u5DE5\u5177（
                        <a target="_blank" href="https://xsyhnb.lanzoui.com/iTA2rg3hfef">\u63A8\u8350\u5DE5\u5177</a>）
                    </div>${content}</div>`;
                }
                h1.after(ele);
            } catch (e) {
                console.log('acfun', e.message);
            }
        }

        function xsyhnbrun() {
            if (reg_acfun.test(localurl)) {
                acfun();
            }
        }
        setInterval(xsyhnbrun, 500);

        mo();
    }


})();